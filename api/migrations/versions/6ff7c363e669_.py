"""empty message

Revision ID: 6ff7c363e669
Revises: 
Create Date: 2018-04-26 18:09:55.798184

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6ff7c363e669'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('users',
    sa.Column('no_hp', sa.String(length=12), nullable=False),
    sa.Column('nama', sa.String(length=50), nullable=True),
    sa.Column('url_foto', sa.String(length=100), nullable=True),
    sa.Column('waktu_registrasi', sa.DateTime(), server_default=sa.text('CURRENT_TIMESTAMP'), nullable=True),
    sa.Column('last_modified', sa.DateTime(), server_default=sa.text('CURRENT_TIMESTAMP'), nullable=True),
    sa.Column('is_online', sa.Boolean(), nullable=True),
    sa.Column('is_verified', sa.Boolean(), nullable=True),
    sa.Column('location', sa.String(length=100), nullable=True),
    sa.Column('password', sa.String(length=64), nullable=True),
    sa.Column('verification_code', sa.String(length=6), nullable=True),
    sa.PrimaryKeyConstraint('no_hp')
    )
    op.create_table('pedagang',
    sa.Column('no_hp', sa.String(length=12), nullable=False),
    sa.Column('nama_dagangan', sa.String(length=50), nullable=True),
    sa.Column('waktu_buka', sa.String(length=5), nullable=True),
    sa.Column('waktu_tutup', sa.String(length=5), nullable=True),
    sa.Column('is_mangkal', sa.Boolean(), nullable=True),
    sa.ForeignKeyConstraint(['no_hp'], ['users.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('no_hp')
    )
    op.create_table('pembeli',
    sa.Column('no_hp', sa.String(length=12), nullable=False),
    sa.ForeignKeyConstraint(['no_hp'], ['users.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('no_hp')
    )
    op.create_table('events',
    sa.Column('no_hp', sa.String(length=12), nullable=False),
    sa.Column('id_event', sa.Integer(), nullable=False),
    sa.Column('nama_event', sa.String(length=25), nullable=False),
    sa.Column('deskripsi', sa.String(length=150), nullable=True),
    sa.Column('waktu_mulai', sa.DateTime(), nullable=True),
    sa.Column('waktu_selesai', sa.DateTime(), nullable=False),
    sa.Column('location', sa.String(length=100), nullable=True),
    sa.ForeignKeyConstraint(['no_hp'], ['pembeli.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('no_hp', 'id_event')
    )
    op.create_table('favorit',
    sa.Column('no_hp_pembeli', sa.String(length=12), nullable=False),
    sa.Column('no_hp_pedagang', sa.String(length=12), nullable=False),
    sa.ForeignKeyConstraint(['no_hp_pedagang'], ['pedagang.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['no_hp_pembeli'], ['pembeli.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('no_hp_pembeli', 'no_hp_pedagang')
    )
    op.create_table('menu',
    sa.Column('no_hp', sa.String(length=12), nullable=False),
    sa.Column('id_menu', sa.Integer(), nullable=False),
    sa.Column('nama_menu', sa.String(length=50), nullable=False),
    sa.Column('is_ready', sa.Boolean(), nullable=True),
    sa.Column('url_foto_menu', sa.String(length=100), nullable=True),
    sa.ForeignKeyConstraint(['no_hp'], ['pedagang.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('no_hp', 'id_menu')
    )
    op.create_table('reviews',
    sa.Column('no_hp_pembeli', sa.String(length=12), nullable=False),
    sa.Column('no_hp_pedagang', sa.String(length=12), nullable=False),
    sa.Column('deskripsi', sa.String(length=150), nullable=False),
    sa.Column('rating', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['no_hp_pedagang'], ['pedagang.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['no_hp_pembeli'], ['pembeli.no_hp'], onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('no_hp_pembeli', 'no_hp_pedagang', 'deskripsi')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('reviews')
    op.drop_table('menu')
    op.drop_table('favorit')
    op.drop_table('events')
    op.drop_table('pembeli')
    op.drop_table('pedagang')
    op.drop_table('users')
    # ### end Alembic commands ###
