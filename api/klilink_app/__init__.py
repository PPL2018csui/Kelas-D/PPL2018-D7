from flask import Flask
from klilink_app.models import db
from klilink_app.config import Config
from klilink_app.routes import api

app = Flask(__name__)
app.config.from_object(Config)
app.register_blueprint(api)
db.init_app(app)
