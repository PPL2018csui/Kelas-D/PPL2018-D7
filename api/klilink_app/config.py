import os


class Config(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TESTING = False


class TestingConfig(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DB_URL_TEST')
    TESTING = True
