from klilink_app.models import Users, Pembeli, Pedagang, Events, Menu, encode_password


class Seed():
    def __init__(self, db):
        self.db = db
        self.db.drop_all()
        self.db.create_all()

        self.seed_user()
        self.seed_pembeli()
        self.seed_pedagang()
        self.seed_events()
        self.seed_menu()

    def seed_user(self):
        users_data = [
            {
                'no_hp': '081234567890',
                'nama': 'Farizki',
                'url_foto': 'https://pbs.twimg.com/profile_images/761921865501712386/UH8G2Dql_400x400.jpg',
                'password': encode_password('password0'),
                'location': '-6.361466, 106.821586'
            },
            {
                'no_hp': '081234567891',
                'nama': 'Indra',
                'url_foto': 'https://pbs.twimg.com/profile_images/737274157059055617/1H8hsqEs_400x400.jpg',
                'password': encode_password('password1'),
                'location': '-6.361466, 106.821586'
            },
            {
                'no_hp': '081234567892',
                'nama': 'Emir',
                'url_foto': 'https://pbs.twimg.com/profile_images/2986398928/75425d2d2ac6eeb7b761e22e9b479aad_400x400.jpeg',
                'password': encode_password('password2'),
                'location': '-6.361466, 106.821586'
            },
            {
                'no_hp': '081234567893',
                'nama': 'Dwi',
                'url_foto': 'https://kamirohis68.files.wordpress.com/2017/01/6e133-28.jpg',
                'is_online': True,
                'password': encode_password('password3'),
                'location': '-6.361466, 106.821586'
            },
            {
                'no_hp': '081234567894',
                'nama': 'Bedil',
                'url_foto': 'http://bedhilzz.com/src/img/gue2.jpg',
                'is_online': True,
                'password': encode_password('password4'),
                'location': '-6.366998, 106.818424'
            },
            {
                'no_hp': '081234567895',
                'nama': 'Ibam',
                'url_foto': 'https://yt3.ggpht.com/-6oXfMWAFoGs/AAAAAAAAAAI/AAAAAAAAAAA/l1PsDMOY5LM/s900-c-k-no-mo-rj-c0xffffff/photo.jpg',
                'is_online': False,
                'password': encode_password('password5'),
                'location': '-6.366568, 106.820637'
            }
        ]

        for d in users_data:
            inp = Users(**d)
            self.db.session.add(inp)
        self.db.session.commit()

    def seed_pembeli(self):
        pembeli_data = [
            {
                'no_hp': '081234567890'
            },
            {
                'no_hp': '081234567891'
            },
            {
                'no_hp': '081234567892'
            }
        ]
        for d in pembeli_data:
            inp = Pembeli(**d)
            self.db.session.add(inp)
        self.db.session.commit()

    def seed_pedagang(self):
        pedagang_data = [
            {
                'no_hp': '081234567893',
                'nama_dagangan': 'Dwi dagang nasi',
                'waktu_buka': '08.00',
                'waktu_tutup': '17.00',
                'is_mangkal': True
            },
            {
                'no_hp': '081234567894',
                'nama_dagangan': 'Bedil dagang domie',
                'waktu_buka': '08.00',
                'waktu_tutup': '19.00',
                'is_mangkal': True
            },
            {
                'no_hp': '081234567895',
                'nama_dagangan': 'Ibam dagang lumpia',
                'waktu_buka': '10.00',
                'waktu_tutup': '20.00',
                'is_mangkal': False
            }
        ]
        for d in pedagang_data:
            inp = Pedagang(**d)
            self.db.session.add(inp)
        self.db.session.commit()

    def seed_events(self):
        events_data = [
            {
                'no_hp': '081234567890',
                'id_event': '0000000001',
                'nama_event': 'Lomba RT 14',
                'deskripsi': 'Lomba tahunan RT 14',
                'waktu_mulai': '2018-4-21 10:00:00',
                'waktu_selesai': '2018-4-30 13:30:10',
                'location': '-6.363100, 106.820413'
            },
            {
                'no_hp': '081234567891',
                'id_event': '0000000002',
                'nama_event': 'Maen bola',
                'deskripsi': 'Bocah maen bola di sokin',
                'waktu_mulai': '2018-4-21 10:00:00',
                'waktu_selesai': '2018-4-30 12:30:10',
                'location': '-6.361726, 106.817071'
            },
            {
                'no_hp': '081234567892',
                'id_event': '0000000003',
                'nama_event': 'Syut',
                'deskripsi': 'Syuting at Masjid Al Hikam',
                'waktu_mulai': '2018-4-21 10:00:00',
                'waktu_selesai': '2018-4-30 15:30:10',
                'location': '-6.359872, 106.820866'
            }
        ]

        for d in events_data:
            inp = Events(**d)
            self.db.session.add(inp)
        self.db.session.commit()

    def seed_menu(self):
        menu_data = [
            {
                'no_hp': '081234567893',
                'id_menu': '0000000001',
                'harga': 8000,
                'nama_menu': 'mie ijo',
                'is_ready': True,
                'url_foto_menu': 'https://selerasa.com/images/mie/Mie_ayam/09-mie-ayam-hijau.jpg'
            },
            {
                'no_hp': '081234567893',
                'id_menu': '0000000002',
                'harga': 10000,
                'nama_menu': 'nasi merah',
                'is_ready': True,
                'url_foto_menu': 'https://i0.wp.com/www.jamuku.com/wp-content/uploads/2017/05/beras-merah.jpg'
            },
            {
                'no_hp': '081234567893',
                'id_menu': '0000000003',
                'harga': 12000,
                'nama_menu': 'nasi kuning',
                'is_ready': True,
                'url_foto_menu': 'https://cdns.klimg.com/merdeka.com/i/w/news/2017/10/30/903570/670x335/cara-membuat-nasi-kuning-spesial-yang-enak-dan-gurih.jpg'
            },
            {
                'no_hp': '081234567894',
                'id_menu': '0000000004',
                'harga': 6000,
                'nama_menu': 'domie polos',
                'is_ready': True,
                'url_foto_menu': 'https://www.pegipegi.com/travel/wp-content/uploads/2017/06/indomie-goreng.jpg'
            },
            {
                'no_hp': '081234567894',
                'id_menu': '0000000005',
                'harga': 7500,
                'nama_menu': 'domie dobel polos',
                'is_ready': True,
                'url_foto_menu': 'http://assets.kompas.com/data/photo/2013/04/08/1505173-mi-rasa-cakalang-rebus-620X310.jpg'
            },
            {
                'no_hp': '081234567894',
                'id_menu': '0000000006',
                'harga': 9000,
                'nama_menu': 'domie telor',
                'is_ready': True,
                'url_foto_menu': 'http://assets.kompas.com/data/photo/2013/04/08/1507362-mi-rasa-kari-ayam-620X310.jpg'
            },
            {
                'no_hp': '081234567894',
                'id_menu': '0000000007',
                'harga': 15000,
                'nama_menu': 'domie dobel telor',
                'is_ready': True,
                'url_foto_menu': 'http://assets.kompas.com/data/photo/2013/04/08/1505173-mi-rasa-cakalang-rebus-620X310.jpg'
            },
            {
                'no_hp': '081234567895',
                'id_menu': '0000000008',
                'harga': 20000,
                'nama_menu': 'lumpia goreng',
                'is_ready': True,
                'url_foto_menu': 'https://i.ytimg.com/vi/WICZseO7GGg/hqdefault.jpg'
            },
            {
                'no_hp': '081234567895',
                'id_menu': '0000000009',
                'harga': 8000,
                'nama_menu': 'lumpia basah',
                'is_ready': True,
                'url_foto_menu': 'https://bacaterus.com/wp-content/uploads/2015/07/resep-lumpia-basah-bandung.jpg'
            },
            {
                'no_hp': '081234567895',
                'id_menu': '0000000010',
                'harga': 6320,
                'nama_menu': 'lumpia telor',
                'is_ready': True,
                'url_foto_menu': 'https://bacaterus.com/wp-content/uploads/2015/05/resep-martabak-telor-mini.jpg'
            }
        ]

        for d in menu_data:
            inp = Menu(**d)
            self.db.session.add(inp)
        self.db.session.commit()
