"""This module defines the database of our app."""
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship, backref
from urllib.parse import urlencode
import hashlib


db = SQLAlchemy()


def encode_password(inp_password):
    return hashlib.sha256(inp_password.encode()).hexdigest()


def sms_url(no_hp, message):
    return 'http://33c599ac.ngrok.io/?{}'.format(urlencode({
        'number': no_hp,
        'message': message
    }))


class Users(db.Model):
    __tablename__ = 'users'
    no_hp = db.Column(db.String(12), primary_key=True)
    nama = db.Column(db.String(50))
    url_foto = db.Column(db.Text)
    waktu_registrasi = db.Column(
        db.DateTime,
        server_default=db.func.current_timestamp()
    )
    last_modified = db.Column(
        db.DateTime,
        server_default=db.func.current_timestamp(),
        server_onupdate=db.func.current_timestamp()
    )
    is_online = db.Column(db.Boolean, default=False)
    is_verified = db.Column(db.Boolean, default=False)
    location = db.Column(db.String(100))
    password = db.Column(db.String(64))
    verification_code = db.Column(db.String(6))
    reset_code = db.Column(db.String(6))
    fcm_token = db.Column(db.String(255))

    def json_format(self):
        dic = {
            'no_hp': self.no_hp,
            'nama': self.nama,
            'role': self.get_role(),
            'url_foto': self.url_foto,
            'waktu_registrasi': str(self.waktu_registrasi),
            'last_modified': str(self.last_modified),
            'is_online': self.is_online,
            'is_verified': self.is_verified,
            'location': self.location,
            'fcm_token': self.fcm_token
        }
        return dic

    def get_role(self):
        pedagang = Pedagang.query.filter_by(no_hp=self.no_hp).first()
        if not pedagang:
            return 'pembeli'
        return 'pedagang'


class Pedagang(db.Model):
    __tablename__ = 'pedagang'
    no_hp = db.Column(
        db.String(12),
        ForeignKey("users.no_hp", ondelete='CASCADE', onupdate='CASCADE'),
        primary_key=True
    )
    nama_dagangan = db.Column(db.String(50))
    waktu_buka = db.Column(db.String(5))
    waktu_tutup = db.Column(db.String(5))
    is_mangkal = db.Column(db.Boolean, default=False)

    def json_format(self):
        dic = {
            'no_hp': self.no_hp,
            'nama_dagangan': self.nama_dagangan,
            'waktu_buka': self.waktu_buka,
            'waktu_tutup': self.waktu_tutup,
            'is_mangkal': self.is_mangkal,
        }
        return dic


class Menu(db.Model):
    __tablename__ = 'menu'
    no_hp = db.Column(
        db.String(12),
        ForeignKey("pedagang.no_hp", ondelete='CASCADE', onupdate='CASCADE'),
        primary_key=True
    )
    id_menu = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama_menu = db.Column(db.String(50), nullable=False)
    harga = db.Column(db.Integer, default=0)
    is_ready = db.Column(db.Boolean, default=True)
    url_foto_menu = db.Column(db.Text)

    def json_format(self):
        dic = {
            'no_hp': self.no_hp,
            'id_menu': self.id_menu,
            'nama_menu': self.nama_menu,
            'harga': self.harga,
            'is_ready': self.is_ready,
            'url_foto_menu': self.url_foto_menu
        }
        return dic


class Pembeli(db.Model):
    __tablename__ = 'pembeli'
    no_hp = db.Column(
        db.String(12),
        ForeignKey("users.no_hp", ondelete='CASCADE', onupdate='CASCADE'),
        primary_key=True
    )


class Events(db.Model):
    __tablename__ = 'events'
    no_hp = db.Column(
        db.String(12),
        ForeignKey("pembeli.no_hp", ondelete='CASCADE', onupdate='CASCADE'),
        primary_key=True
    )
    id_event = db.Column(db.Integer, primary_key=True, autoincrement=True)
    nama_event = db.Column(db.String(25), nullable=False)
    deskripsi = db.Column(db.String(150))
    waktu_mulai = db.Column(db.String(22))
    waktu_selesai = db.Column(db.String(22), nullable=False)
    location = db.Column(db.String(100))

    def json_format(self):
        dic = {
            'no_hp': self.no_hp,
            'id_event': self.id_event,
            'nama_event': self.nama_event,
            'deskripsi': self.deskripsi,
            'waktu_mulai': self.waktu_mulai,
            'waktu_selesai': self.waktu_selesai,
            'location': self.location
        }
        return dic


class Orders(db.Model):
    __tablename__ = 'orders'

    id_order = db.Column(db.Integer, primary_key=True)
    hp_pedagang = db.Column(
        db.String(12),
        ForeignKey("pedagang.no_hp", ondelete='CASCADE', onupdate='CASCADE')
    )
    hp_pembeli = db.Column(
        db.String(12),
        ForeignKey("pembeli.no_hp", ondelete='CASCADE', onupdate='CASCADE')
    )
    status = db.Column(db.String(25), nullable=False)
    harga_total = db.Column(db.Integer)

    def json_format(self):
        dic = {
            'id_order': self.id_order,
            'hp_pedagang': self.hp_pedagang,
            'hp_pembeli': self.hp_pembeli,
            'status': self.status,
            'harga_total': self.harga_total
        }

        return dic


class OrderDetails(db.Model):
    __tablename__ = 'order_details'

    id_details = db.Column(db.Integer, primary_key=True, autoincrement=True)

    id_order = db.Column(
        db.Integer,
        ForeignKey('orders.id_order', ondelete='CASCADE', onupdate='CASCADE')
    )

    id_menu = db.Column(db.Integer)

    no_hp = db.Column(db.String(12))

    ForeignKeyConstraint(
        ['id_menu', 'no_hp'],
        ['menu.id_menu', 'menu.no_hp'],
        onupdate="CASCADE",
        ondelete="SET NULL"
    )

    quantity = db.Column(db.Integer, default=-1)

    order = relationship(
        Orders,
        backref=backref(
            'order_details',
            uselist=True,
            cascade='delete,all'
        )
    )

    def json_format(self):
        dic = {
            'id_details': self.id_details,
            'id_order': self.id_order,
            'quantity': self.quantity,
            'id_menu': self.id_menu
        }

        return dic
