from klilink_app.models import *
from sqlalchemy.exc import IntegrityError
import random
import requests
import json


def get_profile(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}

    user = Users.query.filter_by(no_hp=form['no_hp']).first()
    if not user:
        ret['err'] = 'user_not_found'
        return ret

    ret['is_success'] = True
    ret['data'] = user.json_format()
    return ret


def edit_profile(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}

    user = Users.query.filter_by(no_hp=form['no_hp']).first()
    if not user:
        ret['err'] = 'user_not_found'
        return ret

    ret['is_success'] = True
    if form['nama'] != '':
        user.nama = form['nama']
    if form['url'] != '':
        user.url_foto = form['url']
    if form['password'] != '':
        user.password = encode_password(form['password'])
    db.session.commit()

    ret['data'] = user.json_format()
    return ret


def get_all_pedagang():
    all_user_pedagang = db.session.query(Users, Pedagang).join(Pedagang).filter(Users.is_online)
    arr = []
    for user, pedagang in all_user_pedagang:
        dic = pedagang.json_format()
        dic['nama'] = user.nama
        dic['url_foto'] = user.url_foto
        dic['location'] = user.location
        arr.append(dic)
    return arr


def get_all_event():
    all_event = Events.query.all()
    arr = []
    for event in all_event:
        arr.append(event.json_format())
    return arr


def insert_user(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    try:
        dic = {
            'no_hp': form['no_hp'],
            'nama': form['nama'],
            'url_foto': '',
            'password': encode_password(form['password']),
            'location': form['location'] if ('location' in form) else None,
            'fcm_token': form['fcm_token']
        }

        user = Users(**dic)
        db.session.add(user)
        db.session.commit()

        if form['role'] == 'pembeli':
            Class = Pembeli
        else:
            Class = Pedagang
        obj = Class(no_hp=form['no_hp'])
        db.session.add(obj)

        user.verification_code = random.randint(100000, 999999)
        db.session.commit()
        pesan = "Halo {nama}, berikut kode verifikasi Klilink kamu: {kode}".format(
            nama=user.nama,
            kode=user.verification_code
        )
        requests.get(sms_url(user.no_hp, pesan))

        senbird_data = register_to_sendbird(dic)
        
        ret['is_success'] = True
        ret['data'] = user.json_format()
        ret['data']['sendbird'] = senbird_data

    except IntegrityError as e:
        db.session.rollback()
        ret['err'] = 'duplicate_FK'
    return ret


def register_to_sendbird(form):
    api_headers = {'Api-Token': '3b2cbc13365bf8b66ae8bbfd466d30c3bffb1afb'}
    
    data = {
        'user_id': form['no_hp'],
        'nickname': form['nama'],
        'issue_access_token': True,
        'profile_url': ""
    }
    res = requests.post('https://api.sendbird.com/v3/users', 
                        headers=api_headers, 
                        json=data)

    return json.loads(res.text)


def login_user(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    user = Users.query.filter_by(no_hp=form['no_hp']).first()
    if not user:
        ret['err'] = 'user_not_found'
        return ret

    if not user.is_verified:
        ret['err'] = 'user_not_verified'
        return ret

    if user.password != encode_password(form['password']):
        ret['err'] = 'input_not_match'
        return ret

    fcm_token = form['fcm_token']
    user.fcm_token = fcm_token
    db.session.commit()
    sendbird_data = get_sendbird_user(form['no_hp'])
    ret['is_success'] = True
    ret['data'] = user.json_format()
    ret['data']['sendbird'] = sendbird_data
    return ret


def get_sendbird_user(no_hp):
    api_headers = {'Api-Token': '3b2cbc13365bf8b66ae8bbfd466d30c3bffb1afb'}
    
    url = 'https://api.sendbird.com/v3/users/{}'.format(no_hp)
    res = requests.get(
        url, 
        headers=api_headers
    )

    return json.loads(res.text)


def get_user_menu(form):
    all_menu = Menu.query.filter_by(no_hp=form['no_hp']).all()

    arr = []
    for menu in all_menu:
        arr.append(menu.json_format())

    return arr


def new_transaction(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    pembeli = form['pembeli']
    penjual = form['penjual']
    harga_total = form['harga']
    status = 'pending'

    order = Orders.query.filter_by(hp_pedagang=penjual, hp_pembeli=pembeli, status=status).all()

    if(len(order) == 0):
        dic = {
            'hp_pedagang': penjual,
            'hp_pembeli': pembeli,
            'harga_total': harga_total,
            'status': status
        }

        menu_list = form['order_list']

        new_order = Orders(**dic)
        order_details = []
        for menu in menu_list:

            dic = {
                'id_menu': menu['menu_id'],
                'no_hp': penjual,
                'quantity': menu['amount']
            }

            new_detail = OrderDetails(**dic, order=new_order)
            order_details.append(new_detail)

        db.session.add(new_order)
        db.session.add_all(order_details)

        db.session.commit()

        ret['is_success'] = True
        # TODO: Make push notification to related seller
        send_notification(penjual, "Pembelian baru!", "Terdapat pembelian baru dari {} seharga {}".format(pembeli, harga_total))

    else:
        ret['err'] = 'Anda memiliki transaksi yang belum selesai dengan penjual tersebut'

    return ret


def order_list(phone_number, is_seller):
    ret = {'is_success': False, 'data': {}, 'err': ''}

    ret['is_seller'] = is_seller

    if is_seller:
        order_list = Orders.query.filter_by(hp_pedagang=phone_number).all()
        ret['is_success'] = True
    else:
        order_list = Orders.query.filter_by(hp_pembeli=phone_number).all()
        ret['is_success'] = True

    orders = []
    for order in order_list:
        order = order.json_format()

        if order['status'] != 'pending':
            continue

        no_hp, nama = get_actor(order, is_seller)
        order.pop('hp_pedagang', None)
        order.pop('hp_pembeli', None)

        order['actors'] = {}

        if is_seller:
            order['actors']['type'] = "pembeli"
            order['actors']['hp'] = no_hp
            order['actors']['nama'] = nama
        else:
            order['actors']['type'] = "pedagang"
            order['actors']['hp'] = no_hp
            order['actors']['nama'] = nama

        # details = order_details(order)
        # order['details'] = details
        orders.append(order)

    ret['data'] = orders
    return ret


def make_order_action(id, action):
    ret = {'is_success': False, 'data': {}, 'err': ''}

    if (action != 'accept' and action != 'reject'):
        ret['err'] = 'Unknown action'
        return ret

    temp = Orders.query.filter_by(id_order=id).first()

    if (temp is not None):

        if action == 'accept':
            status = 'accepted'
        else:
            status = 'rejected'
            pembeli = temp.hp_pembeli
            penjual = temp.hp_pedagang
            nama_penjual = Users.query.filter_by(no_hp=penjual).first().json_format()['nama']
            nama_pembeli = Users.query.filter_by(no_hp=pembeli).first().json_format()['nama']

            title = "Transaksi dibatalkan"
            message = "Transaksi antara {} dengan {} berhasil di batalkan".format(nama_pembeli, nama_penjual)

            send_notification(pembeli, title, message)
            send_notification(penjual, title, message)

        temp.status = status
        db.session.commit()
        ret['is_success'] = True

        # TODO: Make push notification to related buyer
    else:
        ret['err'] = "Order id is not existed"

    return ret


def get_actor(order, is_seller):

    hp_pembeli = order['hp_pembeli']
    hp_pedagang = order['hp_pedagang']

    if is_seller:
        nama_pembeli = Users.query.filter_by(no_hp=hp_pembeli).first().json_format()['nama']
        return hp_pembeli, nama_pembeli
    else:
        nama_pedagang = Users.query.filter_by(no_hp=hp_pedagang).first().json_format()['nama']
        return hp_pedagang, nama_pedagang


def order_details(id_order):
    ret = {'is_success': False, 'data': {}, 'err': ''}

    order = Orders.query.filter_by(id_order=id_order).first().json_format()
    details = OrderDetails.query.filter_by(id_order=order['id_order']).all()
    arr = []
    for menu in details:
        menu = menu.json_format()
        temp = Menu.query.filter_by(id_menu=menu['id_menu']).first().json_format()
        menu['nama'] = temp['nama_menu']
        menu['harga'] = temp['harga']
        menu['url_foto'] = temp['url_foto_menu']
        menu.pop('id_menu', None)
        arr.append(menu)

    ret['data']['order_details'] = arr
    ret['data']['order_overview'] = order
    ret['is_success'] = True
    return ret


def add_menu(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    try:
        list_id_menu = db.session.query(Menu.id_menu).filter_by(no_hp=form['no_hp']).all()
        dic = {
            'no_hp': form['no_hp'],
            'id_menu': max([(0,)] + list_id_menu)[0] + 1,
            'nama_menu': form['nama_menu'],
            'url_foto_menu': form['url'] if ('url' in form) else '',
            'harga': form['harga']
        }
        new_menu = Menu(**dic)
        db.session.add(new_menu)
        db.session.commit()

        ret['is_success'] = True
        ret['data'] = new_menu.json_format()
    except IntegrityError as e:
        db.session.rollback()
        ret['err'] = 'id_menu_already_exist'
    return ret


def edit_menu(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}

    menu = Menu.query.filter_by(no_hp=form['no_hp'], id_menu=form['id_menu']).first()
    if not menu:
        ret['err'] = 'menu_not_found'
        return ret

    print(menu.json_format())
    ret['is_success'] = True
    if 'nama_menu' in form:
        menu.nama_menu = form['nama_menu']
    if 'url' in form:
        menu.url_foto_menu = form['url']
    if 'harga' in form:
        menu.harga = form['harga']
    if 'is_ready' in form:
        menu.is_ready = form['is_ready']
    db.session.commit()

    ret['data'] = menu.json_format()
    return ret


def delete_menu(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    menu = Menu.query.filter_by(no_hp=form['no_hp'], id_menu=form['id_menu']).first()
    if not menu:
        ret['err'] = 'menu_not_found'
        return ret

    ret['is_success'] = True
    ret['data'] = menu.json_format()
    db.session.delete(menu)
    db.session.commit()
    return ret


def insert_event(form):
    ret = {'is_success': True, 'data': {}, 'err': ''}
    try:
        if form['waktu_mulai'] >= form['waktu_selesai']:
            ret['err'] = 'time_invalid'
            return ret

        list_id_event = db.session.query(Events.id_event).filter_by(no_hp=form['no_hp']).all()
        dic = {
            'no_hp': form['no_hp'],
            'id_event': max([(0,)] + list_id_event)[0] + 1,
            'nama_event': form['nama_event'],
            'deskripsi': form['deskripsi'],
            'waktu_mulai': form['waktu_mulai'],
            'waktu_selesai': form['waktu_selesai'],
            'location': form['location'] if ('location' in form) else None
        }
        new_event = Events(**dic)
        db.session.add(new_event)
        db.session.commit()
        ret['data'] = new_event.json_format()
        
    except IntegrityError as e:
        db.session.rollback()
        ret['is_success'] = False
        ret['err'] = 'duplicate_FK'

    return ret


def send_notification(no_hp, title, message):
    api_headers = {
        'Authorization': 'key=AAAApzfNGVM:APA91bGgHHDg90T2j3CIDk457UJ9-WUGakkKO4Lrdkg_vtQpN-s1kqmOHBwIDAT9qtaMolCLUAeYKDdlh6oz8PXhkXCptZ1PsNfVoFsSJjrjOa9krHBkZkDlp9j7iFwZWXCmbJgPdFR1',
        'Content-Type': 'application/json'
    }

    user = Users.query.filter_by(no_hp=no_hp).first()

    payload = {}
    payload['title'] = title
    payload['message'] = message

    data = {}
    data['to'] = user.fcm_token
    data['data'] = payload

    requests.post(
        'https://fcm.googleapis.com/fcm/send',
        headers=api_headers,
        json=data
    )


def ask_verify(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    user = Users.query.filter_by(no_hp=form['no_hp']).first()

    if not user:
        ret['err'] = 'user_not_found'
        return ret

    if user.is_verified:
        ret['err'] = 'already_verified'
        return ret

    # otp_now = totp.now()
    user.verification_code = random.randint(100000, 999999)
    db.session.commit()
    pesan = "Halo {nama}, berikut kode verifikasi Klilink kamu: {kode}".format(
        nama=user.nama,
        kode=user.verification_code
    )
    requests.get(sms_url(user.no_hp, pesan))

    ret['is_success'] = True
    ret['data'] = user.json_format()
    return ret


def verify_code(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    user = Users.query.filter_by(no_hp=form['no_hp']).first()

    if not user:
        ret['err'] = 'user_not_found'
        return ret

    if user.verification_code != form['verification_code']:
        ret['err'] = 'invalid_verification_code'
        return ret

    # if not totp.verify(form['verification_code']):
    #     ret['err'] = 'expired_verification_code'
    #     return ret

    user.is_verified = True
    user.verification_code = None
    db.session.commit()
    ret['is_success'] = True
    ret['data'] = user.json_format()
    return ret


def forgot_password(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    user = Users.query.filter_by(no_hp=form['no_hp']).first()

    if not user:
        ret['err'] = 'user_not_found'
        return ret

    user.reset_code = random.randint(100000, 999999)
    db.session.commit()
    pesan = "Halo {nama}, berikut token lupa password Klilink kamu: {kode}".format(
        nama=user.nama,
        kode=user.reset_code
    )
    # print(sms_url(user.no_hp, pesan))
    requests.get(sms_url(user.no_hp, pesan))

    ret['is_success'] = True
    ret['data'] = user.json_format()
    return ret


def reset_password(form):
    ret = {'is_success': False, 'data': {}, 'err': ''}
    user = Users.query.filter_by(no_hp=form['no_hp']).first()

    if not user:
        ret['err'] = 'user_not_found'
        return ret

    if user.reset_code != form['reset_code']:
        ret['err'] = 'invalid_reset_code'
        return ret

    user.reset_code = None
    user.password = encode_password(form['password'])
    db.session.commit()
    ret['is_success'] = True
    ret['data'] = user.json_format()
    return ret
