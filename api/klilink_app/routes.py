"""This module defines the routes of our app."""
from flask import request, jsonify, Blueprint
from klilink_app.models.services import *

api = Blueprint('api', __name__)


@api.route('/')
@api.route('/index/')
def index():
    """This function is just an index home page that returns welcome."""
    posts = "Welcome"
    return posts


@api.route('/user/profile/', methods=['GET', 'POST'])
def profile_api():
    if request.method == 'POST':
        ret = edit_profile(request.json)
    elif request.method == 'GET':
        ret = get_profile(request.args)
    return jsonify(ret)


@api.route('/user/pedagangs/')
def all_pedagang_api():
    return jsonify(get_all_pedagang())


@api.route('/auth/register/', methods=['POST'])
def register_api():
    return jsonify(insert_user(request.json))


@api.route('/auth/login/', methods=['POST'])
def login_api():
    return jsonify(login_user(request.json))


@api.route('/user/order/', methods=['GET'])
def get_transaction_list():
    phone = request.args.get('phone')
    is_seller = request.args.get('seller')

    if(is_seller == 'true'):
        is_seller = True
    else:
        is_seller = False

    return jsonify(order_list(phone, is_seller))


@api.route('/order/', methods=['POST'])
def make_transaction():
    return jsonify(new_transaction(request.json))


@api.route('/order/<int:id>', methods=['POST', 'GET'])
def order_action(id):

    if request.method == 'POST':
        action = request.args.get('action')
        return jsonify(make_order_action(id, action))
    elif request.method == 'GET':
        return jsonify(order_details(id))


@api.route('/user/menu/add/', methods=['POST'])
def add_menu_api():
    return jsonify(add_menu(request.json))


@api.route('/user/menu/edit/', methods=['POST'])
def edit_menu_api():
    return jsonify(edit_menu(request.json))


@api.route('/user/menu/delete/', methods=['POST'])
def delete_menu_api():
    return jsonify(delete_menu(request.json))


@api.route('/user/menu/')
def user_menu_api():
    return jsonify(get_user_menu(request.args))


@api.route('/maps/events/', methods=['GET', 'POST'])
def insert_events_api():
    if request.method == 'GET':
        return jsonify(get_all_event())

    elif request.method == 'POST':
        return jsonify(insert_event(request.json))


@api.route('/auth/password/', methods=['GET', 'POST'])
def forgot_password_api():
    if request.method == 'GET':
        return jsonify(forgot_password(request.args))

    elif request.method == 'POST':
        return jsonify(reset_password(request.json))


@api.route('/auth/verify/', methods=['GET', 'POST'])
def verify_api():
    if request.method == 'GET':
        return jsonify(ask_verify(request.args))

    elif request.method == 'POST':
        return jsonify(verify_code(request.json))
