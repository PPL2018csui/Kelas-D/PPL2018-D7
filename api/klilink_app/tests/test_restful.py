import unittest
import os
from klilink_app import app, db
from klilink_app.models.services import insert_user
from klilink_app.models.seed import Seed
import json


class RestfulTests(unittest.TestCase):

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DB_URL_TEST')
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    # helper function
    def get_user_response(self, no_hp):
        url = '/user/profile/?no_hp={}'.format(no_hp)
        return self.app.get(url, follow_redirects=True)

    def get_user_menu_response(self, no_hp):
        url = '/user/menu/?no_hp={}'.format(no_hp)
        return self.app.get(url, follow_redirects=True)

    def post_add_menu_response(self, no_hp):
        url = '/user/menu/add/'
        response = self.app.post(
            url,
            data=json.dumps(dict(
                no_hp=no_hp,
                nama_menu='menu test',
                harga=1000
            )),
            content_type='application/json',
            follow_redirects=True
        )
        return response

    def post_edit_profile_user_response(self, no_hp):
        url = '/user/profile/'
        response = self.app.post(
            url,
            data=json.dumps(dict(
                no_hp=no_hp,
                nama='nama test lain',
                password='password lain'
            )),
            content_type='application/json',
            follow_redirects=True
        )
        return response

    def post_insert_user_response(self, no_hp):
        url = '/auth/register/'
        response = self.app.post(
            url,
            data=json.dumps(dict(
                no_hp=no_hp,
                nama='nama test',
                password='password',
                role='pedagang'
            )),
            content_type='application/json',
            follow_redirects=True
        )
        return response

    def post_login_response(self, no_hp, password):
        url = '/auth/login/'
        response = self.app.post(
            url,
            data=json.dumps(dict(
                no_hp=no_hp,
                password=password
            )),
            content_type='application/json',
            follow_redirects=True
        )
        return response

    # testing function
    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Welcome', response.data)

    def test_get_user_positive_pembeli(self):
        no_hp = "081122334400"
        form = {
            'no_hp': no_hp,
            'nama': 'nama test',
            'password': 'testpassword',
            'role': 'pembeli'
        }
        with app.app_context():
            insert_user(form)
        response = self.get_user_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)
        self.assertIn(b'"role": "pembeli"', response.data)

    def test_get_user_positive_pedagang(self):
        no_hp = "081122334401"
        form = {
            'no_hp': no_hp,
            'nama': 'nama test',
            'password': 'testpassword',
            'role': 'pedagang'
        }
        with app.app_context():
            insert_user(form)
        response = self.get_user_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)
        self.assertIn(b'"role": "pedagang"', response.data)

    def test_get_user_negative(self):
        no_hp = "01"
        response = self.get_user_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)

    def test_get_all_pedagang_positive(self):
        url = '/user/pedagangs/'
        response = self.app.get(url, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_edit_profile_positive(self):
        no_hp = "081122334402"
        form = {
            'no_hp': no_hp,
            'nama': 'nama test',
            'password': 'testpassword',
            'role': 'pedagang'
        }
        with app.app_context():
            insert_user(form)
        old_user = self.get_user_response(no_hp)
        response = self.post_edit_profile_user_response(no_hp)

        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)
        self.assertNotEqual(old_user.data, response.data)

    def test_edit_profile_negative(self):
        no_hp = "01"
        response = self.post_edit_profile_user_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)
        self.assertIn(b'"err": "user_not_found"', response.data)

    def test_events(self):
        url = '/maps/events/'
        response = self.app.get(url, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_insert_user(self):
        no_hp = "081122334403"
        response = self.post_insert_user_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)

    def test_insert_user_duplicate(self):
        no_hp = "081122334404"
        self.post_insert_user_response(no_hp)
        response = self.post_insert_user_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)
        self.assertIn(b'"err": "duplicate_FK"', response.data)

    def test_login(self):
        no_hp = "081122334405"
        password = 'testpassword'
        form = {
            'no_hp': no_hp,
            'nama': 'nama test',
            'password': password,
            'role': 'pedagang'
        }
        with app.app_context():
            insert_user(form)
        response = self.post_login_response(no_hp, password)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)

    def test_login_not_match(self):
        no_hp = "081122334406"
        password = 'testpassword'
        form = {
            'no_hp': no_hp,
            'nama': 'nama test',
            'password': password,
            'role': 'pedagang'
        }
        with app.app_context():
            insert_user(form)
        response = self.post_login_response(no_hp, 'passwordasal')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)
        self.assertIn(b'"err": "input_not_match"', response.data)

    def test_login_no_user(self):
        response = self.post_login_response('01', 'passwordasal')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)
        self.assertIn(b'"err": "user_not_found"', response.data)

    def test_user_menu(self):
        with app.app_context():
            Seed(db)
        no_hp = '081234567893'
        response = self.get_user_menu_response(no_hp)
        self.assertEqual(response.status_code, 200)

    def test_user_menu_no_user(self):
        no_hp = '01'
        response = self.get_user_menu_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'[]', response.data)

    def test_add_menu_positive(self):
        no_hp = '081234567893'
        response = self.post_add_menu_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)

    def test_add_menu_no_user(self):
        no_hp = '01'
        response = self.post_add_menu_response(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)
        self.assertIn(b'"err": "user_not_found"', response.data)

    def post_insert_events(self, no_hp):
        url = '/maps/events/'
        response = self.app.post(
            url,
            data=json.dumps(dict(
                no_hp=no_hp,
                id_event='9',
                nama_event='test aja',
                deskripsi='test aja',
                waktu_mulai='2018-4-21 10:00:00',
                waktu_selesai='2018-4-30 13:30:10',
                location='-6.363100, 106.820415'
            )),
            content_type='application/json',
            follow_redirects=True
        )
        return response

    def test_insert_event(self):
        no_hp = "081122334400"
        response = self.post_insert_events(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": true', response.data)

    def test_insert_event_negative(self):
        no_hp = "085814759059"
        self.post_insert_events(no_hp)
        response = self.post_insert_events(no_hp)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'"is_success": false', response.data)
        self.assertIn(b'"err": "no_matching_FK"', response.data)
