from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from klilink_app import app, db
from klilink_app.models.seed import Seed
from klilink_app.config import Config, TestingConfig
import subprocess

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


@manager.command
def drop():
    db.drop_all()


@manager.command
def seed():
    Seed(db)


@manager.command
def lint():
    subprocess.call('flake8 --ignore=E501,F403,F405,W293,W291 --exclude=migrations', shell=True)


@manager.command
def unittest():
    app.config.from_object(TestingConfig)
    Seed(db)
    subprocess.call(
        'nose2 -C --coverage klilink_app klilink_app.tests.test_restful',
        shell=True
    )
    db.drop_all()
    app.config.from_object(Config)


@manager.command
def runserver():
    app.run(debug=True)


if __name__ == '__main__':
    manager.run()
