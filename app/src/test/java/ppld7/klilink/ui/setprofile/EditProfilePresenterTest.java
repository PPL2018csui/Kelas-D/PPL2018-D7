package ppld7.klilink.ui.setprofile;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;

import ppld7.klilink.data.network.api.ProyekinApi;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.model.request.profile.EditProfileRequest;
import ppld7.klilink.data.network.model.response.login.LoginResponse;
import ppld7.klilink.data.network.model.response.profile.EditProfileResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;


/**
 * Created by Farhan on 19/04/2018.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Response.class, FirebaseDatabase.class})
@PowerMockIgnore("javax.net.ssl.*")
public class EditProfilePresenterTest{

    private EditProfileMvpView editProfileMvpViewMock;
    private DataManager mockDataManager;
    private EditProfileRequest editProfileRequestMock;
    private EditProfilePresenter<EditProfileMvpView> editProfilePresenter;
    private ProyekinApi apiMock;
    private UserDataModel dataModelMock;

    @Before
    public void setup() {
        mockDataManager = PowerMockito.mock(DataManager.class);
        editProfileMvpViewMock = PowerMockito.mock(EditProfileMvpView.class);
        editProfileRequestMock = PowerMockito.mock(EditProfileRequest.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);
        dataModelMock = PowerMockito.mock(UserDataModel.class);

        doReturn(build()).when(mockDataManager).getRetrofit();
        editProfilePresenter = new EditProfilePresenter<>(mockDataManager);
        editProfilePresenter.onAttach(editProfileMvpViewMock);
        editProfilePresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testPrefillData() {
        editProfilePresenter.getPrefill();
        verify(editProfileMvpViewMock, times(1))
                .prefillData(anyString(), anyString());
    }

    @Test
    public void testUpdateProfile() {
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        editProfilePresenter.updateProfile(userDataModel);
        verify(mockDataManager, times(1))
                .saveUserData(userDataModel);
    }

    @Test
    public void testEditProfileFailed() throws Exception {

        final Call<EditProfileResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.editProfile(editProfileRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EditProfileResponse> callback = invocation.getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        editProfilePresenter.onSaveButtonClick(editProfileRequestMock);

        verify(editProfileMvpViewMock, times(1))
                .editProfileFailed(anyString());
    }

    @Test
    public void testEditProfileResponseNotSuccess() throws Exception {
        final Call<EditProfileResponse> callMock = PowerMockito.mock(Call.class);
        final Response<EditProfileResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.editProfile(editProfileRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EditProfileResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        editProfilePresenter.onSaveButtonClick(editProfileRequestMock);

        verify(editProfileMvpViewMock, times(1))
                .editProfileFailed(anyString());
    }

    @Test
    public void testEditProfileResponseSuccess() throws Exception {
        final Call<EditProfileResponse> callMock = PowerMockito.mock(Call.class);
        final Response<EditProfileResponse> responseMock = PowerMockito.mock(Response.class);

        final EditProfileResponse response = PowerMockito.mock(EditProfileResponse.class);

        when(apiMock.editProfile(editProfileRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EditProfileResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        editProfilePresenter.onSaveButtonClick(editProfileRequestMock);

        verify(editProfileMvpViewMock, times(1))
                .editProfileSuccess(response.getData());
    }

    @Test
    public void testUpdateProfilePictureFirebase() throws Exception {
        FirebaseDatabase firebaseDatabase = PowerMockito.mock(FirebaseDatabase.class);
        DatabaseReference databaseReference = PowerMockito.mock(DatabaseReference.class);

        PowerMockito.mockStatic(FirebaseDatabase.class);
        BDDMockito.given(FirebaseDatabase.getInstance()).willReturn(firebaseDatabase);
        Mockito.when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        Mockito.when(databaseReference.child(anyString())).thenReturn(databaseReference);

        editProfilePresenter.updateProfilePictureFirebase();
        verify(databaseReference, times(1))
                .updateChildren(any(HashMap.class));
    }
}