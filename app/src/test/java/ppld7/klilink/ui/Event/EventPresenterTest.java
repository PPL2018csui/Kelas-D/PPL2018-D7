package ppld7.klilink.ui.Event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.event.EventRequest;
import ppld7.klilink.data.network.model.response.event.EventResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by farhan on 12/05/2018.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class EventPresenterTest{
    private EventMvpView mvpViewMock;
    private DataManager mockDataManager;
    private EventRequest requestMock;
    private Retrofit retrofit;
    private EventPresenter<EventMvpView> presenter;
    private ProyekinApi apiMock;


    @Before
    public void setup() {
        mockDataManager = PowerMockito.mock(DataManager.class);
        mvpViewMock = PowerMockito.mock(EventMvpView.class);
        requestMock = PowerMockito.mock(EventRequest.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(mockDataManager).getRetrofit();
        presenter = new EventPresenter<>(mockDataManager);
        presenter.onAttach(mvpViewMock);
        presenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testPrefillData() {
        presenter.getPrefill();
        verify(mvpViewMock, times(1))
                .prefillData(anyString());
    }

    @Test
    public void testAddEventFailed() throws Exception {

        final Call<EventResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.addEvent(requestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EventResponse> callback = invocation.getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.onAddButtonClick(requestMock);

        verify(mvpViewMock, times(1))
                .addEventFailed(anyString());
    }

    @Test
    public void testAddEventResponseNotSuccess() throws Exception {
        final Call<EventResponse> callMock = PowerMockito.mock(Call.class);
        final Response<EventResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.addEvent(requestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EventResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.onAddButtonClick(requestMock);

        verify(mvpViewMock, times(1))
                .addEventFailed(anyString());
    }

    @Test
    public void testAddEventResponseSuccess() throws Exception {
        final Call<EventResponse> callMock = PowerMockito.mock(Call.class);
        final Response<EventResponse> responseMock = PowerMockito.mock(Response.class);

        final EventResponse response = PowerMockito.mock(EventResponse.class);

        when(apiMock.addEvent(requestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EventResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.onAddButtonClick(requestMock);

        verify(mvpViewMock, times(1))
                .addEventSuccess();
    }
}