package ppld7.klilink.ui.login;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.login.LoginRequest;
import ppld7.klilink.data.network.model.response.SendbirdData;
import ppld7.klilink.data.network.model.response.login.LoginData;
import ppld7.klilink.data.network.model.response.login.LoginResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Ibam on 4/11/2018.
 *
 * Re-developed by Fadhil on 17-Apr-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Response.class, FirebaseDatabase.class})
@PowerMockIgnore("javax.net.ssl.*")
public class LoginPresenterTest {

    private LoginMvpView loginMvpViewMock;
    private DataManager mMockDataManager;
    private LoginRequest loginRequestMock;
    private LoginPresenter<LoginMvpView> loginPresenter;
    private ProyekinApi apiMock;

    @Before
    public void setUp() throws Exception {
        mMockDataManager = PowerMockito.mock(DataManager.class);
        loginMvpViewMock = PowerMockito.mock(LoginMvpView.class);
        loginRequestMock = PowerMockito.mock(LoginRequest.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(mMockDataManager).getRetrofit();
        loginPresenter = new LoginPresenter<>(mMockDataManager);
        loginPresenter.onAttach(loginMvpViewMock);
        loginPresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testSaveUserData() {
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        loginPresenter.setUserLoggedIn(userDataModel);
        verify(mMockDataManager, times(1))
                .saveUserData(userDataModel);
    }

    @Test
    public void testLoginFailed() throws Exception {
        final Call<LoginResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.login(loginRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<LoginResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        loginPresenter.onServerLoginClick(loginRequestMock);
        verify(loginMvpViewMock, times(1))
                .onLoginFailed(anyString());
    }

    @Test
    public void testLoginResponseNotSuccess() throws Exception {
        final Call<LoginResponse> callMock = PowerMockito.mock(Call.class);
        final Response<LoginResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.login(loginRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<LoginResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        loginPresenter.onServerLoginClick(loginRequestMock);

        verify(loginMvpViewMock, times(1))
                .onLoginFailed(anyString());
    }

    @Test
    public void testLoginResponseEmpty() throws Exception {
        final Call<LoginResponse> callMock = PowerMockito.mock(Call.class);
        final Response<LoginResponse> responseMock = PowerMockito.mock(Response.class);

        final LoginResponse response = PowerMockito.mock(LoginResponse.class);

        when(apiMock.login(loginRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<LoginResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        loginPresenter.onServerLoginClick(loginRequestMock);

        verify(loginMvpViewMock, times(1))
                .onLoginFailed(anyString());
    }

    @Test
    public void testLoginResponseSuccess() throws Exception {
        final Call<LoginResponse> callMock = PowerMockito.mock(Call.class);
        final Response<LoginResponse> responseMock = PowerMockito.mock(Response.class);

        final LoginResponse response = PowerMockito.mock(LoginResponse.class);

        when(apiMock.login(loginRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<LoginResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        loginPresenter.onServerLoginClick(loginRequestMock);

        verify(loginMvpViewMock, times(1))
                .onLoginSuccess(any(LoginData.class));
    }

    @Test
    public void testSaveSendbirdData() throws Exception {
        loginPresenter.saveSendbirdData(any(SendbirdData.class));
    }

    @Test
    public void testUpdateProfilePictureFirebase() throws Exception {
        FirebaseDatabase firebaseDatabase = PowerMockito.mock(FirebaseDatabase.class);
        DatabaseReference databaseReference = PowerMockito.mock(DatabaseReference.class);

        PowerMockito.mockStatic(FirebaseDatabase.class);
        BDDMockito.given(FirebaseDatabase.getInstance()).willReturn(firebaseDatabase);
        when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        when(databaseReference.child(anyString())).thenReturn(databaseReference);


        loginPresenter.updateProfilePictureFirebase();

        PowerMockito.verifyStatic();
        FirebaseDatabase.getInstance();
        verify(databaseReference, times(1))
                .updateChildren(any(HashMap.class));
    }
}