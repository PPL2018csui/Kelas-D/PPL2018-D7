package ppld7.klilink.ui.login;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.login.ResetPasswordRequest;
import ppld7.klilink.data.network.model.response.login.PasswordTokenResponse;
import ppld7.klilink.data.network.model.response.login.ResetPasswordResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Fadhil on 03-Jun-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class ResetPasswordPresenterTest {
    private ResetPasswordMvpView mvpView;
    private ResetPasswordPresenter<ResetPasswordMvpView> presenter;
    private ProyekinApi apiMock;
    private DataManager dataManager;
    private ResetPasswordRequest resetPasswordRequestMock;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(ResetPasswordMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);
        resetPasswordRequestMock = PowerMockito.mock(ResetPasswordRequest.class);

        doReturn(build()).when(dataManager).getRetrofit();

        presenter = new ResetPasswordPresenter<>(dataManager);
        presenter.onAttach(mvpView);
        presenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testResetPasswordFailure() throws Exception {
        final Call<ResetPasswordResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.resetPassword(resetPasswordRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<ResetPasswordResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.resetPass(resetPasswordRequestMock);

        verify(mvpView, times(1))
                .onResetPasswordFailed(anyString());
    }

    @Test
    public void testResetPasswordNotSuccess() throws Exception {
        final Call<ResetPasswordResponse> callMock = PowerMockito.mock(Call.class);
        final Response<ResetPasswordResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.resetPassword(resetPasswordRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<ResetPasswordResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.resetPass(resetPasswordRequestMock);

        verify(mvpView, times(1))
                .onResetPasswordFailed(anyString());
    }

    @Test
    public void testResetPasswordSuccess() throws Exception {
        final Call<ResetPasswordResponse> callMock = PowerMockito.mock(Call.class);
        final Response<ResetPasswordResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.resetPassword(resetPasswordRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<ResetPasswordResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.resetPass(resetPasswordRequestMock);

        verify(mvpView, times(1))
                .onResetPaasswordSuccess(any(ResetPasswordResponse.class));
    }
}