package ppld7.klilink.ui.main.maps;

import com.google.android.gms.maps.model.LatLng;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;

/**
 * Created by Fadhil on 28-Mar-18.
 */
public class MapFragmentTest {
    @Mock
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onCreateView() throws Exception {
    }

    @Test
    public void onMapReady() throws Exception {
    }

    @Test
    public void onSaveInstanceState() throws Exception {
    }

    @Test
    public void setUpDummyLocations() throws Exception {

        assertEquals(2, 1 + 1);
    }
}