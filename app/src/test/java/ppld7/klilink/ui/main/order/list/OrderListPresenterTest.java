package ppld7.klilink.ui.main.order.list;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Fadhil on 26-May-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class OrderListPresenterTest {
    private OrderListPresenter<OrderListMvpView> mPresenter;
    private OrderListMvpView mvpView;
    private DataManager dataManager;
    private ProyekinApi apiMock;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(OrderListMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(dataManager).getRetrofit();
        mPresenter = new OrderListPresenter<>(dataManager);
        mPresenter.onAttach(mvpView);
        mPresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testOrderListFailure() throws Exception {
        final Call<OrderListResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.getOrderList(anyString(), anyString())).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderListResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.requestOrderList(anyString(), anyString());

        verify(mvpView, times(1))
                .onRequestOrderListFailed(anyString());
    }

    @Test
    public void testOrderListNotSuccess() throws Exception {
        final Call<OrderListResponse> callMock = PowerMockito.mock(Call.class);
        final Response<OrderListResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.getOrderList(anyString(), anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderListResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.requestOrderList(anyString(), anyString());

        verify(mvpView, times(1))
                .onRequestOrderListFailed(anyString());
    }

    @Test
    public void testOrderListSuccess() throws Exception {
        final Call<OrderListResponse> callMock = PowerMockito.mock(Call.class);
        final Response<OrderListResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.getOrderList(anyString(), anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderListResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.requestOrderList(anyString(), anyString());

        verify(mvpView, times(1))
                .onRequestOrderListSuccess(any(OrderListResponse.class));
    }

    @Test
    public void testExecuteOrderActionFailure() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.executeOrderAction(anyInt(), anyString())).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.executeOrderAction(anyInt(), anyString());

        verify(mvpView, times(1))
                .onExecuteOrderActionFailed(anyString());
    }

    @Test
    public void testExecuteOrderActionNotSuccess() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final Response<GenericResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.executeOrderAction(anyInt(), anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.executeOrderAction(anyInt(), anyString());

        verify(mvpView, times(1))
                .onExecuteOrderActionFailed(anyString());
    }

    @Test
    public void testExecuteOrderActionSuccess() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final Response<GenericResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.executeOrderAction(anyInt(), anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.executeOrderAction(anyInt(), anyString());

        verify(mvpView, times(1))
                .onExecuteOrderActionSuccess(any(GenericResponse.class));
    }
}