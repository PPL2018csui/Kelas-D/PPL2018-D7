package ppld7.klilink.ui.main.profile;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Dwi Nanda on 25/04/2018.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Response.class, FirebaseDatabase.class})
@PowerMockIgnore("javax.net.ssl.*")
public class ProfilePresenterTest {
    private ProfilePresenter<ProfileMvpView> profilePresenter;
    private ProfileMvpView profileMvpView;
    private DataManager dataManager;
    private ProyekinApi apiMock;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        profileMvpView = PowerMockito.mock(ProfileMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(dataManager).getRetrofit();
        profilePresenter = new ProfilePresenter<>(dataManager);
        profilePresenter.onAttach(profileMvpView);
        profilePresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testOnProfileRequestSeller(){
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        when(dataManager.getUserModel()).thenReturn(userDataModel);
        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(true);
        profilePresenter.onProfileRequest();
        verify(profileMvpView, times(1)).onRequestProfilePenjualSuccess(userDataModel);
    }

    @Test
    public void testOnProfileRequestBuyer(){
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        when(dataManager.getUserModel()).thenReturn(userDataModel);
        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(false);
        profilePresenter.onProfileRequest();
        verify(profileMvpView, times(1)).onRequestProfilePembeliSuccess(userDataModel);
    }

    @Test
    public void testOnProfileRequestNotLogin(){
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        when(dataManager.getUserModel()).thenReturn(userDataModel);
        when(dataManager.isLoggedIn()).thenReturn(false);
        profilePresenter.onProfileRequest();
        verify(profileMvpView, times(1)).onRequestProfileFailed();
      }

    @Test
    public void testGetMenuEmptyPhone() throws Exception {
        when(dataManager.getCurrentUserPhone()).thenReturn(null);
        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(true);
        profilePresenter.getMenuPedagang();
        verify(profileMvpView, times(1))
                .onLoadMenuError(anyString());
    }

    @Test
    public void testGetMenuNotLogin() throws Exception {
        when(dataManager.getCurrentUserPhone()).thenReturn("test");
        when(dataManager.isLoggedIn()).thenReturn(false);
        when(dataManager.isSeller()).thenReturn(true);

        profilePresenter.getMenuPedagang();
    }

    @Test
    public void testGetMenuNotSeller() throws Exception {
        when(dataManager.getCurrentUserPhone()).thenReturn("test");
        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(false);

        profilePresenter.getMenuPedagang();
    }

    @Test
    public void testGetMenuFailure() throws Exception {
        final Call<List<FoodResponse>> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(dataManager.getCurrentUserPhone()).thenReturn("test");
        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(true);
        when(apiMock.getFoodList(anyString())).thenReturn(callMock);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<FoodResponse>> callback = invocation
                                .getArgumentAt(0, Callback.class);
                        callback.onFailure(callMock, throwable);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        profilePresenter.getMenuPedagang();

        verify(profileMvpView, times(1))
                .onLoadMenuError(anyString());
    }

    @Test
    public  void testLogOut() throws Exception {
        profilePresenter.logOut();
        verify(dataManager, times(1))
                .setUserAsLoggedOut();
        verify(profileMvpView, times(1))
                .onLoggedOut();
    }

    @Test
    public  void testGetMenuNotSuccess() throws Exception {
        final Call<List<FoodResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<FoodResponse>> responseMock = PowerMockito.mock(Response.class);

        when(dataManager.getCurrentUserPhone()).thenReturn("test");
        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(true);
        when(apiMock.getFoodList(anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);
        when(responseMock.code()).thenReturn(400);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<FoodResponse>> callback = invocation
                                .getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        profilePresenter.getMenuPedagang();

        verify(profileMvpView, times(1))
                .onLoadMenuError(anyString());
    }

    @Test
    public  void testGetMenuSuccess() throws Exception {
        final Call<List<FoodResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<FoodResponse>> responseMock = PowerMockito.mock(Response.class);
        final List<FoodResponse> response = new ArrayList<>();

        when(dataManager.isLoggedIn()).thenReturn(true);
        when(dataManager.isSeller()).thenReturn(true);
        when(dataManager.getCurrentUserPhone()).thenReturn("test");
        when(apiMock.getFoodList(anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<FoodResponse>> callback = invocation
                                .getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        profilePresenter.getMenuPedagang();

        verify(profileMvpView, times(1))
                .onMenuLoaded(response);
    }

    @Test
    public void testRemoveFromFirebaseNotSeller() throws Exception {
        PowerMockito.when(profilePresenter.isSeller()).thenReturn(false);
        profilePresenter.removeFromFirebase();
    }

    @Test
    public void testRemoveFromFirebaseSeller() throws Exception {
        UserDataModel userDataModelMock = PowerMockito.mock(UserDataModel.class);

        FirebaseDatabase firebaseDatabase = PowerMockito.mock(FirebaseDatabase.class);
        DatabaseReference databaseReference = PowerMockito.mock(DatabaseReference.class);

        PowerMockito.mockStatic(FirebaseDatabase.class);
        BDDMockito.given(FirebaseDatabase.getInstance()).willReturn(firebaseDatabase);
        Mockito.when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        Mockito.when(databaseReference.child(anyString())).thenReturn(databaseReference);

        PowerMockito.when(dataManager.getUserModel()).thenReturn(userDataModelMock);
        PowerMockito.when(profilePresenter.isSeller()).thenReturn(true);
        PowerMockito.when(userDataModelMock.getNomorTelepon()).thenReturn("");

        PowerMockito.when(FirebaseDatabase.getInstance()).thenReturn(firebaseDatabase);
        PowerMockito.when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        PowerMockito.when(databaseReference.child("")).thenReturn(databaseReference);

        profilePresenter.removeFromFirebase();
        verify(databaseReference, times(1))
                .removeValue();
    }
}
