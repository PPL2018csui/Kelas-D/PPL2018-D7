package ppld7.klilink.ui.main.order.chat;

import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.model.response.SendbirdData;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Fadhil on 03-Jun-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({SendBird.class, SendBirdException.class})
@SuppressStaticInitializationFor("com.sendbird.android.SendBird")
@PowerMockIgnore("javax.net.ssl.*")
public class ChatPresenterTest {
    private ChatMvpView mvpView;
    private ChatPresenter<ChatMvpView> presenter;
    private DataManager dataManager;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(ChatMvpView.class);

        presenter = new ChatPresenter<>(dataManager);
        presenter.onAttach(mvpView);
    }

    @Test
    public void testSendbirdConnectFailed() throws Exception {
        when(dataManager.getSendbirdData()).thenReturn(null);
        presenter.connectToSendbird();
        verify(mvpView, times(1))
                .onConnectFailed(anyString());
    }

    @Test
    public void testSendbirdConnectNotSuccess() throws Exception {
        final SendbirdData data = PowerMockito.mock(SendbirdData.class);
        final User user = PowerMockito.mock(User.class);
        final SendBirdException exception = PowerMockito.mock(SendBirdException.class);

        when(dataManager.getSendbirdData()).thenReturn(data);

        PowerMockito.mockStatic(SendBird.class);
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                SendBird.ConnectHandler handler = invocation
                        .getArgumentAt(2, SendBird.ConnectHandler.class);
                handler.onConnected(user, exception);
                return null;
            }
        }).when(SendBird.class);
        SendBird.connect(anyString(), anyString(), any(SendBird.ConnectHandler.class));

        presenter.connectToSendbird();
        verify(mvpView, times(1))
                .onConnectFailed(anyString());
    }

    @Test
    public void testSendbirdConnectSuccess() throws Exception {
        final SendbirdData data = PowerMockito.mock(SendbirdData.class);
        final User user = PowerMockito.mock(User.class);

        when(dataManager.getSendbirdData()).thenReturn(data);

        PowerMockito.mockStatic(SendBird.class);
        PowerMockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                SendBird.ConnectHandler handler = invocation
                        .getArgumentAt(2, SendBird.ConnectHandler.class);
                handler.onConnected(user, null);
                return null;
            }
        }).when(SendBird.class);
        SendBird.connect(anyString(), anyString(), any(SendBird.ConnectHandler.class));

        presenter.connectToSendbird();
        verify(mvpView, times(1))
                .onConnectSuccess();
        verify(dataManager, times(1))
                .registerSendbirdPushToken();
    }
}