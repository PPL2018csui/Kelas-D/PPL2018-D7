package ppld7.klilink.ui.main.order;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.order.OrderRequest;
import ppld7.klilink.data.network.model.response.order.OrderResponse;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Fadhil on 18-May-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class OrderPresenterTest {
    private OrderPresenter<OrderMvpView> orderPresenter;
    private OrderMvpView orderMvpView;
    private DataManager dataManager;
    private ProyekinApi apiMock;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        orderMvpView = PowerMockito.mock(OrderMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(dataManager).getRetrofit();
        orderPresenter = new OrderPresenter<>(dataManager);
        orderPresenter.onAttach(orderMvpView);
        orderPresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testOrderMenuFailure() throws Exception {
        final Call<List<FoodResponse>> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.getFoodList(anyString())).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<List<FoodResponse>> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        orderPresenter.requestMenu(anyString());

        verify(orderMvpView, times(1))
                .onRequestMenuFailed(anyString());
    }

    @Test
    public void testOrderMenuNotSuccess() throws Exception {
        final Call<List<FoodResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<FoodResponse>> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.getFoodList(anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<List<FoodResponse>> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        orderPresenter.requestMenu(anyString());

        verify(orderMvpView, times(1))
                .onRequestMenuFailed(anyString());
    }

    @Test
    public void testOrderMenuSuccess() throws Exception {
        final Call<List<FoodResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<FoodResponse>> responseMock = PowerMockito.mock(Response.class);
        final List<FoodResponse> responses = new ArrayList<>();

        when(apiMock.getFoodList(anyString())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(responses);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<List<FoodResponse>> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        orderPresenter.requestMenu(anyString());

        verify(orderMvpView, times(1))
                .onRequestMenuSuccess(responses);
    }

    @Test
    public void testMakeOrderFailure() throws Exception {
        final Call<OrderResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.makeOrder(any(OrderRequest.class))).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        orderPresenter.makeOrder(any(OrderRequest.class));

        verify(orderMvpView, times(1))
                .onMakeOrderFailed(anyString());
    }

    @Test
    public void testMakeOrderNotSuccess() throws Exception {
        final Call<OrderResponse> callMock = PowerMockito.mock(Call.class);
        final Response<OrderResponse> responseMock = PowerMockito.mock(Response.class);
        final OrderResponse responses = new OrderResponse();

        when(apiMock.makeOrder(any(OrderRequest.class))).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        orderPresenter.makeOrder(any(OrderRequest.class));

        verify(orderMvpView, times(1))
                .onMakeOrderFailed(anyString());
    }

    @Test
    public void testMakeOrderSuccess() throws Exception {
        final Call<OrderResponse> callMock = PowerMockito.mock(Call.class);
        final Response<OrderResponse> responseMock = PowerMockito.mock(Response.class);
        final OrderResponse responses = new OrderResponse();

        when(apiMock.makeOrder(any(OrderRequest.class))).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(responses);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        orderPresenter.makeOrder(any(OrderRequest.class));

        verify(orderMvpView, times(1))
                .onMakeOrderSuccess(responses);
    }
}