package ppld7.klilink.ui.main.order.detail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetailResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doReturn;

/**
 * Created by Fadhil on 26-May-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class OrderDetailPresenterTest {
    private OrderDetailPresenter<OrderDetailMvpView> mPresenter;
    private OrderDetailMvpView mvpView;
    private DataManager dataManager;
    private ProyekinApi apiMock;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(OrderDetailMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(dataManager).getRetrofit();
        mPresenter = new OrderDetailPresenter<>(dataManager);
        mPresenter.onAttach(mvpView);
        mPresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testOrderDetailFailure() throws Exception {
        final Call<OrderDetailResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.getOrderDetail(anyInt())).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderDetailResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.requestOrderDetail(anyInt());

        verify(mvpView, times(1))
                .onRequestOrderDetailFailed(anyString());
    }

    @Test
    public void testOrderDetailNotSuccess() throws Exception {
        final Call<OrderDetailResponse> callMock = PowerMockito.mock(Call.class);
        final Response<OrderDetailResponse> responseMock = PowerMockito.mock(Response.class);


        when(apiMock.getOrderDetail(anyInt())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderDetailResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.requestOrderDetail(anyInt());

        verify(mvpView, times(1))
                .onRequestOrderDetailFailed(anyString());
    }

    @Test
    public void testOrderDetailSuccess() throws Exception {
        final Call<OrderDetailResponse> callMock = PowerMockito.mock(Call.class);
        final Response<OrderDetailResponse> responseMock = PowerMockito.mock(Response.class);


        when(apiMock.getOrderDetail(anyInt())).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<OrderDetailResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        mPresenter.requestOrderDetail(anyInt());

        verify(mvpView, times(1))
                .onRequestOrderDetailSuccess(any(OrderDetailResponse.class));
    }
}