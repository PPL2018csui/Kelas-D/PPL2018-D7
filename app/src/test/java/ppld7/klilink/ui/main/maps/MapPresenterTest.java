package ppld7.klilink.ui.main.maps;

import android.location.Location;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.maps.UpdateLocFirebase;
import ppld7.klilink.data.network.model.response.map.GetEventResponse;
import ppld7.klilink.data.network.model.response.map.GetPedagangResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Ibam on 3/28/2018.
 * <p>
 * Re-developed by Fadhil on 4/19/2018.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Response.class, FirebaseDatabase.class})
@PowerMockIgnore("javax.net.ssl.*")
public class MapPresenterTest {

    private MapMvpView mapMvpView;
    private DataManager mMockDataManager;
    private ProyekinApi apiMock;
    private MapPresenter<MapMvpView> mapPresenter;

    @Before
    public void setup() {
        mMockDataManager = PowerMockito.mock(DataManager.class);
        mapMvpView = PowerMockito.mock(MapMvpView.class);
        PowerMockito.mockStatic(FirebaseDatabase.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(mMockDataManager).getRetrofit();
        mapPresenter = new MapPresenter<>(mMockDataManager);
        mapPresenter.onAttach(mapMvpView);
        mapPresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testConstruct() {
        assertTrue(mapPresenter instanceof MapPresenter);
    }

    @Test
    public void testGetUserData() {
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        doReturn(userDataModel).when(mMockDataManager).getUserModel();
        mapPresenter.requestUserData();
        verify(mapMvpView, times(1))
                .onRequestUserData(userDataModel);
    }

    @Test
    public void testGetAllPedagangSuccess() throws Exception {
        final Call<List<GetPedagangResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<GetPedagangResponse>> responseMock = PowerMockito.mock((Response.class));

        final List<GetPedagangResponse> response = new ArrayList<>();
        response.add(new GetPedagangResponse(
                "test",
                "test",
                "test",
                "test",
                "test"
        ));

        when(apiMock.getAllPedagang()).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetPedagangResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
        }).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllPedagang();

        verify(mapMvpView, times(1))
                .onFetchDataSuccess(response);
    }

    @Test
    public void testGetAllPedagangNotSuccess() throws Exception {
        final Call<List<GetPedagangResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<GetPedagangResponse>> responseMock = PowerMockito.mock((Response.class));

        final List<GetPedagangResponse> response = new ArrayList<>();

        when(apiMock.getAllPedagang()).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetPedagangResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllPedagang();

        verify(mapMvpView, times(1))
                .onFetchDataError();
    }

    @Test
    public void testGetAllPedagangEmpty() throws Exception {
        final Call<List<GetPedagangResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<GetPedagangResponse>> responseMock = PowerMockito.mock((Response.class));

        final List<GetPedagangResponse> response = null;

        when(apiMock.getAllPedagang()).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetPedagangResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllPedagang();

        verify(mapMvpView, times(1))
                .onFetchDataError();
    }

    @Test
    public void testGetAllPedagangFailure() throws Exception {
        final Call<List<GetPedagangResponse>> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.getAllPedagang()).thenReturn(callMock);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetPedagangResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onFailure(callMock, throwable);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllPedagang();

        verify(mapMvpView, times(1))
                .onFetchDataError();
    }

    /**
     * Thanks to Ayaz From PPLD-5
     */
    @Test
    public void testGetAllEventSuccess() throws Exception {
        final Call<List<GetEventResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<GetEventResponse>> responseMock = PowerMockito.mock(Response.class);

        final List<GetEventResponse> response = new ArrayList<>();
        response.add(new GetEventResponse(
                "test",
                "test",
                "test",
                "test",
                "test",
                "test"
        ));

        when(apiMock.getAllEvent()).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetEventResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllEvent();

        verify(mapMvpView, times(1))
                .onFetchEventSuccess(response);
    }

    @Test
    public void testGetAllEventNotSuccess() throws Exception {
        final Call<List<GetEventResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<GetEventResponse>> responseMock = PowerMockito.mock(Response.class);

        final List<GetEventResponse> response = new ArrayList<>();

        when(apiMock.getAllEvent()).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetEventResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllEvent();

        verify(mapMvpView, times(1))
                .onFetchDataError();
    }

    @Test
    public void testGetAllEventEmpty() throws Exception {
        final Call<List<GetEventResponse>> callMock = PowerMockito.mock(Call.class);
        final Response<List<GetEventResponse>> responseMock = PowerMockito.mock(Response.class);

        final List<GetEventResponse> response = null;

        when(apiMock.getAllEvent()).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);

        doAnswer(
                new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        Callback<List<GetEventResponse>> callback =
                                invocation.getArgumentAt(0, Callback.class);
                        callback.onResponse(callMock, responseMock);
                        return null;
                    }
                }
        ).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllEvent();

        verify(mapMvpView, times(1))
                .onFetchDataError();
    }

    @Test
    public void testGetAllEventFailure() throws Exception {
        final Call<List<GetEventResponse>> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.getAllEvent()).thenReturn(callMock);
        doAnswer(
            new Answer() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    Callback<List<GetEventResponse>> callback =
                            invocation.getArgumentAt(0, Callback.class);
                    callback.onFailure(callMock, throwable);
                    return null;
                }
            }
        ).when(callMock).enqueue(any(Callback.class));

        mapPresenter.getAllEvent();

        verify(mapMvpView, times(1))
                .onFetchDataError();
    }

    @Test
    public void testUpdateLocationFirebaseSeller() throws Exception {
        UserDataModel userDataModelMock = PowerMockito.mock(UserDataModel.class);
        Location location = PowerMockito.mock(Location.class);

        FirebaseDatabase firebaseDatabase = PowerMockito.mock(FirebaseDatabase.class);
        DatabaseReference databaseReference = PowerMockito.mock(DatabaseReference.class);

        PowerMockito.mockStatic(FirebaseDatabase.class);
        BDDMockito.given(FirebaseDatabase.getInstance()).willReturn(firebaseDatabase);
        Mockito.when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        Mockito.when(databaseReference.child(anyString())).thenReturn(databaseReference);

        when(mMockDataManager.getUserModel()).thenReturn(userDataModelMock);
        when(mapPresenter.isSeller()).thenReturn(true);
        when(userDataModelMock.getNomorTelepon()).thenReturn("");

        when(FirebaseDatabase.getInstance()).thenReturn(firebaseDatabase);
        when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        when(databaseReference.child("")).thenReturn(databaseReference);

        mapPresenter.updateLocationFirebase(location);
        verify(databaseReference, times(1))
                .setValue(any(UpdateLocFirebase.class));
    }

    @Test
    public void testRemoveFromFirebaseSeller() throws Exception {
        UserDataModel userDataModelMock = PowerMockito.mock(UserDataModel.class);

        FirebaseDatabase firebaseDatabase = PowerMockito.mock(FirebaseDatabase.class);
        DatabaseReference databaseReference = PowerMockito.mock(DatabaseReference.class);

        PowerMockito.mockStatic(FirebaseDatabase.class);
        BDDMockito.given(FirebaseDatabase.getInstance()).willReturn(firebaseDatabase);
        Mockito.when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        Mockito.when(databaseReference.child(anyString())).thenReturn(databaseReference);

        when(mMockDataManager.getUserModel()).thenReturn(userDataModelMock);
        when(mapPresenter.isSeller()).thenReturn(true);
        when(userDataModelMock.getNomorTelepon()).thenReturn("");

        when(FirebaseDatabase.getInstance()).thenReturn(firebaseDatabase);
        when(firebaseDatabase.getReference(anyString())).thenReturn(databaseReference);
        when(databaseReference.child("")).thenReturn(databaseReference);

        mapPresenter.removeFromFirebase();
        verify(databaseReference, times(1))
                .removeValue();
    }

    @Test
    public void testUpdateLocationFirebaseNotSeller() throws Exception {
        Location location = PowerMockito.mock(Location.class);

        when(mapPresenter.isSeller()).thenReturn(false);

        mapPresenter.updateLocationFirebase(location);
    }

    @Test
    public void testRemoveFromFirebaseNotSeller() throws Exception {
        when(mapPresenter.isSeller()).thenReturn(false);
        mapPresenter.removeFromFirebase();
    }

    @Test
    public void testStartTrackingService() throws Exception {
        UserDataModel userDataModel = PowerMockito.mock(UserDataModel.class);
        when(mMockDataManager.getUserModel()).thenReturn(userDataModel);
        mapPresenter.startTrackingService();
        verify(mapMvpView, times(1)).startTrackingService(userDataModel);
    }

    @Test
    public void testSetupMapHeader() throws Exception {
        when(mMockDataManager.isSeller()).thenReturn(true);
        when(mMockDataManager.getCurrentUserName()).thenReturn("");
        mapPresenter.setupMapHeader();
        verify(mapMvpView, times(1))
                .setupMapHeader(anyBoolean(), anyString());
    }

    @Test
    public void testGetTrackingStatus() throws Exception {
        when(mMockDataManager.isTrackingEnabled()).thenReturn(true);
        assertTrue(mapPresenter.getTrackingStatus());
    }

    @Test
    public void testSetTrackingStatus() throws Exception {
        mapPresenter.setTrackingStatus(true);
        verify(mMockDataManager, times(1))
                .setTrackingStatus(anyBoolean());
    }
}
