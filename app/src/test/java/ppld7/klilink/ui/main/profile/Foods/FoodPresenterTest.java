package ppld7.klilink.ui.main.profile.Foods;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.ui.main.profile.ProfilePresenter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;

/**
 * Created by Fadhil on 11-May-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class FoodPresenterTest {
    private FoodPresenter foodPresenter;
    private DataManager dataManager;
    private FoodMvpView mvpView;
    private ProyekinApi apiMock;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        doReturn(build()).when(dataManager).getRetrofit();
        foodPresenter = new FoodPresenter(dataManager);
        foodPresenter.onAttach(mvpView);
        apiMock = PowerMockito.mock(ProyekinApi.class);
        foodPresenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testConstruct() throws Exception {
        assertTrue(foodPresenter instanceof FoodPresenter);
    }

    @Test
    public void testOnFoodRequest() throws Exception {
        foodPresenter.onFoodRequest();
    }
}