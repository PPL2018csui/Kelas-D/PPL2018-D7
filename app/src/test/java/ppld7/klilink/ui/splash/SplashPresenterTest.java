package ppld7.klilink.ui.splash;

import org.junit.Before;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import ppld7.klilink.data.DataManager;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Fadhil on 04-Jun-18.
 */
public class SplashPresenterTest {
    private SplashMvpView mvpView;
    private SplashPresenter<SplashMvpView> presenter;
    private DataManager dataManager;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(SplashMvpView.class);

        presenter = new SplashPresenter<>(dataManager);
        presenter.onAttach(mvpView);
    }

    @Test
    public void testDecideNextActivity() throws Exception {
        presenter.decideNextActivity();
        verify(mvpView, times(1))
                .decideNextActivity(anyBoolean());
    }
}