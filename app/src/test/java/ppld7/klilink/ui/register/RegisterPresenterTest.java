package ppld7.klilink.ui.register;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.register.RegisterRequest;
import ppld7.klilink.data.network.model.response.register.Data;
import ppld7.klilink.data.network.model.response.register.RegisterResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class RegisterPresenterTest {

    private Retrofit retrofit;
    private RegisterMvpView registerMvpViewMock;
    private DataManager mMockDataManager;
    private RegisterRequest registerRequestMock;
    private RegisterPresenter<RegisterMvpView> registerPresenter;
    private ProyekinApi apiMock;


    @Before
    public void setUp() throws Exception {
        mMockDataManager = PowerMockito.mock(DataManager.class);
        registerMvpViewMock = PowerMockito.mock(RegisterMvpView.class);
        registerRequestMock = PowerMockito.mock(RegisterRequest.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(getRetrofit()).when(mMockDataManager).getRetrofit();
        registerPresenter = new RegisterPresenter<>(mMockDataManager);
        registerPresenter.onAttach(registerMvpViewMock);
        registerPresenter.api = apiMock;
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-api.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    @Test
    public void registerFailed() {
        final Call<RegisterResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.register(registerRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<RegisterResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        registerPresenter.register(registerRequestMock);
        verify(registerMvpViewMock, times(1))
                .onRegisterFailed(anyString());
    }

    @Test
    public void register() throws Exception {
        final Call<RegisterResponse> callMock = PowerMockito.mock(Call.class);
        final Response<RegisterResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.register(registerRequestMock)).thenReturn(callMock);

        final RegisterResponse response = PowerMockito.mock(RegisterResponse.class);

        when(apiMock.register(registerRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<RegisterResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        registerPresenter.register(registerRequestMock);
        verify(registerMvpViewMock, times(1))
                .onRegisterSuccess(any(Data.class));
    }


    @Test
    public void testRegisterResponseNotSuccess() throws Exception {
        final Call<RegisterResponse> callMock = PowerMockito.mock(Call.class);
        final Response<RegisterResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.register(registerRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<RegisterResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        registerPresenter.register(registerRequestMock);

        verify(registerMvpViewMock, times(1))
                .onRegisterFailed(anyString());
    }

    @Test
    public void testRegisterResponseEmpty() throws Exception {
        final Call<RegisterResponse> callMock = PowerMockito.mock(Call.class);
        final Response<RegisterResponse> responseMock = PowerMockito.mock(Response.class);

        final RegisterResponse response = PowerMockito.mock(RegisterResponse.class);

        when(apiMock.register(registerRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<RegisterResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        registerPresenter.register(registerRequestMock);

        verify(registerMvpViewMock, times(1))
                .onRegisterFailed(anyString());
      }


    @Test
    public void storeSession() {
        final Data registerDataMock = PowerMockito.mock(Data.class);
        final String role = PowerMockito.mock(String.class);
        when(registerDataMock.getRole()).thenReturn("pedagang");
        when(role.equals(anyString())).thenReturn(true);
        when(registerDataMock.getNama()).thenReturn("");
        when(registerDataMock.getNoHp()).thenReturn("");
        registerPresenter.storeSession(registerDataMock);

        verify(mMockDataManager, times(1)).saveUserData(any(UserDataModel.class));
      }
}
