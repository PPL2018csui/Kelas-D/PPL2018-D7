package ppld7.klilink.ui.register.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.register.VerifyRequest;
import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.data.network.model.response.login.PasswordTokenResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Fadhil on 03-Jun-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class VerifyPresenterTest {
    private VerifyMvpView mvpView;
    private VerifyPresenter<VerifyMvpView> presenter;
    private ProyekinApi apiMock;
    private DataManager dataManager;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(VerifyMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(dataManager).getRetrofit();

        presenter = new VerifyPresenter<>(dataManager);
        presenter.onAttach(mvpView);
        presenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testVerifyCodeFailure() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final VerifyRequest requestMock = PowerMockito.mock(VerifyRequest.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);

        when(apiMock.verifyNoHp(requestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.verifyCode(requestMock);

        verify(mvpView, times(1))
                .onVerifyFailed(anyString());
    }

    @Test
    public void testVerifyCodeNotSuccess() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final VerifyRequest requestMock = PowerMockito.mock(VerifyRequest.class);
        final Response<GenericResponse> responseMock = PowerMockito.mock(Response.class);

        when(apiMock.verifyNoHp(requestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.verifyCode(requestMock);

        verify(mvpView, times(1))
                .onVerifyFailed(anyString());
    }

    @Test
    public void testVerifyCodeResponseNotSuccess() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final VerifyRequest requestMock = PowerMockito.mock(VerifyRequest.class);
        final Response<GenericResponse> responseMock = PowerMockito.mock(Response.class);
        final GenericResponse response = PowerMockito.mock(GenericResponse.class);

        when(apiMock.verifyNoHp(requestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.verifyCode(requestMock);

        verify(mvpView, times(1))
                .onVerifyFailed(anyString());
    }

    @Test
    public void testVerifyCodeResponseSuccess() throws Exception {
        final Call<GenericResponse> callMock = PowerMockito.mock(Call.class);
        final VerifyRequest requestMock = PowerMockito.mock(VerifyRequest.class);
        final Response<GenericResponse> responseMock = PowerMockito.mock(Response.class);
        final GenericResponse response = PowerMockito.mock(GenericResponse.class);

        when(apiMock.verifyNoHp(requestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);
        when(responseMock.body()).thenReturn(response);
        when(response.getIsSuccess()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<GenericResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.verifyCode(requestMock);

        verify(mvpView, times(1))
                .onVerifySuccess(anyString());
    }
}