package ppld7.klilink.ui.menu_add_edit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.data.network.model.request.profile.DeleteMenuRequest;
import ppld7.klilink.data.network.model.request.profile.EditMenuRequest;
import ppld7.klilink.data.network.model.response.profile.AddMenuResponse;
import ppld7.klilink.data.network.model.response.profile.DeleteMenuResponse;
import ppld7.klilink.data.network.model.response.profile.EditMenuResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Fadhil on 03-Jun-18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class MenuPresenterTest {
    private MenuMvpView mvpView;
    private MenuPresenter<MenuMvpView> presenter;
    private ProyekinApi apiMock;
    private DataManager dataManager;

    @Before
    public void setUp() throws Exception {
        dataManager = PowerMockito.mock(DataManager.class);
        mvpView = PowerMockito.mock(MenuMvpView.class);
        apiMock = PowerMockito.mock(ProyekinApi.class);

        doReturn(build()).when(dataManager).getRetrofit();

        presenter = new MenuPresenter<>(dataManager);
        presenter.onAttach(mvpView);
        presenter.api = apiMock;
    }

    private Retrofit build() {
        return new Retrofit.Builder()
                .baseUrl("https://klilink-development.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Test
    public void testEditMenuFailure() throws Exception {
        final Call<EditMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);
        final EditMenuRequest editMenuRequestMock = PowerMockito.mock(EditMenuRequest.class);

        when(apiMock.editMenu(editMenuRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EditMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.editMenu(editMenuRequestMock);

        verify(mvpView, times(1))
                .onEditMenuFailed(anyString());
    }

    @Test
    public void testAddMenuFailure() throws Exception {
        final Call<AddMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);
        final AddMenuRequest addMenuRequestMock = PowerMockito.mock(AddMenuRequest.class);

        when(apiMock.addMenu(addMenuRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<AddMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.addMenu(addMenuRequestMock);

        verify(mvpView, times(1))
                .onAddMenuFailed(anyString());
    }

    @Test
    public void testDeleteMenuFailure() throws Exception {
        final Call<DeleteMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Throwable throwable = PowerMockito.mock(Throwable.class);
        final DeleteMenuRequest deleteMenuRequestMock = PowerMockito.mock(DeleteMenuRequest.class);

        when(apiMock.deleteMenu(deleteMenuRequestMock)).thenReturn(callMock);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<DeleteMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onFailure(callMock, throwable);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.deleteMenu(deleteMenuRequestMock);

        verify(mvpView, times(1))
                .onDeleteMenuFailed(anyString());
    }

    @Test
    public void testEditMenuNotSuccess() throws Exception {
        final Call<EditMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Response<EditMenuResponse> responseMock = PowerMockito.mock(Response.class);
        final EditMenuRequest editMenuRequestMock = PowerMockito.mock(EditMenuRequest.class);

        when(apiMock.editMenu(editMenuRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EditMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.editMenu(editMenuRequestMock);

        verify(mvpView, times(1))
                .onEditMenuFailed(anyString());
    }

    @Test
    public void testAddMenuNotSuccess() throws Exception {
        final Call<AddMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Response<AddMenuResponse> responseMock = PowerMockito.mock(Response.class);
        final AddMenuRequest addMenuRequestMock = PowerMockito.mock(AddMenuRequest.class);

        when(apiMock.addMenu(addMenuRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<AddMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.addMenu(addMenuRequestMock);

        verify(mvpView, times(1))
                .onAddMenuFailed(anyString());
    }

    @Test
    public void testDeleteMenuNotSuccess() throws Exception {
        final Call<DeleteMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Response<DeleteMenuResponse> responseMock = PowerMockito.mock(Response.class);
        final DeleteMenuRequest deleteMenuRequestMock = PowerMockito.mock(DeleteMenuRequest.class);

        when(apiMock.deleteMenu(deleteMenuRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(false);


        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<DeleteMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.deleteMenu(deleteMenuRequestMock);

        verify(mvpView, times(1))
                .onDeleteMenuFailed(anyString());
    }

    @Test
    public void testEditMenuSuccess() throws Exception {
        final Call<EditMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Response<EditMenuResponse> responseMock = PowerMockito.mock(Response.class);
        final EditMenuRequest editMenuRequestMock = PowerMockito.mock(EditMenuRequest.class);

        when(apiMock.editMenu(editMenuRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<EditMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.editMenu(editMenuRequestMock);

        verify(mvpView, times(1))
                .onEditMenuSuccess();
    }

    @Test
    public void testAddMenuSuccess() throws Exception {
        final Call<AddMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Response<AddMenuResponse> responseMock = PowerMockito.mock(Response.class);
        final AddMenuRequest addMenuRequestMock = PowerMockito.mock(AddMenuRequest.class);

        when(apiMock.addMenu(addMenuRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<AddMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.addMenu(addMenuRequestMock);

        verify(mvpView, times(1))
                .onAddMenuSuccess();
    }

    @Test
    public void testDeleteMenuSuccess() throws Exception {
        final Call<DeleteMenuResponse> callMock = PowerMockito.mock(Call.class);
        final Response<DeleteMenuResponse> responseMock = PowerMockito.mock(Response.class);
        final DeleteMenuRequest deleteMenuRequestMock = PowerMockito.mock(DeleteMenuRequest.class);

        when(apiMock.deleteMenu(deleteMenuRequestMock)).thenReturn(callMock);
        when(responseMock.isSuccessful()).thenReturn(true);


        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<DeleteMenuResponse> callback = invocation
                        .getArgumentAt(0, Callback.class);
                callback.onResponse(callMock, responseMock);
                return null;
            }
        }).when(callMock).enqueue(any(Callback.class));

        presenter.deleteMenu(deleteMenuRequestMock);

        verify(mvpView, times(1))
                .onDeleteMenuSuccess();
    }
}