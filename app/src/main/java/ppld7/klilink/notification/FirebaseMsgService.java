package ppld7.klilink.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Random;

import ppld7.klilink.R;
import ppld7.klilink.ui.main.MainActivity;
import ppld7.klilink.ui.main.order.chat.ChatActivity;

public class FirebaseMsgService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseMsgService";
    NotificationManager notificationManager;
    private String ADMIN_CHANNEL_ID = "klilink_channel";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Setting up Notification channels for android O and above
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

        String sendbird = remoteMessage.getData().get("sendbird");
        final PendingIntent pendingIntent;

        if (sendbird != null) {
            JsonElement jsonElement = new JsonParser().parse(sendbird);
            Log.e(TAG, "onMessageReceived: jsonElement" + jsonElement);
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            String noHp = jsonObject.get("sender").getAsJsonObject().get("id").getAsString();
            String nama = jsonObject.get("sender").getAsJsonObject().get("name").getAsString();

            Intent backIntent = new Intent(getApplicationContext(), MainActivity.class);
            backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            Intent intent = ChatActivity.getStartIntent(noHp, nama, getApplicationContext()); // Here pass your activity where you want to redirect.

            pendingIntent = PendingIntent.getActivities(this, (int) (Math.random() * 100),
                    new Intent[]{backIntent, intent}, PendingIntent.FLAG_ONE_SHOT);
        } else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            pendingIntent = PendingIntent.getActivity(this, (int) (Math.random() * 100), intent, PendingIntent.FLAG_ONE_SHOT);
        }

        try {
            int notificationId = new Random().nextInt(60000);
            Log.e(TAG, "onMessageReceived: " + remoteMessage.getData().get("message"));
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                    .setSmallIcon(R.drawable.logo_klilink_orange)  //a resource for your custom small icon
                    .setContentTitle("Pesan Baru") //the "title" value you sent in your notification
                    .setContentText(remoteMessage.getData().get("message")) //ditto
                    .setAutoCancel(true)  //dismisses the notification on click
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = "Klilink";
        String adminChannelDescription = "Klilink";

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }
}