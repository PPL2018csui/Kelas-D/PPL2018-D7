package ppld7.klilink.ui.main.order;

import ppld7.klilink.data.network.model.request.order.OrderRequest;
import ppld7.klilink.di.PerActivity;
import ppld7.klilink.ui.base.MvpPresenter;

/**
 * Created by Fadhil on 17-May-18.
 */

@PerActivity
public interface OrderMvpPresenter<V extends OrderMvpView> extends MvpPresenter<V> {

    void requestMenu(String phoneNumber);

    void makeOrder(OrderRequest orderRequest);
}
