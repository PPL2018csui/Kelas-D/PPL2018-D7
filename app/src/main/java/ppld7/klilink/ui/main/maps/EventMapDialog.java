package ppld7.klilink.ui.main.maps;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ppld7.klilink.R;
import ppld7.klilink.data.db.model.EventPopUp;
import ppld7.klilink.ui.base.BaseDialog;

/**
 * Created by Fadhil on 15-Apr-18.
 */

public class EventMapDialog extends BaseDialog {
    private static final String TAG = EventMapDialog.class.getSimpleName();
    private static final String KEY_NAME = "name";
    private static final String KEY_START_TIME = "start_time";
    private static final String KEY_END_TIME = "end_time";

    TextView tvEventname;

    TextView tvDuration;

    Button btnClose;

    public static EventMapDialog newInstance(EventPopUp model) {
        EventMapDialog fragment = new EventMapDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_NAME, model.getName());
        bundle.putString(KEY_START_TIME, model.getWaktuMulai());
        bundle.putString(KEY_END_TIME, model.getWaktuSelesai());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_popup_marker, container, false);

        setUp(view);

        return view;
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    @Override
    protected void setUp(View view) {
        btnClose = view.findViewById(R.id.close_button);

        tvEventname = view.findViewById(R.id.popup_name);
        tvDuration = view.findViewById(R.id.popup_durasi);

        EventPopUp model = getModelFromBundle();
        tvEventname.setText(model.getName());
        String duration = model.getWaktuMulai() + " - " + model.getWaktuSelesai();
        tvDuration.setText(duration);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });
    }

    private EventPopUp getModelFromBundle() {
        Bundle bundle = getArguments();
        String name = bundle.getString(KEY_NAME, "dummy");
        String startTime = bundle.getString(KEY_START_TIME, "00");
        String endTime = bundle.getString(KEY_END_TIME, "00");

        EventPopUp eventPopup = EventPopUp.create(name, startTime, endTime);

        return eventPopup;
    }

    public void dismissDialog() {
        super.dismissDialog(TAG);
    }
}
