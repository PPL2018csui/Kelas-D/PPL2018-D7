package ppld7.klilink.ui.main.order.list;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponse;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderListPresenter<V extends OrderListMvpView> extends BasePresenter<V> implements OrderListMvpPresenter<V> {

    Retrofit retrofit;
    ProyekinApi api;
    Call<OrderListResponse> callRequestOrderList;
    Call<GenericResponse> callExecuteOrderAction;

    @Inject
    public OrderListPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void requestOrderList(String phoneNumber, String isSeller) {
        callRequestOrderList = api.getOrderList(phoneNumber, isSeller);
        callRequestOrderList.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {
                if (response.isSuccessful()) {
                    OrderListResponse orderListResponse = response.body();
                    getMvpView().onRequestOrderListSuccess(orderListResponse);
                } else {
                    int code = response.code();
                    getMvpView().onRequestOrderListFailed("Error " + code + ": Gagal Mengambil Daftar Pesanan");
                }
            }

            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
                getMvpView().onRequestOrderListFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });
    }

    @Override
    public void executeOrderAction(int orderId, String action) {
        callExecuteOrderAction = api.executeOrderAction(orderId, action);
        callExecuteOrderAction.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.isSuccessful()) {
                    GenericResponse data = response.body();
                    getMvpView().onExecuteOrderActionSuccess(data);
                } else {
                    int code = response.code();
                    getMvpView().onExecuteOrderActionFailed("Error " + code);
                }
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                getMvpView().onExecuteOrderActionFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });
    }
}
