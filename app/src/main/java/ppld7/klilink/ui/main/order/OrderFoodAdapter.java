package ppld7.klilink.ui.main.order;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.order.OrderList;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetail;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.utils.Constants;

/**
 * Created by Fadhil on 17-May-18.
 */

public class OrderFoodAdapter extends RecyclerView.Adapter<OrderFoodAdapter.FoodViewHolder> {
    private final Context context;
    private List<FoodResponse> foodList;
    private HashMap<Integer, OrderList> orderLists;
    private HashMap<Integer, OrderDetail> orderDetails;
    private LinearLayout orderCartLayout;
    private int total = 0;
    private static final int ADD = 0;
    private static final int MINUS = 1;
    private RecyclerView recyclerView;

    public OrderFoodAdapter(List<FoodResponse> foods, Context context) {
        this.foodList = foods;
        this.context = context;
    }

    @Override
    public OrderFoodAdapter.FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_food, parent, false);

        return new OrderFoodAdapter.FoodViewHolder(itemView);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(final OrderFoodAdapter.FoodViewHolder holder, final int position) {
        if (foodList.size() != 0) {
            FoodResponse food = foodList.get(position);
            holder.name.setText(toFirstCharUpperAll(food.getNamaMenu()));
            String price = Constants.formatPrice(food.getHarga());
            holder.price.setText(price);
            holder.quantity.setText("0");
            // picasso
            Glide.with(context).load(food.getUrlFotoMenu()).into(holder.ivFood);
        }
    }

    public String toFirstCharUpperAll(String string) {
        StringBuffer sb = new StringBuffer(string);
        for (int i = 0; i < sb.length(); i++)
            if (i == 0 || sb.charAt(i - 1) == ' ')//first letter to uppercase by default
                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
        return sb.toString();
    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public void setNewData(List<FoodResponse> foods) {
        this.foodList = foods;
        notifyDataSetChanged();
        orderLists = new HashMap<>();
        orderDetails = new HashMap<>();
    }

    public void setOrderCartLayout(LinearLayout orderCartLayout) {
        orderCartLayout.setBackgroundColor(Color.GRAY);
        this.orderCartLayout = orderCartLayout;
    }

    public class FoodViewHolder extends RecyclerView.ViewHolder {
        public TextView name, price, quantity, totalOrder;
        public ImageButton buttonAdd, buttonMinus;
        public ImageView ivFood;
        private int subTotal;

        public FoodViewHolder(View view) {
            super(view);
            buttonAdd = view.findViewById(R.id.add_button);
            buttonMinus = view.findViewById(R.id.minus_button);
            name = view.findViewById(R.id.tv_price);
            price = view.findViewById(R.id.tv_food);
            quantity = view.findViewById(R.id.tv_quantity);
            ivFood = view.findViewById(R.id.iv_food);
            totalOrder = orderCartLayout.findViewById(R.id.total_order);
            subTotal = 0;

            setUpButtons();
        }

        private void setUpButtons() {
            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    calculate(ADD);
                }
            });

            buttonMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    calculate(MINUS);
                }
            });
        }

        private void calculateSubTotal(int quantity) {
            FoodResponse food = foodList.get((getAdapterPosition()));
            int currentPrice = Integer.parseInt(food.getHarga());
            addToCart(food, quantity);
            subTotal = quantity * currentPrice;
        }

        private void addToCart(FoodResponse food, int quantity) {
            int position = getAdapterPosition();


            if (orderLists.containsKey(position)) {
                orderLists.get(position).setAmount(String.valueOf(quantity));
                orderDetails.get(position).setQuantity(quantity);
            } else {
                OrderList newOrder = new OrderList();
                newOrder.setMenuId(String.valueOf(food.getIdMenu()));
                newOrder.setAmount(String.valueOf(quantity));
                OrderDetail newOrderDetail = new OrderDetail();
                newOrderDetail.setNama(food.getNamaMenu());
                newOrderDetail.setHarga(Integer.parseInt(food.getHarga()));
                newOrderDetail.setQuantity(quantity);
                newOrderDetail.setUrlFoto(food.getUrlFotoMenu());
                orderLists.put(position, newOrder);
                orderDetails.put(position, newOrderDetail);
            }
        }

        private void calculate(int operation) {
            int currentQuantity = Integer.parseInt(quantity.getText().toString());
            switch (operation) {
                case ADD:
                    currentQuantity += 1;
                    break;
                case MINUS:
                    currentQuantity -= 1;
                    break;
                default:
                    break;
            }
            if (currentQuantity <= 0) {
                currentQuantity = 0;
            }

            total -= subTotal;
            calculateSubTotal(currentQuantity);
            total += subTotal;

            RelativeLayout.LayoutParams params= new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            if (total <= 0) {
                orderCartLayout.setOnClickListener(null);
                orderCartLayout.setBackgroundColor(Color.GRAY);
            } else {
                orderCartLayout.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                orderCartLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OrderActivity orderActivity = (OrderActivity) context;
                        OrderConfirmationDialog dialog = new OrderConfirmationDialog();
                        dialog.setData(
                                new ArrayList<>(orderLists.values()),
                                new ArrayList<>(orderDetails.values()),
                                total
                        );
                        dialog.show(orderActivity.getSupportFragmentManager());
                    }
                });
            }

            totalOrder.setText(Constants.formatPrice(total));
            quantity.setText(String.valueOf(currentQuantity));
        }
    }
}
