package ppld7.klilink.ui.main.order.chat;

import ppld7.klilink.ui.base.MvpPresenter;

public interface ChatMvpPresenter<V extends ChatMvpView> extends MvpPresenter<V> {
    void connectToSendbird();

}
