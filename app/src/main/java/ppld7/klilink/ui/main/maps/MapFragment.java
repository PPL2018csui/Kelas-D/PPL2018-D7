package ppld7.klilink.ui.main.maps;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.db.model.EventPopUp;
import ppld7.klilink.data.db.model.MapPopup;
import ppld7.klilink.data.network.model.request.maps.UpdateLocFirebase;
import ppld7.klilink.data.network.model.response.map.GetEventResponse;
import ppld7.klilink.data.network.model.response.map.GetPedagangResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.service.MapLocationService;
import ppld7.klilink.service.TrackingService;
import ppld7.klilink.ui.Event.EventActivity;
import ppld7.klilink.ui.base.BaseFragment;

/**
 * Created by Ibam on 3/4/2018.
 * <p>
 * This code is redeveloped by Muhammad Fadhillah
 */

public class MapFragment extends BaseFragment implements
        OnMapReadyCallback,
        MapMvpView,
        GoogleMap.OnMarkerClickListener,
        CompoundButton.OnCheckedChangeListener {
    protected static final int DEFAULT_ZOOM = 16;
    protected static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    protected static final int LOCATION_REQUEST_INTERVAL = 5000;
    protected static final int LOCATION_REQUEST_FASTEST_INTERVAL = 5000;
    private static final String TAG = MapFragment.class.getSimpleName();
    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "ppld7.klilink.ui.main.maps.location";
    MapView mMapView;
    Map<String, Marker> mapsMarkerList;
    @Inject
    MapMvpPresenter<MapMvpView> mPresenter;
    ValueEventListener pedagangListener;
    DatabaseReference databaseReference;
    Switch aSwitch;
    TextView headerTitle;
    private View view;
    private GoogleMap mMap;
    private CameraPosition mCameraPosition;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    // Listener when receive newest Location.
    private LocationCallback mLocationCallback;
    // Use to track whether the user has allowed the location permission or not.
    private boolean mLocationPermissionGranted;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private UserDataModel currentUser;
    private ImageButton currentLocationButton;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        view = inflater.inflate(R.layout.fragment_map, container, false);
        getActivityComponent().inject(this);

        mPresenter.onAttach(MapFragment.this);
        currentLocationButton = view.findViewById(R.id.current_location_button);
        currentLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveCamera(true);
            }
        });

        // Retrieve information from saved instance state.
        updateValuesFromBundle(savedInstanceState);
        createLocationCallback();

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mapsMarkerList = new HashMap();

        mMapView = (MapView) view.findViewById(R.id.mapView);
        mPresenter.setupMapHeader();

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);


        return view;
    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        this.mMap = mMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                getActivity(),
                R.raw.style_json
        );
        setUpToolbar();
        mMap.setMapStyle(style);
        mMap.setOnMarkerClickListener(this);
        setupFirebaseListener();
        // mPresenter.getAllPedagang();
        mPresenter.getAllEvent();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
        if (mMap != null) {
            mCameraPosition = mMap.getCameraPosition();
        }
        if (mPresenter.isSeller() && mPresenter.getTrackingStatus()) {
            mPresenter.startTrackingService();
        }
        if (databaseReference != null) {
            databaseReference.removeEventListener(pedagangListener);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //if (mPresenter.isSeller() && mPresenter.getTrackingStatus()) {
        createLocationRequest();
        //}
        if (mCameraPosition != null) {
            moveCamera(false);
        }
        stopTrackingService();
        setupFirebaseListener();
    }

    @Override
    public void startTrackingService(UserDataModel data) {
        Intent intent = TrackingService.getStartIntent(getActivity(), data);
        getActivity().startService(intent);
    }

    private void stopTrackingService() {
        Log.e(TAG, "stopTrackingService: true");
        getActivity().stopService(new Intent(getActivity(), TrackingService.class));
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
    ) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateMapsUI();
    }

    @Override
    public void onFetchDataSuccess(List<GetPedagangResponse> pedagangList) {
        if (!pedagangList.isEmpty()) {
//            MapLocationService.setUpPedagangMarker(mMap, pedagangList);
        } else {
            showMessage("Tidak ada pedagang yang tersedia");
        }
    }

    @Override
    public void onFetchEventSuccess(List<GetEventResponse> eventList) {
        if (!eventList.isEmpty()) {
            MapLocationService.setUpEventMarker(mMap, eventList);
        }
    }

    @Override
    public void onFetchDataError() {
        Toast.makeText(getContext(), "Fetch data error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestUserData(UserDataModel userDataModel) {
        currentUser = userDataModel;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTag() instanceof MapPopup) {
            MapPopup data = (MapPopup) marker.getTag();
            if (mPresenter.isAuthenticated() && !mPresenter.isSeller()) {
                MapDialog dialog = MapDialog.newInstance(data);
                dialog.onAttach(getContext());
                dialog.show(getFragmentManager());
            }
        } else if (marker.getTag() instanceof EventPopUp) {
            EventPopUp data = (EventPopUp) marker.getTag();

            EventMapDialog.newInstance(data).show(getFragmentManager());
        }
        return false;
    }

    private void setUpToolbarPembeli(int menu) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.map_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        toolbar.inflateMenu(menu);
    }

    private void setUpToolbar() {
        if (mPresenter.isAuthenticated()) {
            if (mPresenter.isSeller()) {
                Toolbar toolbar = (Toolbar) view.findViewById(R.id.map_toolbar);
                ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            } else {
                setUpToolbarPembeli(R.menu.menu_on_maps_pembeli);
            }
        }
    }

    protected void createLocationRequest() {
        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        SettingsClient client = LocationServices.getSettingsClient(getActivity());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getLocationPermission();

                updateMapsUI();

                startLocationUpdates();
            }
        }).addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(getActivity(),
                                REQUEST_CHECK_SETTINGS);
                    } catch (Exception sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    protected void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        Log.e(TAG, "updateValuesFromBundle: " + savedInstanceState);
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
            Log.e(TAG, "updateValuesFromBundle: " + mCameraPosition);
        }
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                } else {
                    Log.e(TAG, "onLocationResult: " + locationResult.getLastLocation());
                    mLastKnownLocation = locationResult.getLastLocation();
                    if (mPresenter.isSeller() && mPresenter.getTrackingStatus()) {
                        mPresenter.updateLocationFirebase(mLastKnownLocation);
                    }
                    moveCamera(false);
                }
            }
        };
    }

    private void startLocationUpdates() {
        try {
            mFusedLocationProviderClient.requestLocationUpdates(LocationRequest.create(),
                    mLocationCallback,
                    null);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void stopLocationUpdates() {
        mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    private void updateMapsUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.getUiSettings().setMapToolbarEnabled(false);
                if (!mPresenter.isSeller()) {
                    mMap.setMyLocationEnabled(true);

                    mMap.getUiSettings().setMyLocationButtonEnabled(true);
                }
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void moveCamera(boolean isCurrentLocation) {
        LatLng currentLatLng;
        try {
            currentLatLng = new LatLng(
                    mLastKnownLocation.getLatitude(),
                    mLastKnownLocation.getLongitude()
            );
            if (mCameraPosition != null && !isCurrentLocation) {
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        currentLatLng,
                        DEFAULT_ZOOM)
                );
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void setUpRealtimeMarker(GoogleMap mMap, List<UpdateLocFirebase> list) {
        mapsMarkerList = filterMarker(mapsMarkerList, list);
        for (UpdateLocFirebase model : list) {
            Log.e(TAG, "setUpRealtimeMarker: " + model.getNoHp());
            String location = model.getLocation();
            try {
                String lastLocation = mLastKnownLocation.getLatitude() + ", " + mLastKnownLocation.getLongitude();
                int distance = distance(lastLocation, location);
                double lat = MapLocationService.getLatOrLng(location, true);
                double lng = MapLocationService.getLatOrLng(location, false);
                LatLng latLng = new LatLng(lat, lng);

                if (mapsMarkerList.containsKey(model.getNoHp())) {
                    // if marker already exist in the maps, just update the location
                    Marker marker = mapsMarkerList.get(model.getNoHp());
                    marker.setPosition(latLng);
                } else {
                    Log.e(TAG, "setUpRealtimeMarker: " + model.getUrlProfile());
                    MapPopup mapPopup = MapPopup.getInstance(
                            model.getNama(), "Jarak: " + distance + " meter",
                            model.getNoHp(),
                            model.getUrlProfile()
                    );

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng)
                            .title(model.getNama());

                    if (mPresenter.getCurrentUserPhone().equals(model.getNoHp())) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gerobak_blue));
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gerobak));
                    }

                    Marker marker = mMap.addMarker(markerOptions);
                    marker.setTag(mapPopup);

                    mapsMarkerList.put(model.getNoHp(), marker);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, Marker> filterMarker(Map<String, Marker> currentMarkers, List<UpdateLocFirebase> temp) {
        HashMap<String, String> newMarkers = new HashMap<>();
        HashMap<String, Marker> filteredMarkers = new HashMap<>();

        for (UpdateLocFirebase loc : temp) {
            String key = loc.getNoHp();
            newMarkers.put(key, key);
        }

        for (String key : currentMarkers.keySet()) {
            if (newMarkers.containsKey(key)) {
                filteredMarkers.put(key, currentMarkers.get(key));
            } else {
                Marker marker = currentMarkers.get(key);
                marker.remove();
                Log.e(TAG, "filterMarker: " + key + " is removed");
                // currentMarkers.remove(key);
            }
        }

        return filteredMarkers;
    }

    private int distance(String location1, String location2) {

        /*
         *
         * modified from https://stackoverflow.com/a/16794680
         *
         * */

        String[] latlong1 = location1.replace(",", "").split(" ");
        String[] latlong2 = location2.replace(",", "").split(" ");

        double lat1 = Double.parseDouble(latlong1[0]);
        double lon1 = Double.parseDouble(latlong1[1]);
        double lat2 = Double.parseDouble(latlong2[0]);
        double lon2 = Double.parseDouble(latlong2[1]);
        double el1 = 0;
        double el2 = 0;

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return (int) Math.round(Math.sqrt(distance));
    }

    private void setupFirebaseListener() {
        databaseReference = FirebaseDatabase.getInstance().getReference("location");

        pedagangListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int count = 0;
                List<UpdateLocFirebase> locFirebaseList = new ArrayList<>();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    UpdateLocFirebase pedagang = data.getValue(UpdateLocFirebase.class);
                    //Log.e(TAG, "onDataChange: item " + pedagang.getNama());
                    locFirebaseList.add(pedagang);
                    count++;
                }
                //Log.e(TAG, "onDataChange: data count: " + count);
                setUpRealtimeMarker(mMap, locFirebaseList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        databaseReference.addValueEventListener(pedagangListener);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.e(TAG, "isMyServiceRunning: " + true);
                return true;
            }
        }
        Log.e(TAG, "isMyServiceRunning: " + false);
        return false;
    }

    @Override
    public void setupMapHeader(boolean isSeller, String name) {
        headerTitle = view.findViewById(R.id.header_title);
        aSwitch = view.findViewById(R.id.tracking_switch);
        if (isSeller) {
            headerTitle.setText("Status Tracking");
            aSwitch.setOnCheckedChangeListener(this);
            aSwitch.setChecked(mPresenter.getTrackingStatus());
            if (mPresenter.getTrackingStatus()) {
                startLocationUpdates();
            }
        } else {
            aSwitch.setVisibility(View.GONE);
            if (name != null) {
                String[] names = name.split(" ");
                headerTitle.setText("Halo " + names[0] + ", mau beli apa hari ini?");
            } else {
                headerTitle.setText("Login untuk dapat menggunakan Klilink");
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isOn) {
        if (isOn) {
            compoundButton.setText("Aktif");
            mPresenter.setTrackingStatus(isOn);
            startLocationUpdates();
            Log.e(TAG, "onCheckedChanged: aktif");
        } else {
            Log.e(TAG, "onCheckedChanged: nonaktif");
            compoundButton.setText("Tidak Aktif");
            mPresenter.setTrackingStatus(isOn);
            mPresenter.removeFromFirebase();
            stopTrackingService();
            Marker marker = mapsMarkerList.get(mPresenter.getCurrentUserPhone());
            if (marker != null) {
                marker.remove();
                mapsMarkerList.remove(mPresenter.getCurrentUserPhone());
            }
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        int menu_layout = 0;
        menu_layout = R.menu.menu_on_maps_pembeli;
        inflater.inflate(menu_layout, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addEvent_pembeli_btn:
                Intent intentEvent = new Intent(getContext(), EventActivity.class);
                startActivity(intentEvent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}