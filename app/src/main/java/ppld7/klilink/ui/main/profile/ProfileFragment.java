package ppld7.klilink.ui.main.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.service.TrackingService;
import ppld7.klilink.ui.base.BaseFragment;
import ppld7.klilink.ui.base.MvpView;
import ppld7.klilink.ui.login.LoginActivity;
import ppld7.klilink.ui.menu_add_edit.MenuActivity;
import ppld7.klilink.ui.setprofile.EditProfileActivity;
import ppld7.klilink.utils.Constants;

/**
 * Created by Ibam on 3/4/2018.
 */

public class ProfileFragment extends BaseFragment implements ProfileMvpView, View.OnClickListener, MvpView {

    private static final String TAG = "ProfileFragment";
    @Inject
    ProfileMvpPresenter<ProfileMvpView> mPresenter;

    RecyclerView recyclerView;
    FoodAdapter adapter;
    View view;
    FloatingActionButton fab;
    private Toolbar toolbarProfile;
    private TextView tvDisplayName;
    private TextView tvNamaToko;
    private TextView tvNomerHp;
    private int fragment;
    private UserDataModel userDataModel;
    private Button buttonLogin;
    private Button buttonEditProfile;
    private boolean isReady;
    private CircleImageView imgProfile;
    private String urlPhoto;

    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivityComponent().inject(this);
        mPresenter.onAttach(ProfileFragment.this);
        mPresenter.onProfileRequest();
        showLoading();
        view = inflater.inflate(fragment, container, false);

        if (userDataModel != null) {
            if (userDataModel.isSeller()) {
                setUpPedagangView();
            } else {
                setUpPembeliView();
            }
        } else {
            hideLoading();
            buttonLogin = (Button) view.findViewById(R.id.button_to_login);
            buttonLogin.setOnClickListener(this);
        }
        mPresenter.getMenuPedagang();
        return view;
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                Intent addMenu = new Intent(getActivity(), MenuActivity.class);
                startActivity(addMenu);
                break;
            case R.id.button_to_login:
                getActivity().finish();
                break;
            case R.id.button_to_editprofile:
                Intent intentEditProfileButton = new Intent(getContext(), EditProfileActivity.class);
                startActivityForResult(intentEditProfileButton, 200);
                break;
            default:
                break;
        }
    }


    private AdapterView.OnItemSelectedListener onItemSelectedListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView parent, View view, int position, long id) {
                if (position == 0) {
                    isReady = true;
                } else {
                    isReady = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView parent) {

            }
        };
    }

    public void setUpPembeliView() {
        tvDisplayName = (TextView) view.findViewById(R.id.disp_name);
        tvDisplayName.setText(userDataModel.getName());
        tvNomerHp = (TextView) view.findViewById(R.id.tv_no_hp);
        tvNomerHp.setText(userDataModel.getNomorTelepon());
        setUpToolbar(R.menu.menu_on_profile_pembeli);

        buttonEditProfile = (Button) view.findViewById(R.id.button_to_editprofile);
        buttonEditProfile.setOnClickListener(this);

        imgProfile = view.findViewById(R.id.profile_picture);
        urlPhoto = userDataModel.getUrlPhoto();
        if(urlPhoto== null || urlPhoto.equals("")){
            final Drawable dr = getResources().getDrawable(R.drawable.ic_default_user);
            imgProfile.setImageDrawable(dr);
        } else {
            Glide.with(getContext()).load(urlPhoto).into(imgProfile);
        }
        hideLoading();
    }

    @Override
    public void onRequestProfileFailed() {
        hideLoading();
        fragment = R.layout.fragment_profile_no_login;
    }


    @Override
    public void onRequestProfilePenjualSuccess(UserDataModel userDataModel) {
        hideLoading();
        fragment = R.layout.fragment_profile_penjual;
        this.userDataModel = userDataModel;
    }

    @Override
    public void onRequestProfilePembeliSuccess(UserDataModel userDataModel) {
        hideLoading();
        fragment = R.layout.fragment_profile_pembeli;
        this.userDataModel = userDataModel;
    }


    public void setUpPedagangView() {
        Log.e(TAG, "setUpPedagangView: done");
        urlPhoto = userDataModel.getUrlPhoto();
        tvDisplayName = (TextView) view.findViewById(R.id.disp_name);
        tvDisplayName.setText(userDataModel.getName());
        tvNamaToko = (TextView) view.findViewById(R.id.nama_toko);
        tvNamaToko.setText(userDataModel.getNamaToko());

        recyclerView = view.findViewById(R.id.rv);
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        setUpRv();
        imgProfile = view.findViewById(R.id.profile_picture);
        if (urlPhoto == null || urlPhoto.equals("")) {
            final Drawable dr = getResources().getDrawable(R.drawable.ic_default_user);
            imgProfile.setImageDrawable(dr);
        } else {
            Glide.with(getContext()).load(urlPhoto).into(imgProfile);
        }


        setUpToolbar(R.menu.menu_on_profile_pedagang);
    }


    @SuppressLint("ResourceAsColor")
    private void setUpToolbar(int menu) {
        setHasOptionsMenu(true);
        toolbarProfile = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbarProfile);
        toolbarProfile.inflateMenu(menu);
    }

    @Override
    public void onLoggedOut() {
        hideLoading();
        if (!mPresenter.isAuthenticated()) {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        int menu_layout = 0;
        if (userDataModel != null) {
            if (userDataModel.isSeller()) {
                menu_layout = R.menu.menu_on_profile_pedagang;
            } else {
                menu_layout = R.menu.menu_on_profile_pembeli;
            }
        }
        inflater.inflate(menu_layout, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editprofile_button:
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivityForResult(intent, 200);
                return true;
            case R.id.logout_button:
                showLoading();
                stopTrackingService();
                mPresenter.logOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getFragmentManager()
                .beginTransaction()
                .detach(this)
                .attach(this)
                .commit();
    }

    private void stopTrackingService() {
        Log.e(TAG, "stopTrackingService: true");
        getActivity().stopService(new Intent(getActivity(), TrackingService.class));
    }


    private void setUpRv() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new FoodAdapter(new ArrayList<FoodResponse>(), getContext());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onMenuLoaded(List<FoodResponse> foodList) {
        adapter.setNewData(foodList);
        hideLoading();
    }

    @Override
    public void onLoadMenuError(String string) {
        showMessage(string);
        hideLoading();
    }

}
