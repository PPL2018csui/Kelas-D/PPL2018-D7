package ppld7.klilink.ui.main.maps;

import android.location.Location;

import ppld7.klilink.data.network.model.request.maps.UpdateLocationRequest;
import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Ibam on 3/28/2018.
 */

public interface MapMvpPresenter<V extends MvpView> extends MvpPresenter<V> {

    void getAllPedagang();

    void getAllEvent();

    void requestUserData();

    boolean isSeller();

    void updateLocationFirebase(Location lastLocation);

    void startTrackingService();

    void setTrackingStatus(boolean aa);

    boolean getTrackingStatus();

    void removeFromFirebase();

    void setupMapHeader();
}
