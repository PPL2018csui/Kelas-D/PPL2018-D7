package ppld7.klilink.ui.main.order.list;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponseActor;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponseData;
import ppld7.klilink.ui.main.order.chat.ChatActivity;
import ppld7.klilink.ui.main.order.detail.OrderDetailActivity;
import ppld7.klilink.utils.Constants;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {
    private final OrderListFragment fragment;
    private final Context context;
    private List<OrderListResponseData> orderList;

    public OrderListAdapter(List<OrderListResponseData> list, OrderListFragment fragment, Context context) {
        this.orderList = list;
        this.fragment = fragment;
        this.context = context;
    }

    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_list, parent, false);

        return new OrderListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderListAdapter.ViewHolder holder, final int position) {
        final OrderListResponseData item = orderList.get(position);
        holder.name.setText(item.getOrderListResponseActor().getNama());
        holder.totalPrice.setText(Constants.formatPrice(item.getHargaTotal()));
        holder.orderStatus.setText(item.getStatus().toUpperCase());

        holder.buttonMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderListResponseActor actor = item.getOrderListResponseActor();
                String noHp = actor.getHp();
                String nama = actor.getNama();
                Intent intent = ChatActivity.getStartIntent(noHp, nama, context);
                context.startActivity(intent);
            }
        });

        holder.buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent orderIntent = new Intent(fragment.getActivity(), OrderDetailActivity.class);
                int orderId = orderList.get(position).getIdOrder();
                orderIntent.putExtra("ORDER_ID", orderId);
                fragment.startActivity(orderIntent);
            }
        });

        holder.buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int orderId = orderList.get(position).getIdOrder();
                final OrderListDialog dialog = new OrderListDialog();
                dialog.onAttach(new OrderListDialog.OnDialogClickListener() {
                    @Override
                    public void onAcceptButtonClicked() {
                        dialog.dismiss();
                        fragment.executeOrderAction(orderId, Constants.CANCEL_ORDER_COMMAND, position);
                    }
                });
                dialog.show(fragment.getFragmentManager());
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public void setNewData(List<OrderListResponseData> list) {
        this.orderList = list;
        notifyDataSetChanged();
    }

    public void deleteHolder(int holderPosition) {
        orderList.remove(holderPosition);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, totalPrice, orderStatus;
        public Button buttonMessage, buttonFinish, buttonDetail;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.sender_name);
            totalPrice = view.findViewById(R.id.order_total_price);
            orderStatus = view.findViewById(R.id.order_status);
            buttonMessage = view.findViewById(R.id.order_chat_button);
            buttonFinish = view.findViewById(R.id.finish_order_button);
            buttonDetail = view.findViewById(R.id.order_detail_button);

            if (!fragment.mPresenter.isSeller()) {
                buttonFinish.setText("BATAL");
            }
        }
    }
}
