package ppld7.klilink.ui.main.maps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import ppld7.klilink.R;
import ppld7.klilink.data.db.model.MapPopup;

/**
 * Created by Ibam on 3/21/2018.
 */

public class PopupWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final View mWindow;
    private Context context;

    public PopupWindowAdapter(Context context) {
        this.context = context;
        mWindow = LayoutInflater.from(context).inflate(R.layout.popup_marker, null);
    }

    private void renderWindow(MapPopup model, View view) {
        String name = model.getName();
        String storename = model.getStorename();
        TextView tvName = view.findViewById(R.id.popup_name);
        TextView tvStore = view.findViewById(R.id.popup_openhours);
        if (!name.isEmpty()) {
            tvName.setText(name);
        }
        if (!storename.isEmpty()) {
            tvStore.setText(storename);
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        MapPopup model = (MapPopup) marker.getTag();
        renderWindow(model, mWindow);
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        MapPopup model = (MapPopup) marker.getTag();
        renderWindow(model, mWindow);
        return mWindow;
    }
}
