package ppld7.klilink.ui.main.order;

import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.order.OrderRequest;
import ppld7.klilink.data.network.model.response.order.OrderResponse;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Fadhil on 17-May-18.
 */

public class OrderPresenter<V extends OrderMvpView> extends BasePresenter<V> implements OrderMvpPresenter<V> {

    Retrofit retrofit;
    ProyekinApi api;
    Call<List<FoodResponse>> callRequestMenu;
    Call<OrderResponse> callMakeOrder;

    @Inject
    public OrderPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void requestMenu(String phoneNumber) {
        callRequestMenu = api.getFoodList(phoneNumber);
        callRequestMenu.enqueue(new Callback<List<FoodResponse>>() {
            @Override
            public void onResponse(Call<List<FoodResponse>> call, Response<List<FoodResponse>> response) {
                if (response.isSuccessful()) {
                    List<FoodResponse> data = response.body();
                    getMvpView().onRequestMenuSuccess(data);
                } else {
                    int code = response.code();
                    getMvpView().onRequestMenuFailed("Error " + code + " : Gagal Mengambil Data Menu");
                }
            }

            @Override
            public void onFailure(Call<List<FoodResponse>> call, Throwable t) {
                getMvpView().onRequestMenuFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });
    }

    @Override
    public void makeOrder(OrderRequest orderRequest) {
        callMakeOrder = api.makeOrder(orderRequest);
        callMakeOrder.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                if (response.isSuccessful()) {
                    OrderResponse data = response.body();
                    getMvpView().onMakeOrderSuccess(data);
                } else {
                    int code = response.code();
                    getMvpView().onMakeOrderFailed("Error " + code + " : Gagal Membuat Order");
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                getMvpView().onMakeOrderFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });
    }
}
