package ppld7.klilink.ui.main.order.list;

import ppld7.klilink.ui.base.MvpPresenter;

/**
 * Created by Fadhil on 23-May-18.
 */

public interface OrderListMvpPresenter<V extends OrderListMvpView> extends MvpPresenter<V> {

    void requestOrderList(String phoneNumber, String isSeller);

    void executeOrderAction(int orderId, String action);
}
