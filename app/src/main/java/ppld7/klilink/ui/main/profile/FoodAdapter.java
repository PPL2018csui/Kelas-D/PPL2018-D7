package ppld7.klilink.ui.main.profile;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.ui.menu_add_edit.MenuActivity;
import ppld7.klilink.utils.Constants;
/**
 * Created by Ibam on 3/12/2018.
 */

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.FoodViewHolder> {
    private final Context context;
    private List<FoodResponse> foodList;

    public FoodAdapter(List<FoodResponse> foods, Context context) {
        this.foodList = foods;
        this.context = context;
    }

    @Override
    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_profile_food, parent, false);

        return new FoodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FoodViewHolder holder, int position) {
        if (foodList.size() != 0) {
            FoodResponse food = foodList.get(position);
            holder.name.setText(toFirstCharUpperAll(food.getNamaMenu()));
            String price = Constants.formatPrice(food.getHarga());
            boolean stok = food.getIsReady();
            String isStok = "";
            if(stok){
                isStok = "Masih Ada";
            } else {
                isStok = "Sudah habis";
            }
            holder.tvStock.setText(isStok);
            holder.price.setText(price);
            // picasso
            Glide.with(context).load(food.getUrlFotoMenu()).into(holder.ivFood);
        }
    }

    public String toFirstCharUpperAll(String string) {
        StringBuffer sb = new StringBuffer(string);
        for (int i = 0; i < sb.length(); i++)
            if (i == 0 || sb.charAt(i - 1) == ' ')//first letter to uppercase by default
                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
        return sb.toString();
    }


    @Override
    public int getItemCount() {
        return foodList.size();
    }

    public void setNewData(List<FoodResponse> foods) {
        this.foodList = foods;
        notifyDataSetChanged();
    }

    public class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name, price;
        public TextView tvStock;
        public Button btnAction;
        public ImageView ivFood;


        public FoodViewHolder(View view) {
            super(view);
            btnAction = view.findViewById(R.id.btn_action);
            name = view.findViewById(R.id.tv_food);
            price = view.findViewById(R.id.tv_price);
            tvStock = view.findViewById(R.id.tv_stock);
            ivFood = view.findViewById(R.id.iv_food);

            btnAction.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_action:
                    int position = getAdapterPosition();

                    Intent intent = new Intent(context, MenuActivity.class);
                    FoodResponse response = foodList.get(position);
                    intent.putExtra("FoodPass", response);
                    context.startActivity(intent);
            }
        }
    }
}
