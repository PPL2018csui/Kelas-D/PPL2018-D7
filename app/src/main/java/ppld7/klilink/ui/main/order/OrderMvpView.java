package ppld7.klilink.ui.main.order;

import java.util.List;

import ppld7.klilink.data.network.model.response.order.OrderResponse;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Fadhil on 17-May-18.
 */

public interface OrderMvpView extends MvpView {
    void onRequestMenuSuccess(List<FoodResponse> foodList);

    void onRequestMenuFailed(String message);

    void onMakeOrderSuccess(OrderResponse orderResponse);

    void onMakeOrderFailed(String message);
}
