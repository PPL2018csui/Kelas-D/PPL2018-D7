package ppld7.klilink.ui.main.profile.Foods;

import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Dwi Nanda on 26/04/2018.
 */

public interface FoodMvpView extends MvpView {
    void onRequestFoodFailed();
    void onRequestFoodSuccess();
}
