package ppld7.klilink.ui.main.order.list;

import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponse;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Fadhil on 23-May-18.
 */

public interface OrderListMvpView extends MvpView {

    void onRequestOrderListSuccess(OrderListResponse orderListResponse);

    void onRequestOrderListFailed(String message);

    void onExecuteOrderActionSuccess(GenericResponse response);

    void onExecuteOrderActionFailed(String message);
}
