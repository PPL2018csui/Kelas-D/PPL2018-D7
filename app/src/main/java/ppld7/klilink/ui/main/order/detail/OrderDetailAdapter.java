package ppld7.klilink.ui.main.order.detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetail;
import ppld7.klilink.utils.Constants;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.FoodViewHolder> {
    private final Context context;
    private List<OrderDetail> foods;

    public OrderDetailAdapter(List<OrderDetail> foods, Context context) {
        this.foods = foods;
        this.context = context;
    }

    @Override
    public FoodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_detail, parent, false);

        return new OrderDetailAdapter.FoodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FoodViewHolder holder, int position) {
        if (!foods.isEmpty()) {
            OrderDetail orderDetail = foods.get(position);
            holder.foodName.setText(orderDetail.getNama());
            holder.foodPrice.setText(Constants.formatPrice(orderDetail.getHarga()));
            holder.foodQuantity.setText(getQuantity(orderDetail.getQuantity()));

            Glide.with(context).load(orderDetail.getUrlFoto()).into(holder.foodImage);
        }
    }

    @Override
    public int getItemCount() {
        return foods.size();
    }

    private String getQuantity(int quantity) {
        String quantityStr = "Jumlah : " + quantity;
        return quantityStr;
    }

    public void setNewData(List<OrderDetail> foods) {
        this.foods = foods;
        notifyDataSetChanged();
    }

    public class FoodViewHolder extends RecyclerView.ViewHolder {
        public TextView foodName, foodPrice, foodQuantity;
        public ImageView foodImage;

        public FoodViewHolder(View view) {
            super(view);
            foodName = view.findViewById(R.id.tv_food);
            foodPrice = view.findViewById(R.id.tv_price);
            foodQuantity = view.findViewById(R.id.tv_quantity);
            foodImage = view.findViewById(R.id.iv_food);
        }

    }
}
