package ppld7.klilink.ui.main;

import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.model.response.SendbirdData;
import ppld7.klilink.ui.base.BasePresenter;

/**
 * Created by Ibam on 3/28/2018.
 */

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements
        MainMvpPresenter<V> {


    @Inject
    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void connectToSendbird() {
        SendbirdData data = getDataManager().getSendbirdData();

        if (data != null) {
            SendBird.connect(data.getUserId(), data.getAccessToken(), new SendBird.ConnectHandler() {
                @Override
                public void onConnected(User user, SendBirdException e) {
                    if (e != null) {
                        getMvpView().onSendbirdFailed(e.getMessage());
                        return;
                    }

                    getDataManager().registerSendbirdPushToken();
                    getDataManager().updateSendbirdCurrentUserInfo();
                }
            });
        } else {
            getMvpView().onSendbirdFailed("Error! Null sendbird data");
        }
    }
}
