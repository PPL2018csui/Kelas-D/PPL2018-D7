package ppld7.klilink.ui.main.profile;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.data.network.model.response.profile.AddMenuResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Dwi Nanda on 24/04/2018.
 */
public class ProfilePresenter<V extends ProfileMvpView> extends BasePresenter<V> implements ProfileMvpPresenter<V> {


    private static final String TAG = "ProfilePresenter";
    Retrofit retrofit;
    ProyekinApi api;
    Call<AddMenuResponse> call;
    Call<List<FoodResponse>> callFood;


    Callback<List<FoodResponse>> callbackFood = new Callback<List<FoodResponse>>() {
        @Override
        public void onResponse(Call<List<FoodResponse>> call, Response<List<FoodResponse>> response) {
            if (response.isSuccessful()) {
                getMvpView().onMenuLoaded(response.body());
            } else {
                int code = response.code();
                getMvpView().onLoadMenuError("Error " + code + " - Gagal Mengambil Data Menu");
            }
        }

        @Override
        public void onFailure(Call<List<FoodResponse>> call, Throwable t) {
            getMvpView().onLoadMenuError(Constants.CONNECTION_ERROR_MESSAGE);
        }
    };

    @Inject
    public ProfilePresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void logOut() {
        removeFromFirebase();
        getDataManager().setUserAsLoggedOut();
        getMvpView().onLoggedOut();
    }

    protected void removeFromFirebase() {
        if (isSeller()) {
            UserDataModel userDataModel = getDataManager().getUserModel();
            String noHp = userDataModel.getNomorTelepon();
            FirebaseDatabase firebaseInstance = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseInstance.getReference("location");
            DatabaseReference dbUserRef = databaseReference.child(noHp);
            dbUserRef.removeValue();
        }
    }

    @Override
    public void onProfileRequest() {
        UserDataModel currentUser = getDataManager().getUserModel();

        if (getDataManager().isLoggedIn()) {
            if (getDataManager().isSeller()) {
                getMvpView().onRequestProfilePenjualSuccess(currentUser);
            } else {
                getMvpView().onRequestProfilePembeliSuccess(currentUser);
            }
        } else {
            getMvpView().onRequestProfileFailed();
        }

    }

    @Override
    public void getMenuPedagang() {
        String noHp = getDataManager().getCurrentUserPhone();
        boolean isValid = getDataManager().isLoggedIn() && getDataManager().isSeller();
        if (isValid) {
            if (noHp != null) {
                callFood = api.getFoodList(noHp);
                callFood.enqueue(callbackFood);
            } else {
                getMvpView().onLoadMenuError("Gagal load menu untuk no hp " + noHp);
            }
        }
    }






}


