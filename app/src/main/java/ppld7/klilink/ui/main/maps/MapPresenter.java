package ppld7.klilink.ui.main.maps;

import android.location.Location;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.maps.UpdateLocFirebase;
import ppld7.klilink.data.network.model.request.maps.UpdateLocationRequest;
import ppld7.klilink.data.network.model.response.map.GetEventResponse;
import ppld7.klilink.data.network.model.response.map.GetPedagangResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ibam on 3/28/2018.
 */

public class MapPresenter<V extends MapMvpView> extends BasePresenter<V> implements
        MapMvpPresenter<V> {

    private static final String TAG = "MapPresenter";
    private final String DB_REF = "location";
    Retrofit retrofit;
    ProyekinApi api;
    Call<List<GetPedagangResponse>> callPedagang;
    Call<List<GetEventResponse>> callEvent;
    Call<Response<String>> callLocation;

    @Inject
    public MapPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return getDataManager().getRetrofit().create(ProyekinApi.class);
    }

    @Override
    public void requestUserData() {
        UserDataModel userDataModel = getDataManager().getUserModel();
        getMvpView().onRequestUserData(userDataModel);
    }

    @Override
    public void getAllPedagang() {
        callPedagang = api.getAllPedagang();

        callPedagang.enqueue(new Callback<List<GetPedagangResponse>>() {
            @Override
            public void onResponse(Call<List<GetPedagangResponse>> call, Response<List<GetPedagangResponse>> response) {
                if (response.isSuccessful()) {
                    List<GetPedagangResponse> list = response.body();
                    if (list == null) {
                        getMvpView().onFetchDataError();
                    } else {
                        getMvpView().onFetchDataSuccess(list);
                    }
                } else {
                    getMvpView().onFetchDataError();
                }
            }

            @Override
            public void onFailure(Call<List<GetPedagangResponse>> call, Throwable t) {
                t.printStackTrace();
                getMvpView().onFetchDataError();
            }
        });
    }

    @Override
    public void getAllEvent() {
        callEvent = api.getAllEvent();
        callEvent.enqueue(new Callback<List<GetEventResponse>>() {
            @Override
            public void onResponse(Call<List<GetEventResponse>> call, Response<List<GetEventResponse>> response) {
                if (response.isSuccessful()) {
                    List<GetEventResponse> eventList = response.body();
                    if (eventList == null) {
                        getMvpView().onFetchDataError();
                    } else {
                        getMvpView().onFetchEventSuccess(eventList);
                    }
                } else {
                    getMvpView().onFetchDataError();
                }
            }

            @Override
            public void onFailure(Call<List<GetEventResponse>> call, Throwable t) {
                getMvpView().onFetchDataError();
            }
        });
    }

    @Override
    public void updateLocationFirebase(Location lastLocation) {

        if (isSeller()) {
            UserDataModel userDataModel = getDataManager().getUserModel();
            UpdateLocFirebase request = UpdateLocFirebase.newInstance(lastLocation, userDataModel);
            String noHp = userDataModel.getNomorTelepon();

            FirebaseDatabase firebaseInstance = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseInstance.getReference(DB_REF);
            DatabaseReference dbUserRef = databaseReference.child(noHp);

            dbUserRef.setValue(request);
        }
    }

    @Override
    public void startTrackingService() {
        UserDataModel userDataModel = getDataManager().getUserModel();
        getMvpView().startTrackingService(userDataModel);
    }

    public boolean getTrackingStatus() {
        return getDataManager().isTrackingEnabled();
    }

    public void setTrackingStatus(boolean status) {
        getDataManager().setTrackingStatus(status);
    }

    @Override
    public void removeFromFirebase() {
        if (isSeller()) {
            UserDataModel userDataModel = getDataManager().getUserModel();
            String noHp = userDataModel.getNomorTelepon();
            FirebaseDatabase firebaseInstance = FirebaseDatabase.getInstance();
            DatabaseReference databaseReference = firebaseInstance.getReference(DB_REF);
            DatabaseReference dbUserRef = databaseReference.child(noHp);
            dbUserRef.removeValue();
        }
    }

    @Override
    public void setupMapHeader() {
        boolean isSeller = getDataManager().isSeller();
        String name = getDataManager().getCurrentUserName();
        getMvpView().setupMapHeader(isSeller, name);
    }
}