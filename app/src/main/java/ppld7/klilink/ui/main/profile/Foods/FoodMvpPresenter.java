package ppld7.klilink.ui.main.profile.Foods;

import ppld7.klilink.ui.base.MvpPresenter;

/**
 * Created by Dwi Nanda on 26/04/2018.
 */

public interface FoodMvpPresenter <V extends FoodMvpView> extends MvpPresenter<V> {
    void onFoodRequest();
}
