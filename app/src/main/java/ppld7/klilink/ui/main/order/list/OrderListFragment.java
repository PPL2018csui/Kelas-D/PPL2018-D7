package ppld7.klilink.ui.main.order.list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponse;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponseData;
import ppld7.klilink.ui.base.BaseFragment;
import ppld7.klilink.ui.login.LoginActivity;
import ppld7.klilink.utils.Constants;


/**
 * Created by Ibam on 3/4/2018.
 */

public class OrderListFragment extends BaseFragment implements OrderListMvpView {

    RecyclerView recyclerView;
    private OrderListAdapter adapter;
    private View emptyRecyclerView;

    @Inject
    OrderListPresenter<OrderListMvpView> mPresenter;

    private int holderPosition;

    public static OrderListFragment newInstance() {
        Bundle args = new Bundle();
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        View view = null;
        if(mPresenter.isAuthenticated()) {
            view = inflater.inflate(R.layout.fragment_order_list, container, false);
            recyclerView = view.findViewById(R.id.rvItem);

            emptyRecyclerView = view.findViewById(R.id.order_list_empty_recycler);
            TextView title = emptyRecyclerView.findViewById(R.id.empty_recycler_view_title);
            TextView message = emptyRecyclerView.findViewById(R.id.empty_recycler_view_message);
            title.setText(Constants.EMPTY_ORDER_LIST_TITLE);
            message.setText(Constants.EMPTY_ORDER_LIST_MESSAGE);
            emptyRecyclerView.setVisibility(View.INVISIBLE);
            setUpOrderList();
        } else {
            view = inflater.inflate(R.layout.fragment_message_no_login, null);
            Button button = view.findViewById(R.id.button_to_login);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().finish();
                }
            });
        }
        return view;
    }

    @Override
    public void onRequestOrderListFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onRequestOrderListSuccess(OrderListResponse orderListResponse) {
        List<OrderListResponseData> orderList = orderListResponse.getData();
        if (orderList.isEmpty()) {
            emptyRecyclerView.setVisibility(View.VISIBLE);
        } else {
            adapter.setNewData(orderList);
        }
        hideLoading();
    }

    @Override
    public void onExecuteOrderActionSuccess(GenericResponse response) {
        hideLoading();
        if (!response.getIsSuccess()) {
            showMessage(response.getErr());
        } else {
            adapter.deleteHolder(holderPosition);
            if (adapter.getItemCount() <= 0) {
                emptyRecyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onExecuteOrderActionFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    public void executeOrderAction(int orderId, String action, int holderPosition) {
        showLoading();
        this.holderPosition = holderPosition;
        mPresenter.executeOrderAction(orderId, action);
    }

    private void setUpOrderList() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new OrderListAdapter(new ArrayList<OrderListResponseData>(), this, getActivity());
        recyclerView.setAdapter(adapter);

        if (mPresenter.isAuthenticated()) {
            showLoading();
            String phoneNumber = mPresenter.getCurrentUserPhone();
            String isSeller = String.valueOf(mPresenter.isSeller());
            mPresenter.requestOrderList(phoneNumber, isSeller);
        }
    }
}
