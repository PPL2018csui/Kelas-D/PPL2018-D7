package ppld7.klilink.ui.main.order.chat;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sendbird.android.UserMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ppld7.klilink.R;

/**
 * Adapter binding the Messages Sent/ Received
 * Created by Vyom on 1/3/2017.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    // Messages type
    private static final int MESSAGE_TYPE_SENT = 1;
    private static final int MESSAGE_TYPE_RECEIVED = 2;
    private static final String DATE_FORMAT = "dd/MM HH:mm";
    // Internal data
    private List<UserMessage> mMessagesList;
    private String mMyUserId;
    private SimpleDateFormat mDateFormatter;
    private Context context;

    public ChatAdapter(String myUserId, List<UserMessage> messagesList, Context context) {
        this.mMyUserId = myUserId;
        this.mMessagesList = messagesList;
        mDateFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.US);
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        UserMessage message = mMessagesList.get(position);
        if (mMyUserId.equals(message.getSender().getUserId())) {
            return MESSAGE_TYPE_SENT;
        } else {
            return MESSAGE_TYPE_RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return mMessagesList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        boolean isReceived = false;
        switch (viewType) {
            case MESSAGE_TYPE_SENT:
                layoutId = R.layout.item_message_sent;
                break;
            default:
            case MESSAGE_TYPE_RECEIVED:
                isReceived = true;
                layoutId = R.layout.item_message_received;
                break;
        }

        View v = LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);
        ViewHolder vh = new ViewHolder(v, isReceived);
        return vh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserMessage message = mMessagesList.get(position);
        holder.mMessagesTextView.setText(message.getMessage());
        holder.mDateTextView.setText(mDateFormatter.format(new Date(message.getCreatedAt())));

        if (holder.profPic != null) {
            String profUrl = message.getSender().getProfileUrl();
            Glide.with(context).load(profUrl).into(holder.profPic);
        }
    }

    public void addMessage(UserMessage message) {
        mMessagesList.add(message);
        notifyDataSetChanged();
    }

    // View holder for the view
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mMessagesTextView;
        public TextView mDateTextView;
        public ImageView profPic;

        public ViewHolder(View view, boolean isReceived) {
            super(view);
            this.mMessagesTextView = (TextView) view.findViewById(R.id.text_message_body);
            this.mDateTextView = (TextView) view.findViewById(R.id.text_message_time);

            if (isReceived) {
                this.profPic = view.findViewById(R.id.image_message_profile);
            } else {
                profPic = null;
            }
        }
    }
}