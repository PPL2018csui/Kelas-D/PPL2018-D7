package ppld7.klilink.ui.main.order.chat;

import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.model.response.SendbirdData;
import ppld7.klilink.ui.base.BasePresenter;

public class ChatPresenter<V extends ChatMvpView> extends BasePresenter<V> implements ChatMvpPresenter<V> {

    @Inject
    public ChatPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void connectToSendbird() {
        SendbirdData data = getDataManager().getSendbirdData();

        if (data != null) {
            SendBird.connect(data.getUserId(), data.getAccessToken(), new SendBird.ConnectHandler() {
                @Override
                public void onConnected(User user, SendBirdException e) {
                    if (e != null) {
                        getMvpView().onConnectFailed(e.getMessage());
                        return;
                    }
                    getMvpView().onConnectSuccess();
                    getDataManager().registerSendbirdPushToken();
                }
            });
        } else {
            getMvpView().onConnectFailed("Error! Null sendbird data");
        }
    }
}
