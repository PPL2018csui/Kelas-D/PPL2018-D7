package ppld7.klilink.ui.main;

import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Ibam on 3/28/2018.
 */

public interface MainMvpPresenter<V extends MvpView> extends MvpPresenter<V>{
    void connectToSendbird();
}
