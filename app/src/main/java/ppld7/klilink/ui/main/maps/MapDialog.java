package ppld7.klilink.ui.main.maps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import ppld7.klilink.R;
import ppld7.klilink.data.db.model.MapPopup;
import ppld7.klilink.ui.base.BaseDialog;
import ppld7.klilink.ui.main.order.OrderActivity;

/**
 * Created by Ibam on 4/1/2018.
 */

public class MapDialog extends BaseDialog {

    private static final String TAG = "RateUsDialog";
    private static final String KEY_NAME = "name";
    private static final String KEY_HOURS = "hours";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_PROFILE_PICTURE = "profile";

    TextView tvStorename;

    TextView tvOpenHours;

    View mRatingMessageView;

    CircleImageView profilePicture;

    Button btnClose;
    Button btnOrder;


    public static MapDialog newInstance(MapPopup model) {
        MapDialog fragment = new MapDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_NAME, model.getName());
        bundle.putString(KEY_HOURS, model.getStorename());
        bundle.putString(KEY_PHONE, model.getPhoneNumber());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.popup_marker, container, false);

        setUp(view);

        return view;
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }


    @Override
    protected void setUp(View view) {

        btnClose = view.findViewById(R.id.close_button);
        btnOrder = view.findViewById(R.id.order_button);
        tvOpenHours = view.findViewById(R.id.popup_openhours);
        tvStorename = view.findViewById(R.id.popup_name);
        profilePicture = view.findViewById(R.id.popup_profile_picture);

        MapPopup model = getModelFromBundle();
        final String phoneNumber = model.getPhoneNumber();
        tvOpenHours.setText(model.getStorename());
        tvStorename.setText(model.getName());
        Glide.with(getContext()).load(R.drawable.ic_default_user).into(profilePicture);

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent orderIntent = new Intent(getContext(), OrderActivity.class);
                orderIntent.putExtra("PHONE_NUMBER", phoneNumber);
                startActivity(orderIntent);
                dismissDialog();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialog();
            }
        });
    }

    private MapPopup getModelFromBundle() {
        Bundle bundle = getArguments();
        String name = bundle.getString(KEY_NAME, "dummy");
        String hours = bundle.getString(KEY_HOURS, "00");
        String phone = bundle.getString(KEY_PHONE, "00");
        String profile = bundle.getString(KEY_PROFILE_PICTURE, "profile");

        MapPopup mapPopup = MapPopup.getInstance(name, hours, phone, profile);

        return mapPopup;
    }


    public void dismissDialog() {
        super.dismissDialog(TAG);
    }
}
