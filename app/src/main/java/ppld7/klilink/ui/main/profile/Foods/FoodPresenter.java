package ppld7.klilink.ui.main.profile.Foods;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.menu.GetMenuResponse;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by Dwi Nanda on 26/04/2018.
 */

public class FoodPresenter<V extends FoodMvpView> extends BasePresenter<V> implements FoodMvpPresenter<V> {

    Retrofit retrofit;
    ProyekinApi api;
    Call<GetMenuResponse> call;

    @Inject
    public FoodPresenter(DataManager dataManager){
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi(){
        return retrofit.create(ProyekinApi.class);
    }



    @Override
    public void onFoodRequest() {

    }
}