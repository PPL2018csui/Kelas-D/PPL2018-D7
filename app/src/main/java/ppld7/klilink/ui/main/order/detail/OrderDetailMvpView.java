package ppld7.klilink.ui.main.order.detail;

import ppld7.klilink.data.network.model.response.order.detail.OrderDetailResponse;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Fadhil on 25-May-18.
 */

public interface OrderDetailMvpView extends MvpView {

    void onRequestOrderDetailSuccess(OrderDetailResponse orderDetailResponse);

    void onRequestOrderDetailFailed(String message);
}
