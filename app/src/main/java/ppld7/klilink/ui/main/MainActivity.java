package ppld7.klilink.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.ui.main.maps.MapFragment;
import ppld7.klilink.ui.main.order.list.OrderListFragment;
import ppld7.klilink.ui.main.profile.ProfileFragment;

public class MainActivity extends BaseActivity implements AHBottomNavigation
        .OnTabSelectedListener, MainMvpView {

    private static final int NAVIGATION_MESSAGING = 0;
    private static final int NAVIGATION_MAPS = 1;
    private static final int NAVIGATION_PROFILE = 2;
    private static final String TAG = "MainActivity";

    AHBottomNavigation bottomNavigationView;
    Fragment activeFragment;
    MapFragment mapFragment;
    OrderListFragment messagingFragment;
    ProfileFragment profileFragment;
    @Inject
    MainMvpPresenter<MainMvpView> presenter;
    private int activeNavigation = NAVIGATION_MAPS;
    private Toolbar toolbar;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActivityComponent().inject(this);
        setUp();
        presenter.connectToSendbird();
    }

    @Override
    public void setUp() {
        mapFragment = new MapFragment();
        messagingFragment = OrderListFragment.newInstance();
        profileFragment = ProfileFragment.newInstance();
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        initBottomNav();


        bottomNavigationView.setCurrentItem(NAVIGATION_MAPS);

        presenter.onAttach(MainActivity.this);

    }

    private void initBottomNav() {
        AHBottomNavigationItem chatTab = new AHBottomNavigationItem(getString(R.string.title_messaging),
                R.drawable.ic_order_list);
        AHBottomNavigationItem mapsTab = new AHBottomNavigationItem(getString(R.string
                .title_maps), R
                .drawable.ic_maps);
        AHBottomNavigationItem profileTab = new AHBottomNavigationItem(getString(R.string
                .title_profile), R.drawable.ic_profile);

        bottomNavigationView.addItem(chatTab);
        bottomNavigationView.addItem(mapsTab);
        bottomNavigationView.addItem(profileTab);

        bottomNavigationView.setDefaultBackgroundColor(ContextCompat.getColor(this, R.color.orange_bottomnav));
        bottomNavigationView.setAccentColor(ContextCompat.getColor(this, R.color.white));
        bottomNavigationView.setInactiveColor(ContextCompat.getColor(this, R.color.black_transparant));
        bottomNavigationView.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigationView.setOnTabSelectedListener(this);
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        onSelectedNavigation(position);
        return true;
    }

    private void onSelectedNavigation(int position) {
        switch (position) {
            case NAVIGATION_MESSAGING:
                showMessageFragment();
                break;
            case NAVIGATION_MAPS:
                showMapsFragment();
                break;
            case NAVIGATION_PROFILE:
                showProfileFragment();
                break;
        }
    }

    public void showMessageFragment() {
        activeNavigation = NAVIGATION_MESSAGING;

        if (profileFragment != null) {
            activeFragment = messagingFragment;
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, activeFragment, OrderListFragment.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void showMapsFragment() {
        activeNavigation = NAVIGATION_MAPS;
        if (profileFragment != null) {
            activeFragment = mapFragment;
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, activeFragment, MapFragment.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    public void showProfileFragment() {
        activeNavigation = NAVIGATION_PROFILE;
        if (profileFragment != null) {
            activeFragment = profileFragment;
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content, activeFragment, ProfileFragment.class.getSimpleName())
                    .commitAllowingStateLoss();
        }
    }

    @Override
    public void onSendbirdFailed(String message) {
        showMessage(message);
    }
}
