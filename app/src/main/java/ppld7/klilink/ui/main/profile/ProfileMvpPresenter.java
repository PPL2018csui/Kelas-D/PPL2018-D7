package ppld7.klilink.ui.main.profile;

import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

public interface ProfileMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void onProfileRequest();

    void logOut();

    void getMenuPedagang();

}
