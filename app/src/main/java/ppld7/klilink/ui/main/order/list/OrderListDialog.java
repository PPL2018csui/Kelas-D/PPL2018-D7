package ppld7.klilink.ui.main.order.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ppld7.klilink.R;
import ppld7.klilink.ui.base.BaseDialog;
import ppld7.klilink.utils.Constants;

/**
 * Created by Fadhil on 27-May-18.
 */

public class OrderListDialog extends BaseDialog {

    private static final String TAG = OrderListDialog.class.getSimpleName();
    private OnDialogClickListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirmation_dialog, container, true);

        setUp(view);

        return view;
    }

    @Override
    protected void setUp(View view) {
        TextView title = view.findViewById(R.id.confirmation_dialog_title);
        TextView message = view.findViewById(R.id.confirmation_dialog_message);
        Button cancelButton = view.findViewById(R.id.confirmation_dialog_cancel_button);
        Button acceptButton = view.findViewById(R.id.confirmation_dialog_accept_button);

        title.setText(Constants.CANCEL_ORDER_TITLE);
        message.setText(Constants.CANCEL_ORDER_MESSAGE);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAcceptButtonClicked();
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void onAttach(OnDialogClickListener listener) {
        this.listener = listener;
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }

    public interface OnDialogClickListener {
        void onAcceptButtonClicked();
    }
}
