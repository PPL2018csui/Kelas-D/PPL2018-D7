package ppld7.klilink.ui.main.profile;

import java.util.List;

import ppld7.klilink.data.db.model.Food;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.MvpView;

public interface ProfileMvpView extends MvpView {

    void onRequestProfileFailed();
    void onRequestProfilePenjualSuccess(UserDataModel userDataModel);
    void onRequestProfilePembeliSuccess(UserDataModel userDataModel);
    void onLoggedOut();
    void onMenuLoaded(List<FoodResponse> foodList);

    void onLoadMenuError(String string);
}
