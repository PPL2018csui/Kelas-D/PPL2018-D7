package ppld7.klilink.ui.main.order;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.order.OrderList;
import ppld7.klilink.data.network.model.request.order.OrderRequest;
import ppld7.klilink.data.network.model.response.order.OrderResponse;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.utils.Constants;

/**
 * Created by Fadhil on 17-May-18.
 */

public class OrderActivity extends BaseActivity implements OrderMvpView {

    @Inject
    OrderMvpPresenter<OrderMvpView> mPresenter;

    private OrderFoodAdapter orderFoodAdapter;
    private RecyclerView recyclerView;
    private String sellerPhoneNumber;
    private View emptyRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);

        emptyRecyclerView = findViewById(R.id.order_empty_recycler);
        TextView title = emptyRecyclerView.findViewById(R.id.empty_recycler_view_title);
        TextView message = emptyRecyclerView.findViewById(R.id.empty_recycler_view_message);
        title.setText(Constants.EMPTY_ORDER_TITLE);
        message.setText(Constants.EMPTY_ORDER_MESSAGE);
        emptyRecyclerView.setVisibility(View.INVISIBLE);

        setUp();
        sellerPhoneNumber = getIntent().getStringExtra("PHONE_NUMBER");
        showLoading();
        mPresenter.requestMenu(sellerPhoneNumber);
    }

    @Override
    protected void setUp() {
        setUpToolbar();
        setUpFoodList();
    }

    @Override
    public void onRequestMenuSuccess(List<FoodResponse> foodList) {
        if (foodList.isEmpty()) {
            emptyRecyclerView.setVisibility(View.VISIBLE);
        } else {
            orderFoodAdapter.setNewData(foodList);
        }
        hideLoading();
    }

    @Override
    public void onRequestMenuFailed(String message) {
        showMessage(message);
    }

    @Override
    public void onMakeOrderSuccess(OrderResponse orderResponse) {
        hideLoading();
        String message = orderResponse.getErr();
        if (!message.isEmpty()) {
            showMessage(message);
        }
        finish();
    }

    @Override
    public void onMakeOrderFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    public void onOrderRequest(List<OrderList> orderLists, int total) {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setPembeli(mPresenter.getCurrentUserPhone());
        orderRequest.setPenjual(sellerPhoneNumber);
        orderRequest.setOrderList(orderLists);
        orderRequest.setHarga(total);
        showLoading();
        mPresenter.makeOrder(orderRequest);
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.order_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setUpFoodList() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView = findViewById(R.id.order_recycler_view);
        LinearLayout orderCartLayout = findViewById(R.id.order_cart);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        orderFoodAdapter = new OrderFoodAdapter(new ArrayList<FoodResponse>(), this);
        orderFoodAdapter.setOrderCartLayout(orderCartLayout);
        recyclerView.setAdapter(orderFoodAdapter);
    }
}
