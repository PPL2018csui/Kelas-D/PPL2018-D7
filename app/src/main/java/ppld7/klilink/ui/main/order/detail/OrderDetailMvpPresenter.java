package ppld7.klilink.ui.main.order.detail;

import ppld7.klilink.ui.base.MvpPresenter;

/**
 * Created by Fadhil on 25-May-18.
 */

public interface OrderDetailMvpPresenter<V extends OrderDetailMvpView> extends MvpPresenter<V> {

    void requestOrderDetail(int orderId);
}
