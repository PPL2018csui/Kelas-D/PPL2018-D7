package ppld7.klilink.ui.main.order.detail;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetailResponse;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetailPresenter<V extends OrderDetailMvpView> extends BasePresenter<V> implements OrderDetailMvpPresenter<V> {

    Retrofit retrofit;
    ProyekinApi api;
    Call<OrderDetailResponse> call;

    @Inject
    public OrderDetailPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    private ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void requestOrderDetail(int orderId) {
        call = api.getOrderDetail(orderId);
        call.enqueue(new Callback<OrderDetailResponse>() {
            @Override
            public void onResponse(Call<OrderDetailResponse> call, Response<OrderDetailResponse> response) {
                if (response.isSuccessful()) {
                    OrderDetailResponse data = response.body();
                    getMvpView().onRequestOrderDetailSuccess(data);
                } else {
                    int code = response.code();
                    getMvpView().onRequestOrderDetailFailed("Error " + code);
                }
            }

            @Override
            public void onFailure(Call<OrderDetailResponse> call, Throwable t) {
                getMvpView().onRequestOrderDetailFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });
    }
}
