package ppld7.klilink.ui.main.order.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.ui.base.BaseActivity;

/**
 * Created by Vyom on 1/3/2017. (https://github.com/ledzep9012/SendbirdDemo/)
 * Modified by Ibam on 5/27/2018.
 */

public class ChatActivity extends BaseActivity implements ChatMvpView, View.OnClickListener {

    public static final String INTENT_KEY_USER_ID = "UserId";
    public static final String INTENT_KEY_USER_NAME = "UserName";
    private static final String TAG = "ChatActivity";
    // The unique channelIdentifier for the group channel handler
    private static final String channelIdentifier = "SendbirdTest";
    // All the channels must be distinct. This will allow them to be reused and get previous messages
    private static final boolean IS_DISTINCT = true;
    // Maximum load 30 messages
    private static final int MAXIMUM_MESSAGES_LOAD = 30;
    @Inject
    ChatMvpPresenter<ChatMvpView> mPresenter;
    // Views
    private RecyclerView mMessagesRecyclerView;
    private EditText mMessageBoxEditText;
    private ImageView mViewButton;


    private Context mContext;
    private String mReceiverId;
    private String mReceiverName;
    private List<UserMessage> mMessagesLit;
    private RecyclerView.LayoutManager mLayoutManager;
    private ChatAdapter mMessagesListAdapter;
    private GroupChannel mGroupChannel;

    public static Intent getStartIntent(String mReceiverId, String mReceiverName, Context context) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(INTENT_KEY_USER_ID, mReceiverId);
        intent.putExtra(INTENT_KEY_USER_NAME, mReceiverName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        mContext = getApplicationContext();

        setUp();
    }

    @Override
    protected void setUp() {
        initViews();
        initRV();
        getUserId();
        setUpToolbar();
        connectSendbird();
    }

    private void connectSendbird() {
        showLoading();
        mPresenter.connectToSendbird();
    }

    @Override
    public void onConnectFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onConnectSuccess() {
        hideLoading();
        createChannel();
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(mReceiverName);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initViews() {
        mMessagesRecyclerView = (RecyclerView) findViewById(R.id.messages_recycler_view);
        mMessageBoxEditText = (EditText) findViewById(R.id.edittext_chat);

        mViewButton = (ImageView) findViewById(R.id.button_chat_send);
        mViewButton.setOnClickListener(this);

    }

    private void getUserId() {
        mReceiverId = getIntent().getExtras().getString(INTENT_KEY_USER_ID);
        mReceiverName = getIntent().getExtras().getString(INTENT_KEY_USER_NAME);
        Log.e(TAG, "The receiver is " + mReceiverId + ":" + mReceiverName);
    }

    private void initRV() {
        mLayoutManager = new LinearLayoutManager(this);
        mMessagesRecyclerView.setLayoutManager(mLayoutManager);
    }

    private void createChannel() {
        List<String> userIdsList = new ArrayList<String>();
        userIdsList.add(mReceiverId);
        GroupChannel.createChannelWithUserIds(userIdsList, IS_DISTINCT, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(mContext, "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mGroupChannel = groupChannel;

                setGeneralChannelHandler();
                // Load messages
                loadMessages(groupChannel);
            }
        });
    }

    private void setGeneralChannelHandler() {
        SendBird.addChannelHandler(channelIdentifier, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                Log.d(TAG, "New message received " + baseMessage.getMessageId());
                if (mGroupChannel != null && baseChannel.getUrl().equals(mGroupChannel.getUrl())) {
                    if (mMessagesListAdapter != null && baseMessage instanceof UserMessage) {
                        printUserMesage((UserMessage) baseMessage);
                        mGroupChannel.markAsRead();
                        mMessagesListAdapter.addMessage((UserMessage) baseMessage);
                        // Move the list to the last item
                        mMessagesRecyclerView.smoothScrollToPosition(mMessagesListAdapter.getItemCount());
                    }
                }
            }

            @Override
            public void onReadReceiptUpdated(GroupChannel groupChannel) {
                Log.d(TAG, "The read receipt has been updated for the channel " + groupChannel.getUrl());
            }

            @Override
            public void onTypingStatusUpdated(GroupChannel groupChannel) {
                Log.d(TAG, "The typing status has been updated for the channel " + groupChannel.getUrl());
            }

            @Override
            public void onUserJoined(GroupChannel groupChannel, User user) {
                Log.d(TAG, "New user joined to the channel " + groupChannel.getUrl() + ", " + user.getNickname());
            }

            @Override
            public void onUserLeft(GroupChannel groupChannel, User user) {
                Log.d(TAG, "User left on the channel " + groupChannel.getUrl() + ", " + user.getNickname());
            }
        });
    }

    private void loadMessages(GroupChannel groupChannel) {
        PreviousMessageListQuery previousMessageListQuery = groupChannel.createPreviousMessageListQuery();
        previousMessageListQuery.load(MAXIMUM_MESSAGES_LOAD, false, new PreviousMessageListQuery.MessageListQueryResult() {
            @Override
            public void onResult(List<BaseMessage> baseMessagesList, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(mContext, "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                mMessagesLit = new ArrayList<UserMessage>();
                mMessagesListAdapter = new ChatAdapter(SendBird.getCurrentUser().getUserId(),
                        mMessagesLit, ChatActivity.this);
                mMessagesRecyclerView.setAdapter(mMessagesListAdapter);

                for (BaseMessage baseMessage : baseMessagesList) {
                    if (baseMessage instanceof UserMessage) {
                        UserMessage userMessage = (UserMessage) baseMessage;
                        printUserMesage(userMessage);
                        mMessagesListAdapter.addMessage(userMessage);
                    }

                    // Move the list to the last item
                    mMessagesRecyclerView.smoothScrollToPosition(mMessagesListAdapter.getItemCount());
                }
            }
        });
    }

    private void printUserMesage(UserMessage userMessage) {
        Log.d(TAG, "User message {" +
                "Sender: " + userMessage.getSender().getNickname() +
                ", Message: " + userMessage.getMessage() +
                ", Data: " + userMessage.getData() +
                ", RequestId: " + userMessage.getRequestId() +
                ", Created at: " + userMessage.getCreatedAt() +
                "}"
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_chat_send:
                if (mGroupChannel == null) {
                    Log.d(TAG, "The group channel must be ready when the user want to send the message");
                    return;
                }

                final String textToSend = mMessageBoxEditText.getText().toString().trim();
                if (!TextUtils.isEmpty(textToSend)) {
                    mGroupChannel.sendUserMessage(textToSend, new BaseChannel.SendUserMessageHandler() {
                        @Override
                        public void onSent(UserMessage userMessage, SendBirdException e) {
                            if (e != null) {
                                Toast.makeText(mContext, "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            Log.v(TAG, "Message sent to" + mReceiverName + ":" + textToSend);
                            mMessageBoxEditText.setText("");

                            // Update the users list
                            mMessagesListAdapter.addMessage(userMessage);
                            // Move the list to the last item
                            mMessagesRecyclerView.smoothScrollToPosition(mMessagesListAdapter.getItemCount());
                        }
                    });
                }
                break;
        }
    }
}