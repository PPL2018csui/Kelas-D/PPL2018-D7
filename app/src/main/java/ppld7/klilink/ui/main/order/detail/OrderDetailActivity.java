package ppld7.klilink.ui.main.order.detail;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetail;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetailResponse;
import ppld7.klilink.ui.base.BaseActivity;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetailActivity extends BaseActivity implements OrderDetailMvpView {

    @Inject
    OrderDetailMvpPresenter<OrderDetailMvpView> mPresenter;
    private RecyclerView recyclerView;
    private OrderDetailAdapter orderDetailAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        getActivityComponent().inject(this);
        mPresenter.onAttach(this);

        setUp();
        int orderId = getIntent().getIntExtra("ORDER_ID", 0);
        showLoading();
        mPresenter.requestOrderDetail(orderId);
    }

    @Override
    protected void setUp() {
        setUpToolbar();
        setUpOrderDetail();
    }

    @Override
    public void onRequestOrderDetailSuccess(OrderDetailResponse orderDetailResponse) {
        List<OrderDetail> foods = orderDetailResponse.getData().getOrderDetails();
        orderDetailAdapter.setNewData(foods);
        hideLoading();
    }

    @Override
    public void onRequestOrderDetailFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.order_detail_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setUpOrderDetail() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView = findViewById(R.id.order_detail_recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        orderDetailAdapter = new OrderDetailAdapter(new ArrayList<OrderDetail>(), this);
        recyclerView.setAdapter(orderDetailAdapter);
    }
}
