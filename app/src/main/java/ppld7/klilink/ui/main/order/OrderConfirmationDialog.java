package ppld7.klilink.ui.main.order;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.order.OrderList;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetail;
import ppld7.klilink.ui.base.BaseDialog;
import ppld7.klilink.ui.main.order.detail.OrderDetailAdapter;
import ppld7.klilink.utils.Constants;

/**
 * Created by Fadhil on 26-May-18.
 */

public class OrderConfirmationDialog extends BaseDialog {

    RecyclerView recyclerView;
    OrderDetailAdapter orderDetailAdapter;

    private static final String TAG = OrderConfirmationDialog.class.getSimpleName();
    private List<OrderDetail> orderDetails;
    private List<OrderList> orderLists;
    private int total;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_confirmation_dialog, container, true);

        setUp(view);

        return view;
    }

    @Override
    protected void setUp(View view) {
        Button cancelButton = view.findViewById(R.id.order_cancel_button);
        Button confirmButton = view.findViewById(R.id.order_confirm_button);
        TextView orderTotal = view.findViewById(R.id.order_confirmation_total);
        orderTotal.setText(Constants.formatPrice(total));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = view.findViewById(R.id.order_confirmation_recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        orderDetailAdapter = new OrderDetailAdapter(new ArrayList<OrderDetail>(), getActivity());
        orderDetailAdapter.setNewData(orderDetails);
        recyclerView.setAdapter(orderDetailAdapter);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderActivity activity = (OrderActivity) getActivity();
                activity.onOrderRequest(orderLists, total);
            }
        });
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void setData(List<OrderList> orderLists, List<OrderDetail> orderDetails, int total) {
        this.orderDetails = orderDetails;
        this.orderLists = orderLists;
        this.total = total;
    }

    public void show(FragmentManager fragmentManager) {
        super.show(fragmentManager, TAG);
    }
}
