package ppld7.klilink.ui.main.maps;

import java.util.List;

import ppld7.klilink.data.network.model.response.map.GetEventResponse;
import ppld7.klilink.data.network.model.response.map.GetPedagangResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Ibam on 3/28/2018.
 */

public interface MapMvpView extends MvpView {

    void onFetchDataSuccess(List<GetPedagangResponse> pedagangList);

    void onFetchEventSuccess(List<GetEventResponse> eventList);

    void onFetchDataError();

    void startTrackingService(UserDataModel data);

    void onRequestUserData(UserDataModel userDataModel);

    void setupMapHeader(boolean isSeller, String name);
}
