package ppld7.klilink.ui.main.order.chat;

import ppld7.klilink.ui.base.MvpView;

public interface ChatMvpView extends MvpView {

    void onConnectFailed(String message);

    void onConnectSuccess();
}
