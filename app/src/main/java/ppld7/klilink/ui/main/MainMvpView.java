package ppld7.klilink.ui.main;

import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Ibam on 3/28/2018.
 */

public interface MainMvpView extends MvpView {

    void onSendbirdFailed(String message);
}
