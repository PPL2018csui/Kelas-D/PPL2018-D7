package ppld7.klilink.ui.splash;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.ui.base.BasePresenter;

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {

    private static final String TAG = "SplashPresenter";


    @Inject
    public SplashPresenter(DataManager dataManager) {
        super(dataManager);
    }


    @Override
    public void decideNextActivity() {
        getMvpView().decideNextActivity(isAuthenticated());
    }
}
