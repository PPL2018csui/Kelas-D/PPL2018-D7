package ppld7.klilink.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.ui.login.LoginActivity;
import ppld7.klilink.ui.main.MainActivity;

public class SplashActivity extends BaseActivity implements SplashMvpView {


    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        getActivityComponent().inject(this);

        mPresenter.onAttach(SplashActivity.this);

        decideNextActivity();
    }

    @Override
    protected void setUp() {

    }

    private void decideNextActivity() {
        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                mPresenter.decideNextActivity();
            }
        }, secondsDelayed * 1000);
    }

    @Override
    public void decideNextActivity(boolean isLoggedin) {
        if (isLoggedin) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(LoginActivity.getStartIntent(this));
        }
        finish();
    }
}
