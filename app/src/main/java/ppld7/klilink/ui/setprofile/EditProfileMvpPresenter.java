package ppld7.klilink.ui.setprofile;

import ppld7.klilink.data.network.model.request.profile.EditProfileRequest;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Farhan on 17/04/2018.
 */

public interface EditProfileMvpPresenter <V extends MvpView> extends MvpPresenter<V> {
    void onSaveButtonClick(EditProfileRequest request);
    void getPrefill();
    void updateProfile(UserDataModel data);
    void updateProfilePictureFirebase();
}
