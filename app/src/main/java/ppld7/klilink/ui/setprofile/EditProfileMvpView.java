package ppld7.klilink.ui.setprofile;

import ppld7.klilink.data.network.model.response.profile.EditProfileResponse;
import ppld7.klilink.data.network.model.response.profile.EditProfileResponseData;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Farhan on 17/04/2018.
 */

public interface EditProfileMvpView extends MvpView {

    void editProfileFailed(String string);

    void editProfileSuccess(EditProfileResponseData data);

    void prefillData(String no_hp, String nama);


}