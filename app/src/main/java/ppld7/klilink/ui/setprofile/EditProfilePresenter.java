package ppld7.klilink.ui.setprofile;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.profile.EditProfileRequest;
import ppld7.klilink.data.network.model.response.profile.EditProfileResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Farhan on 17/04/2018.
 */

public class EditProfilePresenter<V extends EditProfileMvpView> extends BasePresenter<V> implements
        EditProfileMvpPresenter<V> {

    private static final String TAG = "EditProfilePresenter";
    private final String DB_REF = "location";


    Retrofit retrofit;
    ProyekinApi api;
    Call<EditProfileResponse> call;


    Callback<EditProfileResponse> callback = new Callback<EditProfileResponse>() {
        @Override
        public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {

            EditProfileResponse data = response.body();

            if (response.isSuccessful()) {
                getMvpView().editProfileSuccess(data.getData());
            } else {
                getMvpView().editProfileFailed("Response Gagal - Perubahan gagal disimpan");
            }
        }
        @Override
        public void onFailure(Call<EditProfileResponse> call, Throwable t) {
            getMvpView().editProfileFailed(t.toString());
        }
    };

    @Inject
    public EditProfilePresenter(DataManager dataManager){
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    public void updateProfile(UserDataModel data){
        getDataManager().saveUserData(data);
    }

    public void getPrefill(){
        String nama = getDataManager().getCurrentUserName();
        String no_hp = getDataManager().getCurrentUserPhone();
        getMvpView().prefillData(no_hp, nama);
    }


    @Override
    public void onSaveButtonClick(EditProfileRequest request) {
        // TODO presenter call the login API from data manager
        // don't forget to create callback object as global variable
        String no_hp = getDataManager().getCurrentUserPhone();
        request.setNo_hp(no_hp);
        call = api.editProfile(request);
        call.enqueue(callback);
    }

    @Override
    public void updateProfilePictureFirebase() {
        String urlProfile = getDataManager().getUrlPhotoUser();
        String noHp = getDataManager().getCurrentUserPhone();
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("urlProfile", urlProfile);

        FirebaseDatabase firebaseInstance = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseInstance.getReference(DB_REF);
        DatabaseReference dbUserRef = databaseReference.child(noHp);

        dbUserRef.updateChildren(hashMap);
    }
}
