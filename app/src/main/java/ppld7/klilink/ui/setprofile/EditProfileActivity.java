package ppld7.klilink.ui.setprofile;

/**
 * Created by Farhan on 05/03/2018.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.profile.EditProfileRequest;
import ppld7.klilink.data.network.model.response.profile.EditProfileResponseData;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.utils.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditProfileActivity extends BaseActivity implements EditProfileMvpView, View.OnClickListener {

    private EditText etNama;
    private EditText etNoTelp;
    private EditText etPassword;
    private EditText etPassword2;
    private ImageView ivPhotoUser;
    private String uploadedMenuImage;
    final String TAG = "EditProfileActivity";
    Toolbar toolbar;
    Button btnSimpan;
    String error;
    private final int PICK_IMAGE_REQUEST = 71;
    private Uri filePath;
    private Uri imageUri;


    boolean isChanged;
    // firebase storage
    FirebaseStorage storage;
    StorageReference storageReference;


    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        return intent;
    }

    @Inject
    EditProfileMvpPresenter<EditProfileMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        getActivityComponent().inject(this);
        presenter.onAttach(EditProfileActivity.this);
//        imageUri = getIntent().getParcelableExtra(Constants.IMAGE_FILE);
//        if (imageUri != null) {
//            showMessage(imageUri.toString());
//        }
        setUp();
    }

    @Override
    protected void setUp() {
        btnSimpan = (Button) findViewById(R.id.btnSimpan);
        etNama = (EditText) findViewById(R.id.etNama);
        etNoTelp = (EditText) findViewById(R.id.etNoTelp);
        etPassword = (EditText) findViewById(R.id.etEditProfilePw);
        etPassword2 = (EditText) findViewById(R.id.etEditProfilePw2);
        ivPhotoUser = (ImageView) findViewById(R.id.iv_photo_user);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        ivPhotoUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

        if(presenter.getUrlPhotoUser() == null || presenter.getUrlPhotoUser().equals("")){
            final Drawable dr = getResources().getDrawable(R.drawable.ic_default_user);
            ivPhotoUser.setImageDrawable(dr);
        } else {
            Glide.with(this).load(presenter.getUrlPhotoUser()).into(ivPhotoUser);
        }


        setUpToolbar();
        btnSimpan.setOnClickListener(this);
        presenter.getPrefill();
    }

    public void prefillData(String no_hp, String nama){
        etNama.setText(nama);
        etNoTelp.setText(no_hp);
        etNoTelp.setEnabled(false);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void editProfileFailed(String string) {
        hideLoading();
        showMessage(string);
    }

    @Override
    public void editProfileSuccess(EditProfileResponseData data) {
        hideLoading();
        boolean seller = data.getRole().equals("pembeli") ? false : true;
        UserDataModel dataModel = UserDataModel.newInstance(
                data.getNama(),
                data.getNoHp(),
                seller,
                "",
                null,
                null,
                data.getUrlFoto()
        );
        presenter.updateProfile(dataModel);
        presenter.updateProfilePictureFirebase();
        showMessage("Perubahan Disimpan");
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar3);
        toolbar.setTitle("Ubah Profil");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivityForResult(Intent.createChooser(intent, "Pilih Gambar Menu"), PICK_IMAGE_REQUEST);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ivPhotoUser.setImageBitmap(bitmap);
                isChanged = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onClick(View view) {

        String passNotMatch = "Kedua password tidak sesuai";
        String bothEmpty = "Dimohon mengisi hal yang ingin diganti";
        String lessThan6 = "Password harus terdiri atas lebih dari 6 karakter";
        String notLetters = "Nama hanya boleh terdiri atas huruf";
        String validation = validateInput();

        int id = view.getId();
        switch (id) {
            case R.id.btnSimpan:
                hideKeyboard();
                if (validation.equals("notLetters")) {
                    showMessage(notLetters);
                } else if (validation.equals("bothEmpty")) {
                    showMessage(bothEmpty);
                } else if (validation.equals("lessThan6")) {
                    showMessage(lessThan6);
                } else if (validation.equals("passNotMatch")) {
                    showMessage(passNotMatch);
                } else { //success
//                    EditProfileRequest request = EditProfileRequest.newInstance("", etNama.getText().toString(), etPassword.getText().toString());
//                    presenter.onSaveButtonClick(request);
                    final String nomer = presenter.getCurrentUserPhone();
                    final String nama = etNama.getText().toString();
                    final String pass = etPassword.getText().toString();
                showLoading();
                    if (isChanged) {
                        if (filePath != null) {
                            final ProgressDialog dialog = new ProgressDialog(this);
                            dialog.setTitle("Mohon Tunggu...");
                            dialog.show();

                            StorageReference ref = storageReference.child("images_profilepicture/" + java.util.UUID.randomUUID().toString() + ".jpg");
                            ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    dialog.dismiss();
                                    showMessage("Berhasil");
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    uploadedMenuImage = downloadUrl.toString();
                                    Log.e(TAG, "onClick: validateInput " + validateInput());
                                    EditProfileRequest request = EditProfileRequest.newInstance("",nama ,pass,uploadedMenuImage);
                                    presenter.onSaveButtonClick(request);
                                    hideLoading();
                                    finish();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    dialog.dismiss();
                                    showMessage("Gagal" + e.getMessage());
                                }
                            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                                    dialog.setMessage("Proses : " + (int) progress + "%");
                                }
                            });
                        } else {

                        }
                        hideKeyboard();
                    } else {
                        EditProfileRequest request = EditProfileRequest.newInstance("",nama ,pass, presenter.getUrlPhotoUser());
                        presenter.onSaveButtonClick(request);
                        hideLoading();
                        finish();
                    }
                }
                break;
        }
    }

    public boolean validate(){
        boolean valid = true;
        error = "ANDA BELUM MEMILIH GAMBAR";
        String nama = etNama.getText().toString();
        String password = etPassword.getText().toString();
        String password2 = etPassword2.getText().toString();

        if (nama.isEmpty() && password.isEmpty()) {
            valid = false;
            error = "KOLOM NAMA DAN HARGA TIDAK BOLEH KOSONG";
        }
        if (nama.isEmpty()) {
            valid = false;
            error = "NAMA TIDAK BOLEH KOSONG\n";
        }
        if (password.isEmpty()) {
            valid = false;
            error = "PASSWORD TIDAK BOLEH KOSONG\n";
        }

        if(!password.equals(password2)){
            valid = false;
            error = "Password harus sama";
        }
        return valid;

    }

    private String validateInput() {
        //Jika nama kosong. password tidak boleh kosong, password 1 dan 2 harus sama, password lebih dari 6 huruf.
        //Jika password kosong. Nama harus mengandung huruf.
        //Jika nama dan password diganti,
        //  password tidak boleh kosong, password 1 dan 2 harus sama, password lebih dari 6 huruf.
        //  dan nama harus mengandung huruf saja.

        String nama = etNama.getText().toString();
        String password1 = etPassword.getText().toString();
        String password2 = etPassword2.getText().toString();

        if(nama.isEmpty()){
            if(password1.isEmpty() || password2.isEmpty()) {
                return "bothEmpty";
            } else if(!password1.equals(password2)){
               return "passNotMatch";
            } else {
               if(password1.length() <= 6 || password2.length() <= 6){
                   return "lessThan6";
               } else {
                   return "success";
               }
            }
        } else if(password1.isEmpty() || password2.isEmpty()) {
            if(nama.matches("[a-zA-Z ]+")){
                return "success";
            } else {
                return "notLetters";
            }
        } else {
            if(!password1.equals(password2)){
                return "passNotMatch";
            } else if ( password1.length() <= 6 || password2.length() <= 6 ){
                return "lessThan6";
            } else {
                if(nama.matches("[a-zA-Z ]+")){
                    return "success";
                } else {
                    return "notLetters";
                }
            }
        }
    }

}