package ppld7.klilink.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.login.ResetPasswordRequest;
import ppld7.klilink.data.network.model.response.login.ResetPasswordResponse;
import ppld7.klilink.ui.base.BaseActivity;

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordMvpView, View.OnClickListener {


    private EditText etResetCode;
    private EditText etResetPassword;
    private Button btnConfirmation;
    private String kodeUnik;
    private String newPassword;
    ResetPasswordHelper helper;

    @Inject
    ResetPasswordMvpPresenter<ResetPasswordMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        getActivityComponent().inject(this);
        presenter.onAttach(ResetPasswordActivity.this);

        setUp();
        helper = (ResetPasswordHelper) getIntent().getSerializableExtra("ResetPass");
    }


    @Override
    public void onClick(View v) {
    }

    @Override
    protected void setUp() {
        etResetCode = (EditText) findViewById(R.id.et_input_code);
        etResetPassword = (EditText) findViewById(R.id.et_input_reset_pw);
        btnConfirmation = (Button) findViewById(R.id.btn_reset_code);

        btnConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kodeUnik = etResetCode.getText().toString();
                newPassword = etResetPassword.getText().toString();
                if (validate(newPassword)) {
                    ResetPasswordRequest req = ResetPasswordRequest.newInstance(helper.getNo_hp(), newPassword, kodeUnik);
                    presenter.resetPass(req);
                } else {
                    showMessage("Kata sandi baru harus 6 karakter atau lebih");
                }
            }
        });
    }

    @Override
    public void onResetPaasswordSuccess(ResetPasswordResponse response) {
        if (response.getIsSuccess()) {
            hideLoading();
            showMessage("Berhasil Memperbaharui Kata Sandi");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            hideLoading();
            showMessage(response.getErr());
        }
    }

    public boolean validate(String str) {
        boolean valid = true;
        if (str.length() < 6) {
            valid = false;
        }
        return valid;
    }

    @Override
    public void onResetPasswordFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
