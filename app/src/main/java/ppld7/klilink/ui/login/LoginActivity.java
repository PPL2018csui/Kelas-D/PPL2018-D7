package ppld7.klilink.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.login.LoginRequest;
import ppld7.klilink.data.network.model.response.login.LoginData;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.ui.main.MainActivity;
import ppld7.klilink.ui.main.maps.MapFragment;
import ppld7.klilink.ui.register.RegisterActivity;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by janisharali on 27/01/17
 */

public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    Button btnLogin;
    EditText etNoHp;
    EditText etPassword;
    TextView tvHookRegister;
    TextView tvSkip;
    TextView lupaPassword;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Montserrat-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_login);

        getActivityComponent().inject(this);

        mPresenter.onAttach(LoginActivity.this);

        setUp();
    }


    @Override
    public void onLoginSuccess(LoginData loginData) {
        hideLoading();
        boolean seller = loginData.getRole().equals("pembeli") ? false : true;
        UserDataModel currentUser = UserDataModel.newInstance(
                loginData.getNama(),
                loginData.getNoHp(),
                seller,
                loginData.getFcmToken(),
                "",
                "",
                loginData.getUrlFoto()
        );
        mPresenter.setUserLoggedIn(currentUser);
        mPresenter.saveSendbirdData(loginData.getSendbirdData());
        mPresenter.updateProfilePictureFirebase();
        Intent mainActivityIntent = MainActivity.getStartIntent(this);
        startActivity(mainActivityIntent);
        finish();
    }

    @Override
    public void onLoginFailed(String string) {
        hideLoading();
        // TODO when the login failed
        showMessage(string);
    }

    @Override
    protected void setUp() {
        btnLogin = findViewById(R.id.btn_login);
        etNoHp = findViewById(R.id.et_nomor_hp);
        etPassword = findViewById(R.id.et_password);
        tvHookRegister = findViewById(R.id.tv_register_hook);
        tvSkip = findViewById(R.id.tv_skip_login);
        tvHookRegister.setOnClickListener(this);
         btnLogin.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
        lupaPassword = (TextView) findViewById(R.id.tv_forgot_password);

        lupaPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lupaPasswordIntent = PasswordTokenActivity.getStartIntent(getBaseContext());
                startActivity(lupaPasswordIntent);

            }
        });

        if (mPresenter.isAuthenticated()) {
            Intent mainActivityIntent = MainActivity.getStartIntent(this);
            startActivity(mainActivityIntent);
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Intent mainActivityIntent = MainActivity.getStartIntent(this);
        switch (id) {
            case R.id.btn_login:
                if (validate()) {
                    hideKeyboard();
                    LoginRequest loginRequest = LoginRequest.newInstance(
                            etNoHp.getText().toString(),
                            etPassword.getText().toString()
                    );
                    mPresenter.onServerLoginClick(loginRequest);
                } else {
                    showMessage("No. Ponsel atau Password kurang tepat");
                }
                break;
            case R.id.tv_register_hook:
                    startActivity(RegisterActivity.getStartIntent(this));
                    break;
            case R.id.tv_skip_login:
                    startActivity(mainActivityIntent);
                    break;
            default:
                break;
        }
    }

    private boolean validate() {
        // phoneNumber is error when it's empty and it's not an phoneNumber
        String phoneNumber = etNoHp.getText().toString();
        String password = etPassword.getText().toString();

        if (phoneNumber.isEmpty() && !phoneNumber.matches("[0-9]{10,12}")) {
            return false;
        }

        // password is error when it's less than 5 char
        if (password.length() < 6) {
            return false;
        }
        return true;
    }
}
