package ppld7.klilink.ui.login;

import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

public interface PasswordTokenMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
    void requestToken(String no_hp);
}

