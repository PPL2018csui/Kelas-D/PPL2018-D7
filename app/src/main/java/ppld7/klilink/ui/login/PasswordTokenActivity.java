package ppld7.klilink.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.response.login.PasswordTokenResponse;
import ppld7.klilink.data.network.model.response.login.PasswordTokenResponseData;
import ppld7.klilink.ui.base.BaseActivity;

public class PasswordTokenActivity extends BaseActivity implements PasswordTokenMvpView {


    private EditText etInputNomer;
    private Button btnRequesToken;
    private PasswordTokenResponseData passwordTokenResponseData;
    private String no_hp;
    ResetPasswordHelper helper;

    @Inject
    PasswordTokenMvpPresenter<PasswordTokenMvpView> presenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, PasswordTokenActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_token_forgotpassword);
        getActivityComponent().inject(this);
        presenter.onAttach(PasswordTokenActivity.this);
        setUp();
    }



    @Override
    protected void setUp() {
        etInputNomer = (EditText) findViewById(R.id.et_input_nomer);
        btnRequesToken = (Button) findViewById(R.id.btn_request_code);
        btnRequesToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                no_hp = etInputNomer.getText().toString();
                if(no_hp.length() < 1){
                    showMessage("Harap isi kolom nomor handphone");
                } else if(no_hp.length() < 10){
                    showMessage("Nomor harus 10 karakter atau lebih");
                } else {
                    presenter.requestToken(no_hp);
                }
            }
        });

    }

    @Override
    public void onRequestTokenSuccess(PasswordTokenResponse response) {
        if(response.getIs_success()) {
            passwordTokenResponseData = response.getData();
            Intent intent = new Intent(this, ResetPasswordActivity.class);
            helper = new ResetPasswordHelper(passwordTokenResponseData.getNama(), no_hp);
            intent.putExtra("ResetPass", helper);
            startActivity(intent);
            finish();
        } else {
            showMessage(response.getErr());
            hideLoading();
        }
    }

    @Override
    public void onRequestTokenFailed(String str) {
        showMessage("error cuy");
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}
