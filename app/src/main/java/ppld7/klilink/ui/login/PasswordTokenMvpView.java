package ppld7.klilink.ui.login;

import ppld7.klilink.data.network.model.response.login.PasswordTokenResponse;
import ppld7.klilink.ui.base.MvpView;

public interface PasswordTokenMvpView extends MvpView{
    void onRequestTokenSuccess(PasswordTokenResponse response);
    void onRequestTokenFailed(String str);
}
