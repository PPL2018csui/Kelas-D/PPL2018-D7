package ppld7.klilink.ui.login;

import java.io.Serializable;

public class ResetPasswordHelper implements Serializable{
    private String nama;
    private String no_hp;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public ResetPasswordHelper(String nama, String no_hp) {

        this.nama = nama;
        this.no_hp = no_hp;
    }
}
