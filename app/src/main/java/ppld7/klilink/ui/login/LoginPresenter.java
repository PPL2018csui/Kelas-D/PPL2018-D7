/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.ui.login;

import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.login.LoginRequest;
import ppld7.klilink.data.network.model.response.SendbirdData;
import ppld7.klilink.data.network.model.response.login.LoginResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by janisharali on 27/01/17.
 */

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {

    private final String DB_REF = "location";

    Retrofit retrofit;
    ProyekinApi api;
    Call<LoginResponse> call;

    @Inject
    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void setUserLoggedIn(UserDataModel currentUser) {
        getDataManager().saveUserData(currentUser);
    }

    @Override
    public void saveSendbirdData(SendbirdData data) {
        getDataManager().saveSendbirdData(data);
    }

    @Override
    public void onServerLoginClick(final LoginRequest loginRequest) {
        getMvpView().showLoading();
        loginRequest.setFcmToken(getDataManager().getFcmToken());
        call = api.login(loginRequest);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    LoginResponse data = response.body();
                    if (data.getIsSuccess()) {
                        getMvpView().onLoginSuccess(data.getData());
                        getDataManager().setTrackingStatus(true);
                    } else {
                        getMvpView().onLoginFailed(data.getErr());
                    }
                } else {
                    int code = response.code();
                    getMvpView().onLoginFailed("Error " + code + " : Silahkan coba beberapa saat lagi");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                getMvpView().onLoginFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });
    }

    @Override
    public void updateProfilePictureFirebase() {
        String urlProfile = getDataManager().getUrlPhotoUser();
        String noHp = getDataManager().getCurrentUserPhone();
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("urlProfile", urlProfile);

        FirebaseDatabase firebaseInstance = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseInstance.getReference(DB_REF);
        DatabaseReference dbUserRef = databaseReference.child(noHp);

        dbUserRef.updateChildren(hashMap);
    }
}
