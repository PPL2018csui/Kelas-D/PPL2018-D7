package ppld7.klilink.ui.login;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.login.ResetPasswordRequest;
import ppld7.klilink.data.network.model.response.login.ResetPasswordResponse;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ResetPasswordPresenter <V extends ResetPasswordMvpView> extends BasePresenter<V> implements ResetPasswordMvpPresenter<V>   {
    Retrofit retrofit;
    ProyekinApi api;
    Call<ResetPasswordResponse> call;

    @Inject
    public ResetPasswordPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void resetPass(ResetPasswordRequest request) {
        getMvpView().showLoading();
        call = api.resetPassword(request);
        call.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                if(response.isSuccessful()){
                    ResetPasswordResponse resetPasswordResponse = response.body();
                    getMvpView().onResetPaasswordSuccess(resetPasswordResponse);
                } else {
                    getMvpView().onResetPasswordFailed("Gagal memperbaharui password");
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                getMvpView().onResetPasswordFailed(Constants.CONNECTION_ERROR_MESSAGE);
            }
        });

    }
}
