package ppld7.klilink.ui.login;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.response.login.PasswordTokenResponse;
import ppld7.klilink.ui.base.BasePresenter;
import ppld7.klilink.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PasswordTokenPresenter<V extends PasswordTokenMvpView> extends BasePresenter<V> implements
        PasswordTokenMvpPresenter<V>  {
    private final static String TAG = "PasswordTokenPresenter";
    Retrofit retrofit;
    ProyekinApi api;
    Call<PasswordTokenResponse> call;

    @Inject
    public PasswordTokenPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    Callback<PasswordTokenResponse> callback = new Callback<PasswordTokenResponse>() {
        @Override
        public void onResponse(Call<PasswordTokenResponse> call, Response<PasswordTokenResponse> response) {
            if(response.isSuccessful()){
                PasswordTokenResponse passwordTokenResponse = response.body();
                getMvpView().onRequestTokenSuccess(passwordTokenResponse);
            } else {
                int code = response.code();
                getMvpView().onRequestTokenFailed("Error " + code + ": Gagal meminta kode unik");
            }
        }

        @Override
        public void onFailure(Call<PasswordTokenResponse> call, Throwable t) {
            getMvpView().onRequestTokenFailed(Constants.CONNECTION_ERROR_MESSAGE);
        }
    };


    @Override
    public void requestToken(String no_hp) {
        getMvpView().showLoading();
        call = api.getToken(no_hp);
        call.enqueue(callback);
    }
}
