package ppld7.klilink.ui.login;

import ppld7.klilink.data.network.model.response.login.ResetPasswordResponse;
import ppld7.klilink.ui.base.MvpView;

public interface ResetPasswordMvpView extends MvpView {

    void onResetPaasswordSuccess(ResetPasswordResponse response);

    void onResetPasswordFailed(String message);
}
