package ppld7.klilink.ui.login;

import ppld7.klilink.data.network.model.request.login.ResetPasswordRequest;
import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

public interface ResetPasswordMvpPresenter <V extends MvpView> extends MvpPresenter<V>  {
    void resetPass(ResetPasswordRequest request);
}
