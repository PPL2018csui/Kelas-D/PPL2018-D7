package ppld7.klilink.ui.menu_add_edit;

import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Dwi Nanda on 23/05/2018.
 */

public interface MenuMvpView extends MvpView {
    void onAddMenuFailed(String message);

    void onAddMenuSuccess();

    void onEditMenuFailed(String message);

    void onEditMenuSuccess();

    void onDeleteMenuFailed(String message);

    void onDeleteMenuSuccess();
}
