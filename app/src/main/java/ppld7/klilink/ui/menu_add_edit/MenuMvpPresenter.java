package ppld7.klilink.ui.menu_add_edit;

import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.data.network.model.request.profile.DeleteMenuRequest;
import ppld7.klilink.data.network.model.request.profile.EditMenuRequest;
import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Dwi Nanda on 23/05/2018.
 */

public interface MenuMvpPresenter <V extends MvpView> extends MvpPresenter<V> {
    void addMenu(AddMenuRequest req);

    void editMenu(EditMenuRequest req);

    void deleteMenu(DeleteMenuRequest req);
}
