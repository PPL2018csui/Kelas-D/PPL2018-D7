package ppld7.klilink.ui.menu_add_edit;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.data.network.model.request.profile.DeleteMenuRequest;
import ppld7.klilink.data.network.model.request.profile.EditMenuRequest;
import ppld7.klilink.data.network.model.response.profile.AddMenuResponse;
import ppld7.klilink.data.network.model.response.profile.DeleteMenuResponse;
import ppld7.klilink.data.network.model.response.profile.EditMenuResponse;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Dwi Nanda on 23/05/2018.
 */

public class MenuPresenter <V extends MenuMvpView> extends BasePresenter<V> implements
        MenuMvpPresenter<V> {
    Retrofit retrofit;
    ProyekinApi api;
    Call<AddMenuResponse> callAddMenu;
    Call<EditMenuResponse> callEditMenu;
    Call<DeleteMenuResponse> callDeleteMenu;

    @Inject
    public MenuPresenter(DataManager dataManager){
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    @Override
    public void addMenu(AddMenuRequest req) {
        getMvpView().showLoading();
        callAddMenu = api.addMenu(req);
        callAddMenu.enqueue(callbackAddMenu);
    }

    @Override
    public void editMenu(EditMenuRequest req) {
        getMvpView().showLoading();
        callEditMenu = api.editMenu(req);
        callEditMenu.enqueue(callbackEditMenu);
    }

    @Override
    public void deleteMenu(DeleteMenuRequest req) {
        getMvpView().showLoading();
        callDeleteMenu = api.deleteMenu(req);
        callDeleteMenu.enqueue(callbackDeleteMenu);
    }

    Callback<DeleteMenuResponse> callbackDeleteMenu = new Callback<DeleteMenuResponse>() {
        @Override
        public void onResponse(Call<DeleteMenuResponse> call, Response<DeleteMenuResponse> response) {
            if (response.isSuccessful()) {
                getMvpView().onDeleteMenuSuccess();
            } else {
                getMvpView().onDeleteMenuFailed("Gagal Delete Menu");
            }
        }
        @Override
        public void onFailure(Call<DeleteMenuResponse> call, Throwable t) {
            getMvpView().onDeleteMenuFailed(t.getMessage() + "\n" + t.getLocalizedMessage());
        }
    };

    Callback<AddMenuResponse> callbackAddMenu = new Callback<AddMenuResponse>() {
        @Override
        public void onResponse(Call<AddMenuResponse> call, Response<AddMenuResponse> response) {
            if (response.isSuccessful()) {
                getMvpView().onAddMenuSuccess();
            } else {
                getMvpView().onAddMenuFailed("Gagal menambahkan menu");
            }
        }
        @Override
        public void onFailure(Call<AddMenuResponse> call, Throwable t) {
            getMvpView().onAddMenuFailed(t.getMessage() + "\n" + t.getLocalizedMessage());
        }
    };

    Callback<EditMenuResponse> callbackEditMenu = new Callback<EditMenuResponse>() {
        @Override
        public void onResponse(Call<EditMenuResponse> call, Response<EditMenuResponse> response) {
            if (response.isSuccessful()) {
                getMvpView().onEditMenuSuccess();
            } else {
                getMvpView().onEditMenuFailed("Gagal mengganti informasi menu");
            }
        }
        @Override
        public void onFailure(Call<EditMenuResponse> call, Throwable t) {
            getMvpView().onEditMenuFailed(t.getMessage() + "\n" + t.getLocalizedMessage());
        }
    };
}
