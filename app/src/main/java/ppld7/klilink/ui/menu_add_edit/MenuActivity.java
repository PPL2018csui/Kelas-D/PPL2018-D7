package ppld7.klilink.ui.menu_add_edit;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.data.network.model.request.profile.DeleteMenuRequest;
import ppld7.klilink.data.network.model.request.profile.EditMenuRequest;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.ui.setprofile.EditProfileActivity;
import retrofit2.http.DELETE;

/**
 * Created by Dwi Nanda on 23/05/2018.
 */

public class MenuActivity extends BaseActivity implements MenuMvpView, View.OnClickListener {
    private static final String TAG = "MenuActivity";
    private boolean isReady;

    @Inject
    MenuMvpPresenter<MenuMvpView> menuPresenter;

    // Variables
    private EditText etNamaMenu;
    private EditText etHarga;
    private Spinner spnStatusStok;
    private Button btnChooseImage;
    private ImageView ivMenu;
    private Uri filePath;
    private RadioButton rbHabis, rbMasihAda;
    private final int PICK_IMAGE_REQUEST = 71;
    private String uploadedMenuImage;
    String error;
    boolean isChanged = true;
    private Toolbar toolbar;

    // Firebase needs
    FirebaseStorage storage;
    StorageReference storageReference;

    FoodResponse response;
    boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_menu);
        getActivityComponent().inject(this);
        menuPresenter.onAttach(MenuActivity.this);
        setUp();
        response = (FoodResponse) getIntent().getSerializableExtra("FoodPass");

        Log.d("IN", String.valueOf(response != null));
        if (response != null) {
            isEdit = true;
            Log.d("OUT", String.valueOf(response != null));
        }

        if (isEdit) {
            setEdit();
        } else {
            setEmpty();
        }
    }


    @Override
    protected void setUp() {
        // set custom component of dialogs
        etNamaMenu = (EditText) findViewById(R.id.et_nama_menu);
        etHarga = (EditText) findViewById(R.id.et_harga_makanan);
        btnChooseImage = (Button) findViewById(R.id.btn_choose_img);
        ivMenu = (ImageView) findViewById(R.id.img_menu);
        isChanged = false;
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        rbHabis = (RadioButton) findViewById(R.id.rb_habis);
        rbMasihAda = (RadioButton) findViewById(R.id.rb_masih_ada);

        setupToolbar();

    }

    public void setEdit() {
        String urlFotoMenu = response.getUrlFotoMenu();
        String namaMenu = response.getNamaMenu();
        String harga = response.getHarga();
        boolean stok = response.getIsReady();
        uploadedMenuImage = urlFotoMenu;
        isChanged = true;
        if (stok) {
            rbMasihAda.setChecked(true);
        } else {
            rbHabis.setChecked(true);
        }

        Glide.with(this).load(urlFotoMenu).into(ivMenu);
        etNamaMenu.setText(namaMenu);
        etHarga.setText(harga);

    }

    public void setEmpty() {
        final Drawable defaultImage = getResources().getDrawable(R.drawable.empty_image);
        ivMenu.setImageDrawable(defaultImage);
        etNamaMenu.setText("");
        etHarga.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(isEdit){
            getMenuInflater().inflate(R.menu.menu_editmenu, menu);
        } else{
            getMenuInflater().inflate(R.menu.menu_addmenu, menu);
        }

        return true;
    }

    public void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_menu);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(isEdit){
            toolbar.setTitle("Ubah Menu");
            toolbar.inflateMenu(R.menu.menu_editmenu);
        } else {
            toolbar.setTitle("Tambah Menu");
            toolbar.inflateMenu(R.menu.menu_addmenu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_save_menu:

                final String namaMenu = etNamaMenu.getText().toString();
                final String harga = etHarga.getText().toString();
                if(rbMasihAda.isChecked()){
                    isReady = true;
                }else {
                    isReady = false;
                }


                final boolean isStockMasihAda = isReady;
                final String nomer = menuPresenter.getCurrentUserPhone();


                if (validate() && isChanged) {
                    if (filePath != null) {
                        final ProgressDialog dialog = new ProgressDialog(this);
                        dialog.setTitle("Mohon Tunggu...");
                        dialog.show();

                        StorageReference ref = storageReference.child("images/" + java.util.UUID.randomUUID().toString() + ".jpg");
                        ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                dialog.dismiss();
                                showMessage("Berhasil");
                                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                uploadedMenuImage = downloadUrl.toString();
                                Log.e(TAG, "onClick: validate " + validate());

                                showLoading();
                                if(isEdit){
                                    int id_menu = response.getIdMenu();
                                    EditMenuRequest req = EditMenuRequest.newInstance(id_menu, nomer, namaMenu, uploadedMenuImage, harga, isStockMasihAda);
                                    menuPresenter.editMenu(req);
                                } else {
                                    AddMenuRequest req = AddMenuRequest.newInstance(nomer, namaMenu, uploadedMenuImage, harga, isStockMasihAda);
                                    menuPresenter.addMenu(req);
                                }
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                dialog.dismiss();
                                showMessage("Gagal" + e.getMessage());
                            }
                        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                                dialog.setMessage("Proses : " + (int) progress + "%");
                            }
                        });
                    } else {
                        if(isEdit){
                            uploadedMenuImage = response.getUrlFotoMenu();
                            int id_menu = response.getIdMenu();
                            showLoading();
                            EditMenuRequest req = EditMenuRequest.newInstance(id_menu, nomer, namaMenu, uploadedMenuImage, harga, isStockMasihAda);
                            menuPresenter.editMenu(req);
                            finish();
                        }
                    }
                    hideKeyboard();

                } else {
                    Log.e("err", "errormssg");
                    showMessage(error);
                }

                return true;
            case R.id.btn_delete_menu:
                String no_hp = menuPresenter.getCurrentUserPhone();
                int id_menu = response.getIdMenu();
                Log.d("DELETE-DELETE-DELETE", "KICUTTTTT");
                DeleteMenuRequest req = DeleteMenuRequest.newInstance(id_menu, no_hp);
                menuPresenter.deleteMenu(req);
                showMessage("Berhasil Menghapus");
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            startActivityForResult(Intent.createChooser(intent, "Pilih Gambar Menu"), PICK_IMAGE_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                ivMenu.setImageBitmap(bitmap);
                isChanged = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // add menu response
    @Override
    public void onAddMenuFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onAddMenuSuccess() {
        hideLoading();
    }


    // edit menu response
    @Override
    public void onEditMenuFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onEditMenuSuccess() {
        hideLoading();
    }

    @Override
    public void onDeleteMenuFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onDeleteMenuSuccess() { hideLoading();}



    public void chooseImage(View view) {
        chooseImage();
    }


    private boolean validate() {
        boolean valid = true;
        error = "ANDA BELUM MEMILIH GAMBAR";
        String namaMenu = etNamaMenu.getText().toString();
        String harga = etHarga.getText().toString();

        if (namaMenu.isEmpty() && harga.isEmpty()) {
            valid = false;
            error = "KOLOM NAMA DAN HARGA TIDAK BOLEH KOSONG";
        }
        if (namaMenu.isEmpty()) {
            valid = false;
            error = "NAMA TIDAK BOLEH KOSONG\n";
        }
        if (harga.isEmpty()) {
            valid = false;
            error = "HARGA TIDAK BOLEH KOSONG\n";
        }
        return valid;
    }



}
