package ppld7.klilink.ui.Event;

import ppld7.klilink.data.network.model.request.event.EventRequest;
import ppld7.klilink.ui.base.MvpPresenter;
import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Farhan on 10/05/2018.
 */

public interface EventMvpPresenter <V extends MvpView> extends MvpPresenter<V> {

    void onAddButtonClick(EventRequest request);
    void getPrefill();
}
