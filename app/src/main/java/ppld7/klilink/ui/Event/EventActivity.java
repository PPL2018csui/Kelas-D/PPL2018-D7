package ppld7.klilink.ui.Event;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.event.EventRequest;
import ppld7.klilink.ui.base.BaseActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;


/**
 * Created by Farhan on 10/05/2018.
 */

public class EventActivity extends BaseActivity implements View.OnClickListener, EventMvpView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private EditText etNamaEvent;
    private EditText etNoTelpEvent;
    private EditText etDeskripsi;
    private EditText etWaktuMulai;
    private EditText etWaktuSelesai;
    Toolbar toolbar;
    Button btnAddEvent;

    private Location mLastLocation;
    private ImageButton btPlacesAPI;
    private TextView tvPlaceAPI;
    private GoogleApiClient mGoogleApiClient;
    private TextView tvLocation;
    private ProgressDialog dialog;

    private int PLACE_PICKER_REQUEST = 1;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, EventActivity.class);
        return intent;
    }

    @Inject
    EventMvpPresenter<EventMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupGoogleAPI();
        setupDialog();
        setContentView(R.layout.activity_addevent);
        getActivityComponent().inject(this);
        presenter.onAttach(EventActivity.this);
        setUp();
    }

    @Override
    protected void setUp() {
        setUpToolbar();

        btnAddEvent = (Button) findViewById(R.id.btnAddEvent);
        etNamaEvent = (EditText) findViewById(R.id.etNamaEvent);
        etNoTelpEvent = (EditText) findViewById(R.id.etNoTelpEvent);
        etDeskripsi = (EditText) findViewById(R.id.etDeskripsi);
        etWaktuMulai = (EditText) findViewById(R.id.etWaktuMulai);
        //SetTime fromTimeMulai = new SetTime(etWaktuMulai, this);
        etWaktuSelesai = (EditText) findViewById(R.id.etWaktuSelesai);
        //SetTime fromTimeSelesai = new SetTime(etWaktuSelesai, this);
        tvLocation = (TextView) findViewById(R.id.tv_place_latitude_longitude);
        btPlacesAPI = (ImageButton)findViewById(R.id.bt_ppicker);
        tvPlaceAPI = findViewById(R.id.et_place_id);
        setTimePicker();
        btPlacesAPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // membuat Intent untuk Place Picker
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                Intent intent;
                try {
                    intent = builder.build(EventActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                    System.out.println("start activity for result");
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        presenter.getPrefill();
        btnAddEvent.setOnClickListener(this);
    }

    public void prefillData(String no_hp){
        etNoTelpEvent.setText(no_hp);
        etNoTelpEvent.setEnabled(false);
    }

    public void setTimePicker(){
        etWaktuMulai.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(EventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String selHour = "" + selectedHour;
                            String selMin = "" + selectedMinute;
                            if(selHour.length() < 2){
                                selHour = "0" + selectedHour;
                            }
                            if(selMin.length() < 2){
                                selMin =  "0" + selectedMinute;
                            }
                            String settext = selHour + ":" + selMin;
                            etWaktuMulai.setText(settext);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }

            }
        });

        etWaktuSelesai.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(EventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            String selHour = "" + selectedHour;
                            String selMin = "" + selectedMinute;
                            if(selHour.length() < 2){
                                selHour = "0" + selectedHour;
                            }
                            if(selMin.length() < 2){
                                selMin =  "0" + selectedMinute;
                            }
                            String settext = selHour + ":" + selMin;
                            etWaktuSelesai.setText(settext);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }

            }
        });
    }

    private void setupDialog(){
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage("Loading");
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar_addevent);
        toolbar.setTitle("Tambah Event");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // menangkap hasil balikan dari Place Picker, dan menampilkannya pada TextView
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                String toastMsg = String.format("Nama Tempat: %s \n" + "Alamat: %s\n", place.getName(), place.getAddress());
                String loc_lat_lng = place.getLatLng().latitude + ", " + place.getLatLng().longitude;
                tvPlaceAPI.setText(toastMsg);
                tvPlaceAPI.setTextSize(20);
                tvLocation.setText(loc_lat_lng);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnAddEvent:
                hideKeyboard();
                if(validate()){
                    showLoading();
                    EventRequest request = EventRequest.newInstance(etNoTelpEvent.getText().toString(), etNamaEvent.getText().toString(), etDeskripsi.getText().toString(), etWaktuMulai.getText().toString(), etWaktuSelesai.getText().toString(), tvLocation.getText().toString());
                    presenter.onAddButtonClick(request);
                } else {
                    showMessage("Harap mengisi semua kolom");
                }
                break;
        }
    }

    @Override
    public void addEventSuccess() {
        hideLoading();
        showMessage("Event Sukses Ditambah");
        finish();
    }

    @Override
    public void addEventFailed(String string) {
        hideLoading();
        showMessage(string);
    }

    private boolean validate(){
        String nama = etNamaEvent.getText().toString();
        String deskripsi = etDeskripsi.getText().toString();
        String buka = etWaktuMulai.getText().toString();
        String tutup = etWaktuSelesai.getText().toString();
        String eWaktuMulai = etWaktuMulai.getText().toString();
        String eWaktuSelesai = etWaktuMulai.getText().toString();

        if(nama.length() < 1 || deskripsi.length()  < 1 || buka.length()  < 1 || tutup.length()  < 1 || eWaktuMulai.length() < 1 || eWaktuSelesai.length() < 1){
            return false;
        } else {
            return true;
        }
    }

    private void setupGoogleAPI(){
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mGoogleApiClient != null) {
            if (mLastLocation != null) {
                Toast.makeText(this," Connected to Google Location API", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

}
