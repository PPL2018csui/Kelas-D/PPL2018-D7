package ppld7.klilink.ui.Event;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.event.EventRequest;
import ppld7.klilink.data.network.model.response.event.EventResponse;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Farhan on 10/05/2018.
 */

public class EventPresenter <V extends EventMvpView> extends BasePresenter<V> implements EventMvpPresenter<V> {

    private static final String TAG = "EventPresenter";

    Retrofit retrofit;
    ProyekinApi api;
    Call<EventResponse> call;

    @Inject
    public EventPresenter(DataManager dataManager){
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    public ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    public void getPrefill(){
        String no_hp = getDataManager().getCurrentUserPhone();
        getMvpView().prefillData(no_hp);
    }

    Callback<EventResponse> callback = new Callback<EventResponse>() {
        @Override
        public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
            EventResponse data = response.body();
            if(response.isSuccessful()){
                getMvpView().addEventSuccess();
            } else {
                getMvpView().addEventFailed("Response Gagal - Perubahan gagal disimpan");
            }
        }
        @Override
        public void onFailure(Call<EventResponse> call, Throwable t) {
            getMvpView().addEventFailed(t.toString());
        }
    };

    @Override
    public void onAddButtonClick(EventRequest request) {
        String no_hp = getDataManager().getCurrentUserPhone();
        request.setNoHp(no_hp);
        call = api.addEvent(request);
        call.enqueue(callback);
    }
}
