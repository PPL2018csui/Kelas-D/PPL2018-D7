package ppld7.klilink.ui.Event;

import ppld7.klilink.ui.base.MvpView;

/**
 * Created by Farhan on 10/05/2018.
 */

public interface EventMvpView extends MvpView{

    void addEventSuccess();

    void addEventFailed(String string);

    void prefillData(String noHp);
}
