package ppld7.klilink.ui.register.verify;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.register.VerifyRequest;
import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class VerifyPresenter<V extends VerifyMvpView> extends BasePresenter<V>
        implements VerifyMvpPresenter<V> {

    private static final String TAG = "VerifyPresenter";
    Retrofit retrofit;
    ProyekinApi api;

    private Callback<GenericResponse> callback = new Callback<GenericResponse>() {
        @Override
        public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
            if (response.isSuccessful()) {
                if (response.body().getIsSuccess()) {
                    getMvpView().onVerifySuccess("Verifikasi sukses dilakukan, silahkan login.");
                } else {
                    getMvpView().onVerifyFailed(response.body().getErr());
                }
            } else {
                getMvpView().onVerifyFailed("Verifikasi Gagal");
            }
        }

        @Override
        public void onFailure(Call<GenericResponse> call, Throwable t) {
            getMvpView().onVerifyFailed(t.getMessage() + "\n" + t.getLocalizedMessage());
        }
    };

    @Inject
    public VerifyPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    private ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }


    @Override
    public void verifyCode(VerifyRequest req) {
        getMvpView().showLoading();
        Call<GenericResponse> call = api.verifyNoHp(req);
        call.enqueue(callback);
    }
}
