package ppld7.klilink.ui.register.verify;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.register.VerifyRequest;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.ui.login.LoginActivity;
import ppld7.klilink.ui.register.RegisterActivity;

public class VerifyActivity extends BaseActivity implements VerifyMvpView, View.OnClickListener {

    Button btnVerify;
    EditText etNoHp;
    EditText etUniqueCode;
    Toolbar toolbar;
    String errorMessage;
    @Inject
    VerifyPresenter<VerifyMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, VerifyActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void setUp() {
        btnVerify = findViewById(R.id.btn_verify);
        etUniqueCode = findViewById(R.id.et_unicode);
        etNoHp = findViewById(R.id.et_no_hp);

        btnVerify.setOnClickListener(this);

        setUpToolbar();
    }

    @Override
    public void onVerifySuccess(String message) {
        hideLoading();
        showMessage(message);
        Intent intent = LoginActivity.getStartIntent(this);
        startActivity(intent);
    }

    @Override
    public void onVerifyFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_verify:
                hideKeyboard();
                if (validate()) {
                    String noHp = etNoHp.getText().toString().trim();
                    String uniqueCode = etUniqueCode.getText().toString().trim();
                    VerifyRequest request = new VerifyRequest(noHp, uniqueCode);
                    mPresenter.verifyCode(request);
                } else {
                    showMessage(errorMessage);
                }
                break;
            default:
                break;
        }
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Verifikasi");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private boolean validate() {
        boolean isValid = true;
        errorMessage = "";

        String noHp = etNoHp.getText().toString().trim();
        String uniqueCode = etUniqueCode.getText().toString().trim();

        if (noHp.isEmpty()) {
            isValid = false;
            errorMessage = "No. HP tidak boleh kosong\n";
        }

        if (noHp.length() < 10) {
            isValid = false;
            errorMessage = "No. HP tidak boleh kurang dari 10 digit\n";
        }

        if (uniqueCode.isEmpty()) {
            isValid = false;
            errorMessage = "Unique code tidak boleh kosong\n";
        }

        return isValid;
    }
}
