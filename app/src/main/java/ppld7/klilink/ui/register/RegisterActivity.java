package ppld7.klilink.ui.register;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.UUID;

import javax.inject.Inject;

import ppld7.klilink.R;
import ppld7.klilink.data.network.model.request.register.RegisterRequest;
import ppld7.klilink.data.network.model.response.register.Data;
import ppld7.klilink.ui.base.BaseActivity;
import ppld7.klilink.ui.register.verify.VerifyActivity;

public class RegisterActivity extends BaseActivity implements RegisterMvpView, View.OnClickListener {

    private static final String TAG = "RegisterActivity";
    Toolbar toolbar;
    Button btnRegister;
    EditText etNama;
    EditText etNomorHp;
    EditText etPassword;
    EditText etPasswordConf;
    RadioButton rbPembeli;
    RadioButton rbPenjual;
    String errorMessage;
    TextView tvVerify;

    @Inject
    RegisterMvpPresenter<RegisterMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        return intent;
    }

    public static String generateString() {
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(0, 14);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    public void setUp() {
        btnRegister = findViewById(R.id.btn_register);
        etNomorHp = findViewById(R.id.et_nomor_hp);
        etNama = findViewById(R.id.et_nama);
        etPassword = findViewById(R.id.et_password);
        etPasswordConf = findViewById(R.id.et_password_conf);
        rbPembeli = findViewById(R.id.rb_pembeli);
        rbPenjual = findViewById(R.id.rb_penjual);
        tvVerify = findViewById(R.id.verify_text);
        setUpToolbar();
        tvVerify.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
//        randomInput();
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Registrasi");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                hideKeyboard();
                if (validate()) {
                    Log.e(TAG, "onClick: validate " + validate());
                    String nama = etNama.getText().toString();
                    String password = etPassword.getText().toString();
                    String noHp = etNomorHp.getText().toString();
                    String role = "";

                    if (rbPenjual.isChecked()) {
                        role = "pedagang";
                    } else {
                        role = "pembeli";
                    }
                    RegisterRequest req = RegisterRequest.newInstance(noHp, nama, role, password);
                    mPresenter.register(req);

                } else {
                    Log.e(TAG, errorMessage);
                    showMessage(errorMessage);
                }
                break;
            case R.id.verify_text:
                Intent intent = VerifyActivity.getStartIntent(this);
                startActivity(intent);
            default:
                break;
        }
    }

    @Override
    public void onRegisterFailed(String message) {
        hideLoading();
        showMessage(message);
    }

    @Override
    public void onRegisterSuccess(Data registerData) {
        hideLoading();
        startActivity(VerifyActivity.getStartIntent(this));
        showMessage("Masukkan No HP dan kode verifikasi untuk melakukan verifikasi.");
        finish();
    }


    private boolean validate() {

        boolean isValid = true;
        errorMessage = "";
        String name = etNama.getText().toString();
        String password = etPassword.getText().toString();
        String passwordConf = etPasswordConf.getText().toString();
        String noHp = etNomorHp.getText().toString();

        if (name.isEmpty()) {
            isValid = false;
            errorMessage = "Nama tidak boleh kosong\n";
        }

        if (noHp.isEmpty() || noHp.length() < 10) {
            isValid = false;
            errorMessage += "Jumlah No HP minimal 10 digit\n";
        }

        if (password.length() < 6) {
            isValid = false;
            errorMessage += "Panjang password minimal 6 karakter\n";
        } else {
            if (!password.equals(passwordConf)) {
                isValid = false;
                errorMessage += "Password yang diulang harus sama\n";
            }
        }

        if (!(rbPenjual.isChecked() || rbPembeli.isChecked())) {
            isValid = false;
            errorMessage += "Pilih minimal 1 peran";
        }

        return isValid;
    }
}

