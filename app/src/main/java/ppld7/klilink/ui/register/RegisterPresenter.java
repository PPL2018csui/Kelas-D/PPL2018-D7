/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.ui.register;

import javax.inject.Inject;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.api.ProyekinApi;
import ppld7.klilink.data.network.model.request.register.RegisterRequest;
import ppld7.klilink.data.network.model.response.register.Data;
import ppld7.klilink.data.network.model.response.register.RegisterResponse;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.ui.base.BasePresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by janisharali on 27/01/17.
 */

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V> {

    private static final String TAG = "RegisterPresenter";

    Retrofit retrofit;
    ProyekinApi api;

    private Callback<RegisterResponse> callback = new Callback<RegisterResponse>() {
        @Override
        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
            if (response.isSuccessful()) {
                if (response.body().getIsSuccess()) {
                    RegisterResponse registerResponse = response.body();
                    getMvpView().onRegisterSuccess(registerResponse.getData());
                    getDataManager().setTrackingStatus(true);
                } else {
                    getMvpView().onRegisterFailed(response.body().getErr());
                }
            } else {
                getMvpView().onRegisterFailed("Register Failed");
            }
        }

        @Override
        public void onFailure(Call<RegisterResponse> call, Throwable t) {
            getMvpView().onRegisterFailed(t.getMessage() + "\n" + t.getLocalizedMessage());
        }
    };

    @Inject
    public RegisterPresenter(DataManager dataManager) {
        super(dataManager);
        retrofit = dataManager.getRetrofit();
        api = getApi();
    }

    private ProyekinApi getApi() {
        return retrofit.create(ProyekinApi.class);
    }

    public void storeSession(Data registerData) {
        String role = registerData.getRole();
        boolean isSeller = role.equals("pedagang");
        UserDataModel model = UserDataModel.newInstance(
                registerData.getNama(),
                registerData.getNoHp(),
                isSeller,
                registerData.getFcmToken(), null, null, null
        );
        getDataManager().saveUserData(model);
        getDataManager().saveSendbirdData(registerData.getSendbirdData());
    }

    @Override
    public void register(RegisterRequest req) {
        getMvpView().showLoading();
        req.setFcmToken(getDataManager().getFcmToken());
        Call<RegisterResponse> call = api.register(req);
        call.enqueue(callback);
    }
}
