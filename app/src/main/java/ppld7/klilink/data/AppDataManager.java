/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.data;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Types;
import com.google.gson.reflect.TypeToken;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import okhttp3.OkHttpClient;
import ppld7.klilink.BuildConfig;
import ppld7.klilink.data.db.DbHelper;
import ppld7.klilink.data.db.model.Option;
import ppld7.klilink.data.db.model.Question;
import ppld7.klilink.data.db.model.User;
import ppld7.klilink.data.network.ApiHeader;
import ppld7.klilink.data.network.ApiHelper;
import ppld7.klilink.data.network.model.response.SendbirdData;
import ppld7.klilink.data.prefs.PreferencesHelper;
import ppld7.klilink.data.prefs.UserDataModel;
import ppld7.klilink.di.ApplicationContext;
import ppld7.klilink.utils.AppConstants;
import ppld7.klilink.utils.CommonUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }


    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public Observable<Long> insertUser(User user) {
        return mDbHelper.insertUser(user);
    }

    @Override
    public Observable<List<User>> getAllUsers() {
        return mDbHelper.getAllUsers();
    }

    @Override
    public UserDataModel getUserModel() {
        if (isLoggedIn()) {
            UserDataModel model = UserDataModel.newInstance(
                    getCurrentUserName(),
                    getCurrentUserPhone(),
                    isSeller(),
                    getFcmToken(),
                    getVendorName(),
                    getAccessToken(),
                    getUrlPhotoUser()
            );

            return model;
        }

        return null;
    }

    @Override
    public void updateApiHeader(String userId, String accessToken) {
        mApiHelper.getApiHeader().getProtectedApiHeader().setUserId(userId);
        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(accessToken);
    }

    @Override
    public void saveUserData(UserDataModel model) {
        mPreferencesHelper.saveUserData(model);
    }

    @Override
    public String getCurrentUserPhone() {
        return mPreferencesHelper.getCurrentUserPhone();
    }

    @Override
    public void setCurrentUserPhone(String userId) {
        mPreferencesHelper.setCurrentUserPhone(userId);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public boolean isSeller() {
        return mPreferencesHelper.isSeller();
    }

    @Override
    public void setIsSeller(boolean isSeller) {
        mPreferencesHelper.setIsSeller(isSeller);
    }

    @Override
    public String getFcmToken() {
        return mPreferencesHelper.getFcmToken();
    }

    @Override
    public void setFcmToken(String token) {
        mPreferencesHelper.setFcmToken(token);
    }

    @Override
    public boolean isLoggedIn() {
        return mPreferencesHelper.isLoggedIn();
    }

    @Override
    public void clearPreference() {

    }


    @Override
    public String getVendorName() {
        return mPreferencesHelper.getVendorName();
    }

    @Override
    public void setVendorName(String vendorName) {
        mPreferencesHelper.setVendorName(vendorName);
    }

    @Override
    public void setUserAsLoggedOut() {
        mPreferencesHelper.clearPreference();
    }

    @Override
    public void saveSendbirdData(SendbirdData data) {
        mPreferencesHelper.saveSendbirdData(data);
    }

    @Override
    public SendbirdData getSendbirdData() {
        return mPreferencesHelper.getSendbirdData();
    }

    @Override
    public String getUrlPhotoUser() {
        return mPreferencesHelper.getUrlPhotoUser();
    }

    @Override
    public void setUrlPhotoUser(String url) {
        mPreferencesHelper.setUrlPhotoUser(url);
    }

    @Override
    public Observable<Boolean> isQuestionEmpty() {
        return mDbHelper.isQuestionEmpty();
    }

    @Override
    public Observable<Boolean> isOptionEmpty() {
        return mDbHelper.isOptionEmpty();
    }

    @Override
    public Observable<Boolean> saveQuestion(Question question) {
        return mDbHelper.saveQuestion(question);
    }

    @Override
    public Observable<Boolean> saveOption(Option option) {
        return mDbHelper.saveOption(option);
    }

    @Override
    public Observable<Boolean> saveQuestionList(List<Question> questionList) {
        return mDbHelper.saveQuestionList(questionList);
    }

    @Override
    public Observable<Boolean> saveOptionList(List<Option> optionList) {
        return mDbHelper.saveOptionList(optionList);
    }

    @Override
    public Observable<List<Question>> getAllQuestions() {
        return mDbHelper.getAllQuestions();
    }

    @Override
    public Observable<Boolean> seedDatabaseQuestions() {

        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        return mDbHelper.isQuestionEmpty()
                .concatMap(new Function<Boolean, ObservableSource<? extends Boolean>>() {
                    @Override
                    public ObservableSource<? extends Boolean> apply(Boolean isEmpty)
                            throws Exception {
                        if (isEmpty) {
                            Type type = $Gson$Types
                                    .newParameterizedTypeWithOwner(null, List.class,
                                            Question.class);
                            List<Question> questionList = gson.fromJson(
                                    CommonUtils.loadJSONFromAsset(mContext,
                                            AppConstants.SEED_DATABASE_QUESTIONS),
                                    type);

                            return saveQuestionList(questionList);
                        }
                        return Observable.just(false);
                    }
                });
    }

    @Override
    public Observable<Boolean> seedDatabaseOptions() {

        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        return mDbHelper.isOptionEmpty()
                .concatMap(new Function<Boolean, ObservableSource<? extends Boolean>>() {
                    @Override
                    public ObservableSource<? extends Boolean> apply(Boolean isEmpty)
                            throws Exception {
                        if (isEmpty) {
                            Type type = new TypeToken<List<Option>>() {
                            }
                                    .getType();
                            List<Option> optionList = gson.fromJson(
                                    CommonUtils.loadJSONFromAsset(mContext,
                                            AppConstants.SEED_DATABASE_OPTIONS),
                                    type);

                            return saveOptionList(optionList);
                        }
                        return Observable.just(false);
                    }
                });
    }


    @Override
    public Retrofit getRetrofit() {

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new ChuckInterceptor(mContext))
                .build();

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    @Override
    public void registerSendbirdPushToken() {
        SendBird.registerPushTokenForCurrentUser(getFcmToken(), new SendBird.RegisterPushTokenWithStatusHandler() {
            @Override
            public void onRegistered(SendBird.PushTokenRegistrationStatus ptrs, SendBirdException e) {
                if (e != null) {
                    return;
                }

                Log.e(TAG, "registerSendbirdPushToken: " + getFcmToken());
                if (ptrs == SendBird.PushTokenRegistrationStatus.PENDING) {
                    // Try registering the token after a connection has been successfully established.
                }
            }
        });
    }

    @Override
    public void setTrackingStatus(boolean status) {
        mPreferencesHelper.setTrackingStatus(status);
    }

    @Override
    public boolean isTrackingEnabled() {
        return mPreferencesHelper.isTrackingEnabled();
    }

    @Override
    public void updateSendbirdCurrentUserInfo() {
        SendBird.updateCurrentUserInfo(getCurrentUserName(), getUrlPhotoUser(), new SendBird.UserInfoUpdateHandler() {
            @Override
            public void onUpdated(SendBirdException e) {
                if (e != null) {
                    // Error.
                    return;
                }
                Log.e(TAG, "updateSendbirdCurrentUserInfo: " + getCurrentUserName());
            }
        });
    }
}
