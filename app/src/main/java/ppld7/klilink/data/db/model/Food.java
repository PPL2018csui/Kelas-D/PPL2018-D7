package ppld7.klilink.data.db.model;

/**
 * Created by Ibam on 3/12/2018.
 */

public class Food {

    private String name;
    private int price;
    private float rate;

    public static Food newInstance(String name, int price, float rate){
        Food food = new Food();
        food.setName(name);
        food.setPrice(price);
        food.setRate(rate);

        return food;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
