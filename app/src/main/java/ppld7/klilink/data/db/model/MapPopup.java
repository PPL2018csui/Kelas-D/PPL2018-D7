package ppld7.klilink.data.db.model;

/**
 * Created by Ibam on 3/21/2018.
 */

public class MapPopup {
    private String name;
    private String phoneNumber;
    private String storename;
    private String urlPhoto;

    public static MapPopup getInstance(String name, String storename, String phoneNumber, String urlPhoto) {
        MapPopup temp = new MapPopup();
        temp.setName(name);
        temp.setPhoneNumber(phoneNumber);
        temp.setStorename(storename);
        temp.setUrlPhoto(urlPhoto);
        return temp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
}
