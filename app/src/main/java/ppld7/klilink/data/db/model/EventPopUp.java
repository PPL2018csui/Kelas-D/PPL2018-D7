package ppld7.klilink.data.db.model;

/**
 * Created by Fadhil on 12-Apr-18.
 */

public class EventPopUp {
    private String name;
    private String waktuMulai;
    private String waktuSelesai;

    public static EventPopUp create(String name, String waktuMulai, String waktuSelesai) {
        EventPopUp temp = new EventPopUp();
        temp.setName(name);
        temp.setWaktuMulai(waktuMulai);
        temp.setWaktuSelesai(waktuSelesai);
        return temp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWaktuMulai() {
        return waktuMulai;
    }

    public void setWaktuMulai(String waktuMulai) {
        this.waktuMulai = waktuMulai;
    }

    public String getWaktuSelesai() {
        return waktuSelesai;
    }

    public void setWaktuSelesai(String waktuSelesai) {
        this.waktuSelesai = waktuSelesai;
    }
}
