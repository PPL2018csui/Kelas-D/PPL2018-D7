package ppld7.klilink.data.network.model.request.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 17/04/2018.
 */

public class EditProfileRequest {

    @SerializedName("no_hp")
    @Expose
    private String no_hp;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("url")
    @Expose
    private String url;

    public static EditProfileRequest newInstance(String no_hp, String nama, String password, String url) {
        EditProfileRequest model = new EditProfileRequest();
        model.setNama(nama);
        model.setPassword(password);
        model.setNo_hp(no_hp);
        model.setUrl(url);

        return model;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
