package ppld7.klilink.data.network.model.request.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ibam on 4/11/2018.
 */

public class LoginRequest {

    @SerializedName("no_hp")
    @Expose
    private String phoneNumber;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;

    public static LoginRequest newInstance(String phoneNumber, String password) {
        LoginRequest model = new LoginRequest();
        model.setPhoneNumber(phoneNumber);
        model.setPassword(password);
        return model;
    }

    public static LoginRequest newInstance(String phoneNumber, String password, String token) {
        LoginRequest model = newInstance(phoneNumber, password);
        model.setFcmToken(token);

        return model;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
