package ppld7.klilink.data.network.model.request.register;

import com.google.gson.annotations.SerializedName;

public class RegisterRequest {
    @SerializedName("no_hp")
    private String no_hp;
    @SerializedName("nama")
    private String nama;
    @SerializedName("role")
    private String role;
    @SerializedName("password")
    private String password;
    @SerializedName("fcm_token")
    private String fcmToken;

    public static RegisterRequest newInstance(String no_hp, String nama, String role, String password) {
        RegisterRequest temp = new RegisterRequest();
        temp.setNo_hp(no_hp);
        temp.setNama(nama);
        temp.setRole(role);
        temp.setPassword(password);

        return temp;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
