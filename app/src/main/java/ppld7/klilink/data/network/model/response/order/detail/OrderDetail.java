package ppld7.klilink.data.network.model.response.order.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetail {
    @SerializedName("harga")
    @Expose
    private Integer harga;
    @SerializedName("id_details")
    @Expose
    private Integer idDetails;
    @SerializedName("id_order")
    @Expose
    private Integer idOrder;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("url_foto")
    @Expose
    private String urlFoto;

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(Integer idDetails) {
        this.idDetails = idDetails;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}
