package ppld7.klilink.data.network.model.request.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderRequest {

    @SerializedName("pembeli")
    @Expose
    private String pembeli;
    @SerializedName("penjual")
    @Expose
    private String penjual;
    @SerializedName("order_list")
    @Expose
    private List<OrderList> orderList = null;
    @SerializedName("harga")
    @Expose
    private Integer harga;

    public String getPembeli() {
        return pembeli;
    }

    public void setPembeli(String pembeli) {
        this.pembeli = pembeli;
    }

    public String getPenjual() {
        return penjual;
    }

    public void setPenjual(String penjual) {
        this.penjual = penjual;
    }

    public List<OrderList> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderList> orderList) {
        this.orderList = orderList;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

}
