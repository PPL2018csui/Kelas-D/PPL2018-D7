package ppld7.klilink.data.network.model.request.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPasswordRequest {
    @SerializedName("no_hp")
    @Expose
    private String phoneNumber;

    @SerializedName("password")
    @Expose
    private String newPassword;

    @SerializedName("reset_code")
    @Expose
    private String reset_code;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getReset_code() {
        return reset_code;
    }

    public void setReset_code(String reset_code) {
        this.reset_code = reset_code;
    }

    public static ResetPasswordRequest newInstance(String no_hp, String newPassword, String reset_code) {
        ResetPasswordRequest model = new ResetPasswordRequest();
        model.setPhoneNumber(no_hp);
        model.setNewPassword(newPassword);
        model.setReset_code(reset_code);
        return model;
    }


}
