package ppld7.klilink.data.network.model.response.order.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderListResponse {

    @SerializedName("data")
    @Expose
    private List<OrderListResponseData> data = null;
    @SerializedName("err")
    @Expose
    private String err;
    @SerializedName("is_seller")
    @Expose
    private Boolean isSeller;
    @SerializedName("is_success")
    @Expose
    private Boolean isSuccess;

    public List<OrderListResponseData> getData() {
        return data;
    }

    public void setData(List<OrderListResponseData> data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public Boolean getIsSeller() {
        return isSeller;
    }

    public void setIsSeller(Boolean isSeller) {
        this.isSeller = isSeller;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

}
