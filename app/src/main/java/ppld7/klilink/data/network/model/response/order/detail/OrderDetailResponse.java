package ppld7.klilink.data.network.model.response.order.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetailResponse {
    @SerializedName("data")
    @Expose
    private OrderDetailResponseData data;
    @SerializedName("err")
    @Expose
    private String err;
    @SerializedName("is_success")
    @Expose
    private Boolean isSuccess;

    public OrderDetailResponseData getData() {
        return data;
    }

    public void setData(OrderDetailResponseData data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

}
