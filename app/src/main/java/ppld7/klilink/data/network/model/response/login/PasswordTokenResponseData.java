package ppld7.klilink.data.network.model.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PasswordTokenResponseData {
    @SerializedName("nama")
    @Expose
    private String nama;

    public PasswordTokenResponseData(String nama) {
        this.nama = nama;
    }

    public String getNama() {

        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
