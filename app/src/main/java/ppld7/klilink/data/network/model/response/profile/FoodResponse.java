package ppld7.klilink.data.network.model.response.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FoodResponse implements Serializable {
    @SerializedName("id_menu")
    @Expose
    private Integer idMenu;
    @SerializedName("is_ready")
    @Expose
    private Boolean isReady;
    @SerializedName("nama_menu")
    @Expose
    private String namaMenu;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("url_foto_menu")
    @Expose
    private String urlFotoMenu;

    @SerializedName("harga")
    @Expose
    private String harga;

    public Boolean getReady() {
        return isReady;
    }

    public void setReady(Boolean ready) {
        isReady = ready;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public Integer getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public Boolean getIsReady() {
        return isReady;
    }

    public void setIsReady(Boolean isReady) {
        this.isReady = isReady;
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getUrlFotoMenu() {
        return urlFotoMenu;
    }

    public void setUrlFotoMenu(String urlFotoMenu) {
        this.urlFotoMenu = urlFotoMenu;
    }

}
