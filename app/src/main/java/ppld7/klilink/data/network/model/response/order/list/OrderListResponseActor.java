package ppld7.klilink.data.network.model.response.order.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderListResponseActor {
    @SerializedName("hp")
    @Expose
    private String hp;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("type")
    @Expose
    private String type;

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
