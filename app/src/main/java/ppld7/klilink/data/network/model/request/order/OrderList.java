package ppld7.klilink.data.network.model.request.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderList {

    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("amount")
    @Expose
    private String amount;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}