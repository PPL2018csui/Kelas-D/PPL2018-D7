package ppld7.klilink.data.network.model.response.event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by farhan on 12/05/2018.
 */

public class EventResponseData{

    @SerializedName("no_hp")
    @Expose
    private String noHp;

    @SerializedName("id_event")
    @Expose
    private String idEvent;

    @SerializedName("nama_event")
    @Expose
    private String namaEvent;

    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;

    @SerializedName("waktu_mulai")
    @Expose
    private String waktuMulai;

    @SerializedName("waktu_selesai")
    @Expose
    private String waktuSelesai;

    @SerializedName("location")
    @Expose
    private String location;

    public String getNoHp(){
        return noHp;
    }

    public void setNoHp(String noHp){
        this.noHp=noHp;
    }

    public String getIdEvent(){
        return idEvent;
    }

    public void setIdEvent(String idEvent){
        this.idEvent=idEvent;
    }

    public String getNamaEvent(){
        return namaEvent;
    }

    public void setNamaEvent(String namaEvent){
        this.namaEvent=namaEvent;
    }

    public String getDeskripsi(){
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi){
        this.deskripsi=deskripsi;
    }

    public String getWaktuMulai(){
        return waktuMulai;
    }

    public void setWaktuMulai(String waktuMulai){
        this.waktuMulai=waktuMulai;
    }

    public String getWaktuSelesai(){
        return waktuSelesai;
    }

    public void setWaktuSelesai(String waktuSelesai){
        this.waktuSelesai=waktuSelesai;
    }

    public String getLocation(){
        return location;
    }

    public void setLocation(String location){
        this.location=location;
    }
}
