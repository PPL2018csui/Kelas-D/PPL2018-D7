package ppld7.klilink.data.network.model.response.order.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetailResponseData {
    @SerializedName("order_details")
    @Expose
    private List<OrderDetail> orderDetails = null;
    @SerializedName("order_overview")
    @Expose
    private OrderDetailResponseOverview orderOverview;

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public OrderDetailResponseOverview getOrderOverview() {
        return orderOverview;
    }

    public void setOrderOverview(OrderDetailResponseOverview orderOverview) {
        this.orderOverview = orderOverview;
    }
}
