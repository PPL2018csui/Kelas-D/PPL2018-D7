package ppld7.klilink.data.network.model.response.event;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 10/05/2018.
 */

public class EventResponse {

    @SerializedName("data")
    @Expose
    private EventResponseData data;

    @SerializedName("err")
    @Expose
    private String err;

    @SerializedName("is_success")
    @Expose
    private Boolean isSuccess;

    public EventResponseData getData(){
        return data;
    }

    public void setData(EventResponseData data){
        this.data=data;
    }

    public String getErr(){
        return err;
    }

    public void setErr(String err){
        this.err=err;
    }

    public Boolean getIsSuccess(){
        return isSuccess;
    }

    public void setIsSuccess(Boolean success){
        isSuccess=success;
    }
}
