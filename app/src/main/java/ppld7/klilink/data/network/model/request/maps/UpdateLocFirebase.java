package ppld7.klilink.data.network.model.request.maps;

import android.location.Location;

import ppld7.klilink.data.prefs.UserDataModel;

public class UpdateLocFirebase {

    private String nama;
    private String noHp;
    private String location;
    private String waktuBuka;
    private String waktuTutup;
    private String namaDagangan;
    private String urlProfile;

    public static UpdateLocFirebase newInstance(Location location, UserDataModel model) {
        UpdateLocFirebase newLocation = new UpdateLocFirebase();

        if (location != null && model != null) {
            newLocation.setNama(model.getName());
            newLocation.setNoHp(model.getNomorTelepon());
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            newLocation.setLocation(latitude + ", " + longitude);
            newLocation.setWaktuBuka("00:00");
            newLocation.setWaktuTutup("23:55");
            newLocation.setNamaDagangan(model.getNamaToko());
            newLocation.setUrlProfile(model.getUrlPhoto());
        }

        return newLocation;
    }

    public String getWaktuBuka() {
        return waktuBuka;
    }

    public void setWaktuBuka(String waktuBuka) {
        this.waktuBuka = waktuBuka;
    }

    public String getWaktuTutup() {
        return waktuTutup;
    }

    public void setWaktuTutup(String waktuTutup) {
        this.waktuTutup = waktuTutup;
    }

    public String getNamaDagangan() {
        return namaDagangan;
    }

    public void setNamaDagangan(String namaDagangan) {
        this.namaDagangan = namaDagangan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return nama + " " + noHp + " " + location;
    }

    public String getUrlProfile() {
        return urlProfile;
    }

    public void setUrlProfile(String urlProfile) {
        this.urlProfile = urlProfile;
    }
}