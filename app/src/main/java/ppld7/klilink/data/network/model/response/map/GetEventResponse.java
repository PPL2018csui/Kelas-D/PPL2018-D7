package ppld7.klilink.data.network.model.response.map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 10-Apr-18.
 */

public class GetEventResponse {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("id_event")
    @Expose
    private String idEvent;

    @SerializedName("nama_event")
    @Expose
    private String namaEvent;

    @SerializedName("waktu_mulai")
    @Expose
    private String waktuMulai;

    @SerializedName("waktu_selesai")
    @Expose
    private String waktuSelesai;

    @SerializedName("location")
    @Expose
    private String location;

    public GetEventResponse(
            String id,
            String idEvent,
            String namaEvent,
            String waktuMulai,
            String waktuSelesai,
            String location
    ) {
        this.id = id;
        this.idEvent = idEvent;
        this.namaEvent = namaEvent;
        this.waktuMulai = waktuMulai;
        this.waktuSelesai = waktuSelesai;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    public String getNamaEvent() {
        return namaEvent;
    }

    public void setNamaEvent(String namaEvent) {
        this.namaEvent = namaEvent;
    }

    public String getWaktuMulai() {
        return waktuMulai;
    }

    public void setWaktuMulai(String waktuMulai) {
        this.waktuMulai = waktuMulai;
    }

    public String getWaktuSelesai() {
        return waktuSelesai;
    }

    public void setWaktuSelesai(String waktuSelesai) {
        this.waktuSelesai = waktuSelesai;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
