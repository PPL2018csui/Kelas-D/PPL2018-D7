package ppld7.klilink.data.network.model.response.order.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderListResponseData {

    @SerializedName("actors")
    @Expose
    private OrderListResponseActor OrderListResponseActor;
    @SerializedName("harga_total")
    @Expose
    private Integer hargaTotal;
    @SerializedName("id_order")
    @Expose
    private Integer idOrder;
    @SerializedName("status")
    @Expose
    private String status;

    public OrderListResponseActor getOrderListResponseActor() {
        return OrderListResponseActor;
    }

    public void setOrderListResponseActor(OrderListResponseActor OrderListResponseActor) {
        this.OrderListResponseActor = OrderListResponseActor;
    }

    public Integer getHargaTotal() {
        return hargaTotal;
    }

    public void setHargaTotal(Integer hargaTotal) {
        this.hargaTotal = hargaTotal;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
