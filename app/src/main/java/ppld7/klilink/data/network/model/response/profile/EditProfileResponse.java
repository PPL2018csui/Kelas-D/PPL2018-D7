package ppld7.klilink.data.network.model.response.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 17/04/2018.
 */

public class EditProfileResponse {

    @SerializedName("data")
    @Expose
    private EditProfileResponseData data;
    @SerializedName("err")
    @Expose
    private String err;
    @SerializedName("is_success")
    @Expose
    private Boolean isSuccess;

    public EditProfileResponseData getData() {
        return data;
    }

    public void setData(EditProfileResponseData data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
}
