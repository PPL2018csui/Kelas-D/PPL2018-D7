package ppld7.klilink.data.network.model.request.maps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ppld7.klilink.data.network.model.request.login.LoginRequest;

/**
 * Created by Fadhil on 27-Apr-18.
 */

public class UpdateLocationRequest {
    @SerializedName("no_hp")
    @Expose
    private String phoneNumber;
    @SerializedName("new_location")
    @Expose
    private String newLocation;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNewLocation() {
        return newLocation;
    }

    public void setNewLocation(String newLocation) {
        this.newLocation = newLocation;
    }

    public static UpdateLocationRequest newInstance(String phoneNumber, String newLocation) {
        UpdateLocationRequest model = new UpdateLocationRequest();
        model.setPhoneNumber(phoneNumber);
        model.setNewLocation(newLocation);

        return model;
    }
}
