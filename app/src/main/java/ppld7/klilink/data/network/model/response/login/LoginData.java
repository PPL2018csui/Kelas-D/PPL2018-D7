package ppld7.klilink.data.network.model.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ppld7.klilink.data.network.model.response.SendbirdData;

/**
 * Created by Fadhil on 24-Apr-18.
 */

public class LoginData {
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("url_foto")
    @Expose
    private String urlFoto;
    @SerializedName("waktu_registrasi")
    @Expose
    private String waktuRegistrasi;
    @SerializedName("last_modified")
    @Expose
    private String lastModified;
    @SerializedName("is_online")
    @Expose
    private Boolean isOnline;
    @SerializedName("is_verified")
    @Expose
    private Boolean isVerified;
    @SerializedName("location")
    @Expose
    private Object location;
    @SerializedName("fcm_token")
    @Expose
    private String fcmToken;
    @SerializedName("sendbird")
    @Expose
    private SendbirdData sendbirdData;

    public SendbirdData getSendbirdData() {
        return sendbirdData;
    }

    public void setSendbirdData(SendbirdData sendbirdData) {
        this.sendbirdData = sendbirdData;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getWaktuRegistrasi() {
        return waktuRegistrasi;
    }

    public void setWaktuRegistrasi(String waktuRegistrasi) {
        this.waktuRegistrasi = waktuRegistrasi;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }
}
