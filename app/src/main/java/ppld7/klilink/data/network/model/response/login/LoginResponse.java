package ppld7.klilink.data.network.model.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ibam on 3/7/2018.
 */

public class LoginResponse {
    @SerializedName("is_success")
    @Expose
    private Boolean isSuccess;
    @SerializedName("data")
    @Expose
    private LoginData data;
    @SerializedName("err")
    @Expose
    private String err;

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }
}
