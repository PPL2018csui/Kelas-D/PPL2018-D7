package ppld7.klilink.data.network.model.response.map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ibam on 3/28/2018.
 */

public class GetPedagangResponse {
    @SerializedName("is_mangkal")
    @Expose
    private Boolean isMangkal;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("nama_dagangan")
    @Expose
    private String namaDagangan;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("url_foto")
    @Expose
    private String urlFoto;
    @SerializedName("waktu_buka")
    @Expose
    private String waktuBuka;
    @SerializedName("waktu_tutup")
    @Expose
    private String waktuTutup;

    public GetPedagangResponse(String noHp, String namaDagangan, String waktuBuka, String waktuTutup, String location) {
        this.noHp = noHp;
        this.namaDagangan = namaDagangan;
        this.waktuBuka = waktuBuka;
        this.waktuTutup = waktuTutup;
        this.location = location;
    }

    public Boolean getMangkal() {
        return isMangkal;
    }

    public void setMangkal(Boolean mangkal) {
        isMangkal = mangkal;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNamaDagangan() {
        return namaDagangan;
    }

    public void setNamaDagangan(String namaDagangan) {
        this.namaDagangan = namaDagangan;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getWaktuBuka() {
        return waktuBuka;
    }

    public void setWaktuBuka(String waktuBuka) {
        this.waktuBuka = waktuBuka;
    }

    public String getWaktuTutup() {
        return waktuTutup;
    }

    public void setWaktuTutup(String waktuTutup) {
        this.waktuTutup = waktuTutup;
    }
}