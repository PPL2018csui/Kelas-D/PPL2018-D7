package ppld7.klilink.data.network.model.request.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyRequest {

    @SerializedName("no_hp")
    @Expose
    private String noHP;
    @SerializedName("verification_code")
    @Expose
    private String uniqueCode;

    public VerifyRequest(String noHP, String uniqueCode) {
        this.noHP = noHP;
        this.uniqueCode = uniqueCode;
    }

    public String getNoHP() {
        return noHP;
    }

    public void setNoHP(String noHP) {
        this.noHP = noHP;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }
}
