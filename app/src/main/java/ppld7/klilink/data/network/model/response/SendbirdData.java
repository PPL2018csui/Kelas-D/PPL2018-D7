package ppld7.klilink.data.network.model.response;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SendbirdData {

    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("discovery_keys")
    @Expose
    private List<Object> discoveryKeys = null;
    @SerializedName("has_ever_logged_in")
    @Expose
    private Boolean hasEverLoggedIn;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("is_online")
    @Expose
    private Boolean isOnline;
    @SerializedName("is_shadow_blocked")
    @Expose
    private Boolean isShadowBlocked;
    @SerializedName("last_seen_at")
    @Expose
    private long lastSeenAt;
    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("profile_url")
    @Expose
    private String profileUrl;
    @SerializedName("session_tokens")
    @Expose
    private List<Object> sessionTokens = null;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public List<Object> getDiscoveryKeys() {
        return discoveryKeys;
    }

    public void setDiscoveryKeys(List<Object> discoveryKeys) {
        this.discoveryKeys = discoveryKeys;
    }

    public Boolean getHasEverLoggedIn() {
        return hasEverLoggedIn;
    }

    public void setHasEverLoggedIn(Boolean hasEverLoggedIn) {
        this.hasEverLoggedIn = hasEverLoggedIn;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    public Boolean getIsShadowBlocked() {
        return isShadowBlocked;
    }

    public void setIsShadowBlocked(Boolean isShadowBlocked) {
        this.isShadowBlocked = isShadowBlocked;
    }

    public long getLastSeenAt() {
        return lastSeenAt;
    }

    public void setLastSeenAt(long lastSeenAt) {
        this.lastSeenAt = lastSeenAt;
    }


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public List<Object> getSessionTokens() {
        return sessionTokens;
    }

    public void setSessionTokens(List<Object> sessionTokens) {
        this.sessionTokens = sessionTokens;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String toString(){
        Log.e(TAG, "toString: " + nickname + userId);

        return null;
    }

    private static final String TAG = "SendbirdData";
}
