package ppld7.klilink.data.network.model.response.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 25-May-18.
 */
public class PasswordTokenResponse
{
    @SerializedName("is_success")
    @Expose
    private Boolean is_success;
    @SerializedName("err")
    @Expose
    private String err;
    @SerializedName("data")
    @Expose
    private PasswordTokenResponseData data = null;


    public Boolean getIs_success() {
        return is_success;
    }

    public void setIs_success(Boolean is_success) {
        this.is_success = is_success;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public PasswordTokenResponseData getData() {
        return data;
    }

    public void setData(PasswordTokenResponseData data) {
        this.data = data;
    }
}
