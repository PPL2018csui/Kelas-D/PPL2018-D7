package ppld7.klilink.data.network.model.request.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dwi Nanda on 27/05/2018.
 */

public class DeleteMenuRequest {
    @SerializedName("id_menu")
    @Expose
    private int id_menu;
    @SerializedName("no_hp")
    @Expose
    private String no_hp;


    public int getId_menu() {
        return id_menu;
    }

    public void setId_menu(int id_menu) {
        this.id_menu = id_menu;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public static DeleteMenuRequest newInstance(int id_menu, String no_hp) {
        DeleteMenuRequest model = new DeleteMenuRequest();
        model.setId_menu(id_menu);
        model.setNo_hp(no_hp);


        return model;
    }
}
