package ppld7.klilink.data.network.model.response.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dwi Nanda on 16/05/2018.
 */

public class DeleteMenuResponse {
    @SerializedName("is_success")
    @Expose
    private Boolean is_success;
    @SerializedName("err")
    @Expose
    private String err;


    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public Boolean getIsSuccess() {
        return is_success;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.is_success = isSuccess;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [is_success = "+is_success+", err = "+err+"]";
    }
}
