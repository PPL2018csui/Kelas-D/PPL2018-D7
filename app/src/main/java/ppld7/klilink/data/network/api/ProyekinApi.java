package ppld7.klilink.data.network.api;

import java.util.List;

import ppld7.klilink.data.network.model.request.event.EventRequest;
import ppld7.klilink.data.network.model.request.login.LoginRequest;
import ppld7.klilink.data.network.model.request.login.ResetPasswordRequest;
import ppld7.klilink.data.network.model.request.maps.UpdateLocationRequest;
import ppld7.klilink.data.network.model.request.order.OrderRequest;
import ppld7.klilink.data.network.model.request.profile.AddMenuRequest;
import ppld7.klilink.data.network.model.request.profile.DeleteMenuRequest;
import ppld7.klilink.data.network.model.request.profile.EditMenuRequest;
import ppld7.klilink.data.network.model.request.profile.EditProfileRequest;
import ppld7.klilink.data.network.model.request.register.RegisterRequest;
import ppld7.klilink.data.network.model.request.register.VerifyRequest;
import ppld7.klilink.data.network.model.response.GenericResponse;
import ppld7.klilink.data.network.model.response.event.EventResponse;
import ppld7.klilink.data.network.model.response.login.LoginResponse;
import ppld7.klilink.data.network.model.response.login.PasswordTokenResponse;
import ppld7.klilink.data.network.model.response.login.ResetPasswordResponse;
import ppld7.klilink.data.network.model.response.map.GetEventResponse;
import ppld7.klilink.data.network.model.response.map.GetPedagangResponse;
import ppld7.klilink.data.network.model.response.menu.GetMenuResponse;
import ppld7.klilink.data.network.model.response.order.OrderResponse;
import ppld7.klilink.data.network.model.response.order.detail.OrderDetailResponse;
import ppld7.klilink.data.network.model.response.order.list.OrderListResponse;
import ppld7.klilink.data.network.model.response.profile.AddMenuResponse;
import ppld7.klilink.data.network.model.response.profile.DeleteMenuResponse;
import ppld7.klilink.data.network.model.response.profile.EditMenuResponse;
import ppld7.klilink.data.network.model.response.profile.EditProfileResponse;
import ppld7.klilink.data.network.model.response.profile.FoodResponse;
import ppld7.klilink.data.network.model.response.register.RegisterResponse;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ibam on 2/10/2018.
 */

public interface ProyekinApi {

    @GET("user/pedagangs/")
    Call<List<GetPedagangResponse>> getAllPedagang();

    @POST("auth/login/")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("auth/register/")
    Call<RegisterResponse> register(@Body RegisterRequest request);

    @GET("user/menu/?no_hp={no_hp}")
    Call<List<GetMenuResponse>> getMenu(@Path("no_hp") String no_hp);

    @POST("user/profile/")
    Call<EditProfileResponse> editProfile(@Body EditProfileRequest request);

    @GET("maps/events/")
    Call<List<GetEventResponse>> getAllEvent();

    @POST("maps/events/")
    Call<EventResponse> addEvent(@Body EventRequest request);

    @POST("maps/location/")
    Call<Response<String>> updateLocation(@Body UpdateLocationRequest location);

    @GET("user/menu/")
    Call<List<FoodResponse>> getFoodList(@Query("no_hp") String noHp);

    @POST("/user/menu/add/")
    Call<AddMenuResponse> addMenu(@Body AddMenuRequest request);

    @POST("/user/menu/edit/")
    Call<EditMenuResponse> editMenu(@Body EditMenuRequest request);

    @POST("/user/menu/delete/")
    Call<DeleteMenuResponse> deleteMenu(@Body DeleteMenuRequest request);

    @POST("order/")
    Call<OrderResponse> makeOrder(@Body OrderRequest orderRequest);

    @GET("user/order/")
    Call<OrderListResponse> getOrderList(@Query("phone") String phoneNumber, @Query("seller") String isSeller);

    @GET("order/{id_order}")
    Call<OrderDetailResponse> getOrderDetail(@Path("id_order") int orderId);

    @POST("order/{id}")
    Call<GenericResponse> executeOrderAction(@Path("id") int orderId, @Query("action") String action);

    @POST("auth/verify/")
    Call<GenericResponse> verifyNoHp(@Body VerifyRequest request);

    @GET("/auth/password/")
    Call<PasswordTokenResponse> getToken(@Query("no_hp") String noHp);

    @POST("/auth/password/")
    Call<ResetPasswordResponse> resetPassword(@Body ResetPasswordRequest request);

    @GET("auth/verify")
    Call<GenericResponse> getVerifyToken(@Query("no_hp") String phoneNumber);
}
