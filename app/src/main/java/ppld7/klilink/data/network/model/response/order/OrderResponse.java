package ppld7.klilink.data.network.model.response.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 23-May-18.
 */

public class OrderResponse {

    @SerializedName("data")
    @Expose
    private OrderResponseData data;
    @SerializedName("err")
    @Expose
    private String err;
    @SerializedName("is_success")
    @Expose
    private Boolean isSuccess;

    public OrderResponseData getData() {
        return data;
    }

    public void setData(OrderResponseData data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

}
