package ppld7.klilink.data.network.model.response.menu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dwi Nanda on 26/04/2018.
 */

public class GetMenuResponse {
    @SerializedName("id_menu")
    @Expose
    private String id_menu;

    @SerializedName("is_ready")
    @Expose
    private boolean is_ready;

    @SerializedName("nama_menu")
    @Expose
    private String nama_menu;

    @SerializedName("no_hp")
    @Expose
    private String no_hp;

    @SerializedName("url_foto_menu")
    @Expose
    private String url_foto_menu;


    public GetMenuResponse(
            String id_menu,
            boolean is_ready,
            String nama_menu,
            String no_hp,
            String url_foto_menu) {
        this.id_menu = id_menu;
        this.is_ready = is_ready;
        this.nama_menu = nama_menu;
        this.no_hp = no_hp;
        this.url_foto_menu = url_foto_menu;
    }


    public String getId_menu() {
        return id_menu;
    }

    public void setId_menu(String id_menu) {
        this.id_menu = id_menu;
    }

    public boolean isIs_ready() {
        return is_ready;
    }

    public void setIs_ready(boolean is_ready) {
        this.is_ready = is_ready;
    }

    public String getNama_menu() {
        return nama_menu;
    }

    public void setNama_menu(String nama_menu) {
        this.nama_menu = nama_menu;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getUrl_foto_menu() {
        return url_foto_menu;
    }

    public void setUrl_foto_menu(String url_foto_menu) {
        this.url_foto_menu = url_foto_menu;
    }
}
