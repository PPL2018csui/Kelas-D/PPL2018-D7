package ppld7.klilink.data.network.model.request.profile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dwi Nanda on 16/05/2018.
 */

public class AddMenuRequest {
    @SerializedName("no_hp")
    @Expose
    private String no_hp;
    @SerializedName("nama_menu")
    @Expose
    private String nama_menu;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("is_ready")
    @Expose
    private boolean is_ready;


    public static AddMenuRequest newInstance(String no_hp, String nama_menu, String url, String harga, boolean isReady) {
        AddMenuRequest model = new AddMenuRequest();
        model.setNo_hp(no_hp);
        model.setNama_menu(nama_menu);
        model.setUrl(url);
        model.setHarga(harga);
        model.setIs_ready(isReady);

        return model;
    }


    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }



    public String getNama_menu() {
        return nama_menu;
    }

    public void setNama_menu(String nama_menu) {
        this.nama_menu = nama_menu;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public boolean isIs_ready() {
        return is_ready;
    }

    public void setIs_ready(boolean is_ready) {
        this.is_ready = is_ready;
    }
}
