package ppld7.klilink.data.network.model.response.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Farhan on 21/04/2018.
 */

public class EditProfileResponseData {

    @SerializedName("is_online")
    @Expose
    private Boolean isOnline;
    @SerializedName("is_verified")
    @Expose
    private Boolean isVerified;
    @SerializedName("last_modified")
    @Expose
    private String lastModified;
    @SerializedName("location")
    @Expose
    private Object location;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("url_foto")
    @Expose
    private String urlFoto;
    @SerializedName("waktu_registrasi")
    @Expose
    private String waktuRegistrasi;

    public Boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Boolean isOnline) {
        this.isOnline = isOnline;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getWaktuRegistrasi() {
        return waktuRegistrasi;
    }

    public void setWaktuRegistrasi(String waktuRegistrasi) {
        this.waktuRegistrasi = waktuRegistrasi;
    }
}
