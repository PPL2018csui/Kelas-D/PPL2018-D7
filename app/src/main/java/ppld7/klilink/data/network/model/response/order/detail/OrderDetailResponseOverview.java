package ppld7.klilink.data.network.model.response.order.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fadhil on 25-May-18.
 */

public class OrderDetailResponseOverview {
    @SerializedName("harga_total")
    @Expose
    private Integer hargaTotal;
    @SerializedName("hp_pedagang")
    @Expose
    private String hpPedagang;
    @SerializedName("hp_pembeli")
    @Expose
    private String hpPembeli;
    @SerializedName("id_order")
    @Expose
    private Integer idOrder;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getHargaTotal() {
        return hargaTotal;
    }

    public void setHargaTotal(Integer hargaTotal) {
        this.hargaTotal = hargaTotal;
    }

    public String getHpPedagang() {
        return hpPedagang;
    }

    public void setHpPedagang(String hpPedagang) {
        this.hpPedagang = hpPedagang;
    }

    public String getHpPembeli() {
        return hpPembeli;
    }

    public void setHpPembeli(String hpPembeli) {
        this.hpPembeli = hpPembeli;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
