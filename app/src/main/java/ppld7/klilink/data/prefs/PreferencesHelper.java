/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.data.prefs;

import ppld7.klilink.data.DataManager;
import ppld7.klilink.data.network.model.response.SendbirdData;

/**
 * Created by janisharali on 27/01/17.
 */

public interface PreferencesHelper {

    void saveUserData(UserDataModel model);

    String getCurrentUserPhone();

    void setCurrentUserPhone(String userId);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getAccessToken();

    void setAccessToken(String accessToken);

    boolean isSeller();

    void setIsSeller(boolean isSeller);

    String getFcmToken();

    void setFcmToken(String token);

    boolean isLoggedIn();

    void clearPreference();

    void setVendorName(String vendorName);

    String getVendorName();

    void saveSendbirdData(SendbirdData data);

    SendbirdData getSendbirdData();

    void setTrackingStatus(boolean status);

    boolean isTrackingEnabled();

    String getUrlPhotoUser();

    void setUrlPhotoUser(String url);
}
