/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

import ppld7.klilink.data.network.model.response.SendbirdData;
import ppld7.klilink.di.ApplicationContext;
import ppld7.klilink.di.PreferenceInfo;

import static android.content.ContentValues.TAG;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_CURRENT_PHONE_NUMBER = "PREF_KEY_CURRENT_PHONE_NUMBER";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_FCM_TOKEN = "PREF_KEY_FCM_TOKEN";
    private static final String PREF_KEY_VENDOR_NAME = "PREF_KEY_VENDOR_NAME";
    private static final String PREF_KEY_IS_SELLER = "PREF_KEY_IS_SELLER";
    private static final String PREF_KEY_SENDBIRD = "sendbirdisveryfuntheysaid";
    private static final String PREF_KEY_TRACKING = "wowowoasdasdasdsadadwecantrackyouyeaah";
    private static final String PREF_KEY_URL_PHOTO_USER = "PREF_KEY_CURRENT_URL_PHOTO_USER";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getCurrentUserPhone() {
        String phoneNumber = mPrefs.getString(PREF_KEY_CURRENT_PHONE_NUMBER, null);
        return phoneNumber;
    }

    @Override
    public void setCurrentUserPhone(String phoneNumber) {
        String id = phoneNumber == null ? "" : phoneNumber;
        mPrefs.edit().putString(PREF_KEY_CURRENT_PHONE_NUMBER, id).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, null);
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply();
    }


    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public void saveUserData(UserDataModel model) {
        setCurrentUserName(model.getName());
        setCurrentUserPhone(model.getNomorTelepon());
        setAccessToken(model.getAccessToken());
        setFcmToken(model.getFcmToken());
        setIsSeller(model.isSeller());
        setUrlPhotoUser(model.getUrlPhoto());
        if (model.isSeller()) {
            setVendorName(model.getNamaToko());
        }
    }

    @Override
    public boolean isSeller() {
        return mPrefs.getBoolean(PREF_KEY_IS_SELLER, false);
    }

    @Override
    public void setIsSeller(boolean isSeller) {
        mPrefs.edit().putBoolean(PREF_KEY_IS_SELLER, isSeller).apply();
    }

    @Override
    public String getFcmToken() {
        String fcmToken = mPrefs.getString(PREF_KEY_FCM_TOKEN, null);

        if (fcmToken == null) {
            fcmToken = FirebaseInstanceId.getInstance().getToken();
        }

        return fcmToken;
    }

    @Override
    public void setFcmToken(String token) {
        mPrefs.edit().putString(PREF_KEY_FCM_TOKEN, token).apply();
    }

    @Override
    public boolean isLoggedIn() {
        return getCurrentUserPhone() != null;
    }

    @Override
    public String getVendorName() {
        return mPrefs.getString(PREF_KEY_VENDOR_NAME, null);
    }

    @Override
    public void setVendorName(String vendorName) {
        mPrefs.edit().putString(PREF_KEY_VENDOR_NAME, vendorName).apply();
    }

    @Override
    public void clearPreference() {
        Log.e(TAG, "clearPreference: current " + getCurrentUserPhone());
        mPrefs.edit().clear().apply();
        Log.e(TAG, "clearPreference: after log out" + getCurrentUserPhone());
    }

    @Override
    public void saveSendbirdData(SendbirdData data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        mPrefs.edit().putString(PREF_KEY_SENDBIRD, json).apply();
    }

    @Override
    public SendbirdData getSendbirdData() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_KEY_SENDBIRD, "");
        return gson.fromJson(json, SendbirdData.class);
    }

    @Override
    public void setTrackingStatus(boolean status) {
        mPrefs.edit().putBoolean(PREF_KEY_TRACKING, status).apply();
    }

    @Override
    public boolean isTrackingEnabled() {
        return mPrefs.getBoolean(PREF_KEY_TRACKING, false);
    }

    public String getUrlPhotoUser() {
        String url = mPrefs.getString(PREF_KEY_URL_PHOTO_USER, null);
        return url;
    }

    @Override
    public void setUrlPhotoUser(String url) {
        mPrefs.edit().putString(PREF_KEY_URL_PHOTO_USER, url).apply();
    }
}
