package ppld7.klilink.data.prefs;

import android.support.annotation.Nullable;

/**
 * Created by Ibrahim on 21 April 2018
 */
public class UserDataModel {

    private String name;
    private String nomorTelepon;
    private boolean isSeller;
    private String fcmToken;
    private String namaToko;
    private String accessToken;
    private String urlPhoto;


    public static UserDataModel newInstance(String nama, String nomor, boolean isSeller, String fcm, @Nullable String namaToko, @Nullable  String accessToken, @Nullable String urlPhoto) {
        UserDataModel user = new UserDataModel();
        user.setName(nama);
        user.setNomorTelepon(nomor);
        user.setSeller(isSeller);
        user.setFcmToken(fcm);
        user.setAccessToken(accessToken);
        user.setUrlPhoto(urlPhoto);

        if (isSeller) {
            user.setNamaToko(namaToko);
        } else {
            user.setNamaToko(null);
        }

        return user;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNomorTelepon() {
        return nomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        this.nomorTelepon = nomorTelepon;
    }

    public boolean isSeller() {
        return isSeller;
    }

    public void setSeller(boolean seller) {
        isSeller = seller;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getNamaToko() { return namaToko; }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }


}


