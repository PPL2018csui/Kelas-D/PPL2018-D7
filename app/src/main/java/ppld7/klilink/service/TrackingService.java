package ppld7.klilink.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

import ppld7.klilink.data.network.model.request.maps.UpdateLocFirebase;
import ppld7.klilink.data.prefs.UserDataModel;

public class TrackingService extends Service {
    private static final String TAG = "TrackingService";
    private static final String KEY_NAMA = "nama";
    private static final String KEY_HP = "hp";
    private static final String KEY_TOKO = "toko";
    private static final String KEY_IS_SELLER = "isseler";
    private static final String KEY_FCM = "fcmtoken";

    Location location;
    TimerTask mThread;
    Timer timer;
    LocationCallback locationCallback;
    FusedLocationProviderClient client;

    public static Intent getStartIntent(Context context, UserDataModel userDataModel) {
        Intent intent = new Intent(context, TrackingService.class);
        intent.putExtra(KEY_NAMA, userDataModel.getName());
        intent.putExtra(KEY_TOKO, userDataModel.getNamaToko());
        intent.putExtra(KEY_HP, userDataModel.getNomorTelepon());
        intent.putExtra(KEY_IS_SELLER, userDataModel.isSeller());
        intent.putExtra(KEY_FCM, userDataModel.getFcmToken());

        return intent;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final UserDataModel model = getUserDataModel(intent);
        mThread = new TimerTask() {
            public void run() {
                getLastLocation(model);
            }
        };
        timer = new Timer();
        timer.schedule(mThread, 0, 7000);
        return START_STICKY;
    }

    private UserDataModel getUserDataModel(Intent intent) {
        String nama = intent.getStringExtra(KEY_NAMA);
        String noHP = intent.getStringExtra(KEY_HP);
        String namaToko = intent.getStringExtra(KEY_TOKO);
        boolean isSeller = intent.getBooleanExtra(KEY_IS_SELLER, false);
        String fcmToken = intent.getStringExtra(KEY_FCM);

        UserDataModel userDataModel = new UserDataModel();
        userDataModel.setName(nama);
        userDataModel.setNomorTelepon(noHP);
        userDataModel.setNamaToko(namaToko);
        userDataModel.setSeller(isSeller);
        userDataModel.setFcmToken(fcmToken);

        return userDataModel;
    }

    private void getLastLocation(final UserDataModel model) {
        Log.e(TAG, "getLastLocation: true");
        LocationRequest request = LocationRequest.create();
        request.setInterval(7000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        client = LocationServices.getFusedLocationProviderClient(this);
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (timer != null) {
                    location = locationResult.getLastLocation();
                    UpdateLocFirebase request = UpdateLocFirebase.newInstance(location, model);
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("location");
                    databaseReference.child(model.getNomorTelepon()).setValue(request);
                    Log.e(TAG, "onLocationResult: " + request.toString() + " timer is " + timer);
                }
            }
        };

        if (permission == PackageManager.PERMISSION_GRANTED) {
            client.requestLocationUpdates(request, locationCallback, Looper.getMainLooper());
        }

    }

    @Override
    public void onDestroy() {
        // mThread.interrupt();
        if (client != null) {
            client.removeLocationUpdates(locationCallback);
            client = null;
            locationCallback = null;
        }
        mThread.cancel();
        timer.cancel();
        timer.purge();
        timer = null;
        super.onDestroy();
        Log.e(TAG, "onDestroy: service is destroyed");
    }
}