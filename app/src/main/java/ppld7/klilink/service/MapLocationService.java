package ppld7.klilink.service;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import ppld7.klilink.R;
import ppld7.klilink.data.db.model.EventPopUp;
import ppld7.klilink.data.db.model.MapPopup;
import ppld7.klilink.data.network.model.response.map.GetEventResponse;
import ppld7.klilink.data.network.model.response.map.GetPedagangResponse;

/**
 * Created by Fadhil on 12-May-18.
 */

public class MapLocationService {

    public static double getLatOrLng(String locString, boolean isLat) {
        locString = locString.replace(" ", "");
        String[] locStrArr = locString.split(",");
        double lat = Double.parseDouble(locStrArr[0]);
        double lng = Double.parseDouble(locStrArr[1]);

        return isLat ? lat : lng;
    }

    public static LatLng parseLocation(String locationString) {
        double lat = getLatOrLng(locationString, true);
        double lng = getLatOrLng(locationString, false);

        LatLng position = new LatLng(lat, lng);

        return position;
    }

    public static void setUpPedagangMarker(GoogleMap googleMap, List<GetPedagangResponse> list) {
        for (GetPedagangResponse model : list) {
            try {
                String location = model.getLocation();

                LatLng latLng = MapLocationService.parseLocation(location);

                String waktu = model.getWaktuBuka() + " - " + model.getWaktuTutup();

                MapPopup mapPopup = MapPopup.getInstance(
                        model.getNamaDagangan(),
                        waktu,
                        model.getNoHp(),
                        model.getUrlFoto()
                );

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng)
                        .title(model.getNama())
                        .snippet(model.getNamaDagangan())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_gerobak));
                Marker pedagangMarker = googleMap.addMarker(markerOptions);
                pedagangMarker.setTag(mapPopup);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

    }

    public static void setUpEventMarker(GoogleMap googleMap, List<GetEventResponse> eventList) {
        for (GetEventResponse event : eventList) {
            String location = event.getLocation();

            LatLng latLng = MapLocationService.parseLocation(location);
            EventPopUp eventPopUp = EventPopUp.create(
                    event.getNamaEvent(),
                    event.getWaktuMulai(),
                    event.getWaktuSelesai()
            );

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions
                    .position(latLng)
                    .title(event.getNamaEvent())
                    .snippet(event.getNamaEvent())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_event));
            Marker eventMarker = googleMap.addMarker(markerOptions);
            eventMarker.setTag(eventPopUp);
        }
    }
}
