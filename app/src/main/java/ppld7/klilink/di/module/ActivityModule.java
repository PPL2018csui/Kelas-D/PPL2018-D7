/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import ppld7.klilink.di.ActivityContext;
import ppld7.klilink.di.PerActivity;
import ppld7.klilink.ui.Event.EventMvpPresenter;
import ppld7.klilink.ui.Event.EventMvpView;
import ppld7.klilink.ui.Event.EventPresenter;
import ppld7.klilink.ui.login.LoginMvpPresenter;
import ppld7.klilink.ui.login.LoginMvpView;
import ppld7.klilink.ui.login.LoginPresenter;
import ppld7.klilink.ui.login.PasswordTokenMvpPresenter;
import ppld7.klilink.ui.login.PasswordTokenMvpView;
import ppld7.klilink.ui.login.PasswordTokenPresenter;
import ppld7.klilink.ui.login.ResetPasswordMvpPresenter;
import ppld7.klilink.ui.login.ResetPasswordMvpView;
import ppld7.klilink.ui.login.ResetPasswordPresenter;
import ppld7.klilink.ui.main.MainMvpPresenter;
import ppld7.klilink.ui.main.MainMvpView;
import ppld7.klilink.ui.main.MainPresenter;
import ppld7.klilink.ui.main.maps.MapMvpPresenter;
import ppld7.klilink.ui.main.maps.MapMvpView;
import ppld7.klilink.ui.main.maps.MapPresenter;
import ppld7.klilink.ui.main.order.OrderMvpPresenter;
import ppld7.klilink.ui.main.order.OrderMvpView;
import ppld7.klilink.ui.main.order.OrderPresenter;
import ppld7.klilink.ui.main.order.chat.ChatMvpPresenter;
import ppld7.klilink.ui.main.order.chat.ChatMvpView;
import ppld7.klilink.ui.main.order.chat.ChatPresenter;
import ppld7.klilink.ui.main.order.detail.OrderDetailMvpPresenter;
import ppld7.klilink.ui.main.order.detail.OrderDetailMvpView;
import ppld7.klilink.ui.main.order.detail.OrderDetailPresenter;
import ppld7.klilink.ui.main.order.list.OrderListMvpPresenter;
import ppld7.klilink.ui.main.order.list.OrderListMvpView;
import ppld7.klilink.ui.main.order.list.OrderListPresenter;
import ppld7.klilink.ui.main.profile.ProfileMvpPresenter;
import ppld7.klilink.ui.main.profile.ProfileMvpView;
import ppld7.klilink.ui.main.profile.ProfilePresenter;
import ppld7.klilink.ui.menu_add_edit.MenuMvpPresenter;
import ppld7.klilink.ui.menu_add_edit.MenuMvpView;
import ppld7.klilink.ui.menu_add_edit.MenuPresenter;
import ppld7.klilink.ui.register.RegisterMvpPresenter;
import ppld7.klilink.ui.register.RegisterMvpView;
import ppld7.klilink.ui.register.RegisterPresenter;
import ppld7.klilink.ui.register.verify.VerifyMvpPresenter;
import ppld7.klilink.ui.register.verify.VerifyMvpView;
import ppld7.klilink.ui.register.verify.VerifyPresenter;
import ppld7.klilink.ui.setprofile.EditProfileMvpPresenter;
import ppld7.klilink.ui.setprofile.EditProfileMvpView;
import ppld7.klilink.ui.setprofile.EditProfilePresenter;
import ppld7.klilink.ui.splash.SplashMvpPresenter;
import ppld7.klilink.ui.splash.SplashMvpView;
import ppld7.klilink.ui.splash.SplashPresenter;
import ppld7.klilink.utils.rx.AppSchedulerProvider;
import ppld7.klilink.utils.rx.SchedulerProvider;

/**
 * Created by janisharali on 27/01/17.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MapMvpPresenter<MapMvpView> provideMapPresenter(MapPresenter<MapMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> providesMainPresenter(MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ProfileMvpPresenter<ProfileMvpView> providesProfilePresenter(ProfilePresenter<ProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @PerActivity
    EditProfileMvpPresenter<EditProfileMvpView> provideProfilePresenter(EditProfilePresenter<EditProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegisterMvpPresenter<RegisterMvpView> provideRegisterPresenter(RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MenuMvpPresenter<MenuMvpView> provideMenuPresenter(MenuPresenter<MenuMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    EventMvpPresenter<EventMvpView> provideEventPresenter(EventPresenter<EventMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OrderMvpPresenter<OrderMvpView> provideOrderPresenter(OrderPresenter<OrderMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OrderListMvpPresenter<OrderListMvpView> provideOrderListPresenter(OrderListPresenter<OrderListMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OrderDetailMvpPresenter<OrderDetailMvpView> provideOrderDetailPresenter(OrderDetailPresenter<OrderDetailMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ChatMvpPresenter<ChatMvpView> provideChatPresenter(ChatPresenter<ChatMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    VerifyMvpPresenter<VerifyMvpView> provideVerifyPresenter(VerifyPresenter<VerifyMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ResetPasswordMvpPresenter<ResetPasswordMvpView> provideResetPasswordPresenter(ResetPasswordPresenter<ResetPasswordMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PasswordTokenMvpPresenter<PasswordTokenMvpView> providePasswordTokenPresenter(PasswordTokenPresenter<PasswordTokenMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

}
