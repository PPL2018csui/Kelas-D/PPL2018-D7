/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package ppld7.klilink.di.component;

import dagger.Component;
import ppld7.klilink.di.PerActivity;
import ppld7.klilink.di.module.ActivityModule;
import ppld7.klilink.ui.Event.EventActivity;
import ppld7.klilink.ui.login.LoginActivity;
import ppld7.klilink.ui.login.PasswordTokenActivity;
import ppld7.klilink.ui.login.ResetPasswordActivity;
import ppld7.klilink.ui.main.MainActivity;
import ppld7.klilink.ui.main.maps.MapFragment;
import ppld7.klilink.ui.main.order.OrderActivity;
import ppld7.klilink.ui.main.order.chat.ChatActivity;
import ppld7.klilink.ui.main.order.detail.OrderDetailActivity;
import ppld7.klilink.ui.main.order.list.OrderListFragment;
import ppld7.klilink.ui.main.profile.ProfileFragment;
import ppld7.klilink.ui.menu_add_edit.MenuActivity;
import ppld7.klilink.ui.register.RegisterActivity;
import ppld7.klilink.ui.register.verify.VerifyActivity;
import ppld7.klilink.ui.setprofile.EditProfileActivity;
import ppld7.klilink.ui.splash.SplashActivity;

/**
 * Created by janisharali on 27/01/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(LoginActivity activity);

    void inject(EditProfileActivity activity);

    void inject(MapFragment fragment);

    void inject(MainActivity activity);

    void inject(ProfileFragment f);

    void inject(RegisterActivity activity);

    void inject(MenuActivity activity);

    void inject(EventActivity activity);

    void inject(OrderActivity orderActivity);

    void inject(OrderListFragment fragment);

    void inject(OrderDetailActivity activity);

    void inject(ChatActivity activity);

    void inject(VerifyActivity activity);

    void inject(ResetPasswordActivity activity);

    void inject(PasswordTokenActivity activity);

    void inject(SplashActivity activity);
}
