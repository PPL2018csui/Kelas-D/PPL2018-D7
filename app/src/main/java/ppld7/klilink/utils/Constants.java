package ppld7.klilink.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Fadhil on 26-May-18.
 */

public class Constants {

    public static String CONNECTION_ERROR_MESSAGE = "Error: Koneksi Bermasalah!";
    public static String CANCEL_ORDER_MESSAGE = "Apakah anda yakin untuk membatalkan pesanan anda?";
    public static String CANCEL_ORDER_TITLE = "Pembatalan Pesanan";
    public static String EMPTY_ORDER_TITLE = "Pedagang Ini Belum Memiliki Menu...";
    public static String EMPTY_ORDER_MESSAGE = "Mungkin anda dapat mencari pedagang lain terlebih dahulu";
    public static String EMPTY_ORDER_LIST_TITLE = "Daftar Pesanan Kosong...";
    public static String EMPTY_ORDER_LIST_MESSAGE = "Daftar pesanan masih kosong";
    public static String EMPTY_MENU_LIST_TITLE = "Daftar Menu Kosong...";
    public static String EMPTY_MENU_LIST_MESSAGE = "Silahkan anda menambah baru";
    public static String CANCEL_ORDER_COMMAND = "reject";

    public static String formatPrice(int price) {
        String priceStr = NumberFormat.getNumberInstance(Locale.GERMAN).format(price);
        return "Rp " + priceStr;
    }

    public static String formatPrice(String price) {
        Integer num = Integer.parseInt(price);
        String priceStr = NumberFormat.getNumberInstance(Locale.GERMAN).format(num);
        return "Rp " + priceStr;
    }
}
