[![Klilink](https://drive.google.com/uc?export=download&id=1oeJCiFoOsBglDMSeK7KTDNCEtSZ18-DH)](https://appsklilink.wordpress.com/)

# Introduction
### "Klilink? Apa itu?"

Klilink mempunyai misi sederhana yaitu memudahkan para pedagang keliling di sekitar rumah kalian itu mendapatkan customer dengan mudah dan efisien.  Karena target pengguna aplikasi kami ini difokuskan kepada penjual jasa atau barang. Maka, kami akan memberikan yang terbaik untuk mereka. Tapi jangan khawatir, pembeli pun mendapatkan kemudahan yang serupa, contohnya lebih mudah kalau mau mencari makanan yang “lagi ingin dibeli tapi gamau capek muter-muter komplek” dan sebagainya.

# Build Status & Coverage

Berikut adalah ringkasan **_build status_** dan **_code coverage_** dari *branch* yang penting :


### master
- Pipeline: [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/badges/master/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/commits/master)
- Android Coverage: [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/badges/master/coverage.svg?job=android_unit_test)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/commits/master)
- Back-End Coverage: [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/badges/master/coverage.svg?job=restful_test)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/commits/master)

### sit-uat
- Pipeline: [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/badges/sit_uat/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/commits/sit_uat)
- Android Coverage: [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/badges/sit_uat/coverage.svg?job=android_unit_test)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/commits/sit_uat)
- Back-End Coverage: [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/badges/sit_uat/coverage.svg?job=restful_test)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/commits/sit_uat)


# Getting Started

### Clone The Project:

```bash
$ git clone https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7.git [Folder_Name]
$ cd [Folder_Name]
```

>
**Semua Perintah yang dijalankan dibawah telah diasumsikan bahwa anda sedang berada di _directory_ [Folder_Name]**

### Build The Project

**Android**

Untuk _build project_ caranya dapat langsung di-_build_ melalui Android Studio atau dengan cara menjalankan perintah:

```bash
$ ./gradle build
$ ./gradle assembleRelease
```

**Back-End**

Untuk menjalankan server secara lokal, jalankan perintah berikut:

```bash
$ pip install -r requirements.txt
$ cd api
$ python manage.py runserver
```

### Running The Test

**Android**

Anda bisa membuat _unit test_ untuk Android baru dengan membuat _file test_ baru di `app/src/test/java/ppld7/klilink`. Untuk menjalankan _test_ pada Android, jalankan perintah berikut:

```bash
$ ./gradlew jacocoTestReport
```

Hasil _test report_ dapat dilihat di bagian `app/build/reports/jacoco/jacocoTestReport/html/index.html`

**Back-End**

Anda bisa membuat _unit test_ untuk Back-End baru dengan membuat _file test_ baru di `api/klilink_app/tests`. Untuk menjalankan _test_ pada Back-End, jalankan perintah berikut:

```bash
$ cd api
$ python manage.py unittest
```

### Checkstyle

Perintah ini berguna untuk mengecek apakah kode yang dibuat sudah sesuai dengan ketentuan umum, dikenal juga dengan istilah _linter_, berikut adalah perintahnya:

**Android**
```bash
$ ./gradlew lint
```

**Back-End**
```bash
$ cd api
$ python manage.py lint
```

# Built With

* [Gradle](https://gradle.org/) - Build tool for Android
* [Firebase](https://firebase.google.com/) - Realtime Database and Backend Service
* [Retrofit2](http://square.github.io/retrofit/) - HTTP Requests for Android and Java
* [Glide](https://github.com/bumptech/glide) - Image Loading and Caching for Android
* [JUnit](https://github.com/junit-team/junit4) - Testing Framework for Java
* [Mockito](https://github.com/mockito/mockito) - Mocking framework for unit tests
* [PowerMock](http://powermock.github.io/) - Extended version of Mockito
* [Flask Microframework](http://flask.pocoo.org/) - REST Framework used
* [Maven](https://maven.apache.org/) - Dependency Management


# Versioning

Kami menggunakan [SemVer](http://semver.org/) untuk melakukan _versioning_. Untuk melihat versi yang tersedia, silahkan anda lihat pada [_tags_ di _repository_ ini](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D7/tags).

# Authors

Silahkan kunjungi blog kami di [BTS Of Klilink](https://appsklilink.wordpress.com/blog)

- [**Dwi Nanda**](https://gitlab.com/dwi.nanda09) - Hustler
- [**Farizki Yazid**](https://gitlab.com/yazidfarizki) - Hipster
- [**Ibrahim**](https://gitlab.com/ibamibrahim0) - Hacker
- [**Indra Pambudi**](https://gitlab.com/idrpambudi) - Hacker
- [**Muhammad Farhan**](https://gitlab.com/mfarhan27) - Hacker
- [**Muhammad Fadhillah**](https://gitlab.com/bedhilzz) - Hipster