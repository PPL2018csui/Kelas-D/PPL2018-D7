#!/bin/bash
apt-get --quiet update --yes
apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1
wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/android-sdk_r${ANDROID_SDK_TOOLS}-linux.tgz
tar --extract --gzip --file=android-sdk.tgz

# add license
mkdir -p $ANDROID_HOME/licenses
printf "8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e" > android-sdk-linux/licenses/android-sdk-license

echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter android-${ANDROID_TARGET_SDK}
echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter platform-tools
echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS}
echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository
echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-google_play_services
echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository

